// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
// Components
import { BaseComponent } from './views/theme/base/base.component';
import { LandingComponent } from './views/pages/landing/landing.component';
import { PortfolioComponent } from './views/pages/portfolio/portfolio.component';
import { AnalyzeDataComponent } from './views/pages/analyze-data/analyze-data.component';
import { ErrorPageComponent } from './views/theme/content/error-page/error-page.component';
import { UserDashboardComponent } from './views/pages/user-dashboard/user-dashboard.component';
import { InstructionsComponent } from './views/pages/instructions/instructions.component';
import { SearchResultsComponent } from './views/pages/search-results/search-results.component';
import { BrowseTopicsComponent } from './views/pages/browse-topics/browse-topics.component';
import { PrintReportComponent } from './views/pages/print-report/print-report.component';

// Auth
import { AuthGuard } from './core/auth';

const routes: Routes = [
	{path: 'auth', loadChildren: () => import('app/views/pages/auth/auth.module').then(m => m.AuthModule)},
	{path: 'landing', component: LandingComponent},
	{path: 'print-report/:id', component: PrintReportComponent},
	{path: 'print-report', component: PrintReportComponent},
	{path: '', redirectTo: 'landing', pathMatch: 'full'},
	{
		path: '',
		component: BaseComponent,
		//canActivate: [AuthGuard],
		children: [
		
			{
				path: 'report/:id',
				loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
			},
			{
				//path: 'report',
				path: 'report', redirectTo: 'landing', pathMatch: 'full'
				//loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
			},
			{
				path: 'mail',
				loadChildren: () => import('app/views/pages/apps/mail/mail.module').then(m => m.MailModule)
			},
			{
				path: 'portfolio', component:PortfolioComponent,
				canActivate: [AuthGuard]
				//loadChildren: () => import('app/views/pages/portfolio/portfolio.module').then(m => m.PortfolioModule)
			},
			{
				path: 'analyze-data', component:AnalyzeDataComponent
			},
			{
				path: 'user-dashboard', component:UserDashboardComponent,
				canActivate: [AuthGuard]
			},
			{
				path: 'instructions', component:InstructionsComponent
			},
			{
				path: 'search-results', component:SearchResultsComponent
			},
			{
				path: 'browse-topics', component:BrowseTopicsComponent
			},
			//{path: 'error/:type', component: ErrorPageComponent},
			//{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
			{path: '', redirectTo: 'landing', pathMatch: 'full'},
			//{path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
		]
	},
	{path: '', redirectTo: 'landing', pathMatch: 'full'},
	{
		path: 'error/403',
		component: ErrorPageComponent,
		data: {
			'type': 'error-v6',
			'code': 403,
			'title': '403... Access forbidden',
			'desc': 'Looks like you don\'t have permission to access the requested page.<br> Please, contact administrator'
		}
	},
	{path: '**', redirectTo: 'error/403', pathMatch: 'full'}
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
