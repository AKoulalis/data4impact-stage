// Angular
import { Pipe, PipeTransform } from '@angular/core';

/**
 * Returns only first letter of string
 */
@Pipe({
	name: 'filterResults'
})
export class FilterResultsPipe implements PipeTransform {

	transform(items: any[], searchText: string, type: any): any[] {
		if(!items) return [];
		if(!searchText) return items;
		console.log(searchText);
		searchText = searchText.toLowerCase();
		return items.filter( it => {
			console.log(it);
			if(type=='minorCat') {
				return (it.minor_name.toLowerCase().includes(searchText) || it.minor_slug.toLowerCase().includes(searchText));
			} else if(type=='portfolio') {
				return (it.title.toLowerCase().includes(searchText) || it.slug.toLowerCase().includes(searchText));
			}
		});
	}
}
