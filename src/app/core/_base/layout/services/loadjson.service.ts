// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
// RxJS
import { Observable } from 'rxjs';
import { of } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators';
//Echarts needed for Map chart
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;

@Injectable()
export class LoadJsonService {
	private test: string;
	
	constructor(private http: HttpClient) {
    }
	
    public getTitle(title): Observable<any> {
		this.test=title;
		//console.log(this.test);
		return title;
    }
	
    public getJSON(): Observable<any> {
        return this.http.get("./assets/json/"+this.test+".json");
    }
	
	public getChart(slug): Observable<any> {
       // return this.http.get("./assets/json/response1.json");
	   return this.http.get("https://d4iapi.sociality.gr/v1/data/reports/" + slug)
	   //return this.http.get("https://d4iapi.sociality.gr/v1/data/reports/test")
	   /*.pipe(tap(data => console.log('data.comment :', data.data)),
                                 map(data => data.data));*/
    }
	
	public getSearch(keyword): Observable<any>  {
		 //return this.http.get("./assets/json/response2.json");
		//return this.http.get("https://d4iapi.sociality.gr/v1/terms/topics/ear", {
		return this.http.get("https://d4iapi.sociality.gr/v1/terms/topics/" + keyword, {
		}).pipe(catchError(err => {
            console.log('Handling error locally and rethrowing it...', err);
            throw err;
        }));
       	
    }
	
	public getChartOptions(title,data) {		
		const options = this[title](data);
		//console.log('options');
		//console.log(options);
		return options;
    }
	
	
}