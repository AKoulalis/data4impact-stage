// Angular
import { Injectable } from '@angular/core';
// RxJS
import { Observable, Subject,BehaviorSubject } from 'rxjs';
import { of } from 'rxjs';


@Injectable()
export class SearchSidebarService {
	private eventSubject = new BehaviorSubject<any>(undefined);
	
	constructor() {
	}
	
	public toggleSearch(): Observable<any> { 
		//console.log('SearchSidebarService toggleSearch');
		this.eventSubject.next(true); 
		return this.eventSubject.asObservable();
	}
	
	public getEventSubject(): BehaviorSubject<any> {
		//console.log('SearchSidebarService getEventSubject');
		return this.eventSubject;
	}
	
}