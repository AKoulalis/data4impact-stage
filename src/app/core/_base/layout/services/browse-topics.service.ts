// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const API_URL = 'https://d4iapi.sociality.gr/v1/terms/all';

@Injectable()
export class BrowseTopicsService {
	private test: string;
	
    constructor(private http: HttpClient) {}	
	
    public getTopics(): Observable<any> {
        return this.http.get(API_URL);
    }
	
}