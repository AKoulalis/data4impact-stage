import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
 
@Injectable({
    providedIn: 'root'
})
export class SearchKeywordService {
    sKeyword: BehaviorSubject<any>;
    constructor() {
        this.sKeyword = new BehaviorSubject("");
    }
 
    changeKeyword(value: any) {
		this.sKeyword.next(value);
	}
}