// Angular
import { Injectable } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
import { of } from 'rxjs'
//Echarts needed for Map chart


@Injectable()
export class DownloadCSVService {
	
	constructor() {}
	
	public JSONToCSVConvertor(JSONData): Observable<any> {
		//If JSONData is not an object then JSON.parse will parse the JSON string in an Object
		var reportTitle = JSONData.reportTitle;
		var title = JSONData.title;
		var indicator = JSONData.indicator;
		var filters = JSONData.filters;
		var source = JSONData.source;
		var CSV = '';    
		//Set Report title in first row or line   
		if(reportTitle.length) {
			CSV += reportTitle + '\r\n';
		}
		CSV += title + '\r\n';
		CSV += indicator + ' Indicator\r\n';
		if(filters.length) {
			CSV += filters + '\r\n';
		}
		CSV += '\r\n';
		//This condition will generate the Label/Header
		/*if (ShowLabel) {
			var row = "";
			
			//This loop will extract the label from 1st index of on array
			for (var index in arrData[0]) {				
				//Now convert each value to string and comma-seprated
				row += index + ',';
			}
			row = row.slice(0, -1);			
			//append Label row with line break
			CSV += row + '\r\n';
		}*/
		var dData = this[JSONData.csv](JSONData);
		CSV += dData;
		
		if (CSV == '') {        
			alert("Invalid data");
			return;
		}
		CSV += "\r\n" + "Description/Source:," + source;
		
		//Generate a file name
		var fileName = "Data4Impact_";
		//this will remove the blank-spaces from the title and replace it with an underscore
		fileName += title.replace(/ /g,"_");   
		
		//Initialize file format you want csv or xls
		var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
		console.log(CSV);
		// Now the little tricky part.
		// you can use either>> window.open(uri);
		// but this will not work in some browsers
		// or you will not get the correct file extension    
		
		//this trick will generate a temp <a /> tag
		var link = document.createElement("a");    
		link.href = uri;
		
		//set the visibility hidden so it will not effect on your web-layout
		//link.style = "visibility:hidden";
		link.download = fileName + ".csv";
		
		//this part will append the anchor tag and remove it after automatic click
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}

	CSV1(csvData) {
		console.log('CSV1');
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var legend = [];
		Object.keys(arrData).map((key, index) => {
			var row = '"' + key + '",';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				if(index == 0) {
					legend.push(linked_key); 
				}
				row += '"' + arrData[key][linked_key] + '",';				
			});
			row.slice(0, row.length - 1);
			//add a line break after each row
			tableData += row + '\r\n';
		});
		var heading = '"' + csvData.xAxis + '","' + legend[0] + ' (' + csvData.yAxis + ')","' + legend[1] + ' ('+ csvData.yAxis + ')"\r\n';	
		return heading + tableData;
	}
	
	/*CSV2(csvData) {
		console.log('csv2');
		console.log(csvData);
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';		
		var row = '';
		Object.keys(arrData).map((key, index) => {			
			Object.keys(arrData[key]).map((linked_key, linked_index) => {				
				row += '"' + key + '","' + [linked_key] + '","' + arrData[key][linked_key] + '",';
				row.slice(0, row.length - 1);
				row += '\r\n';
			});
		});
		tableData += row + '\r\n';
		var heading = '"' + csvData.yAxis + '",EU Contribution,"' + csvData.xAxis + '"\r\n';	
		return heading + tableData;
	}*/
	
	CSVMap(csvData) {
		console.log('csvData');
		console.log(csvData);
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		
		Object.keys(arrData).map((key, index) => {
			var row = '';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				row += '"' + arrData[key][linked_key] + '",';				
			});
			row.slice(0, row.length - 1);
			//add a line break after each row
			tableData += row + '\r\n';
		});
		var heading = '"Country","EU Contribution (EUR)"\r\n';	
		return heading + tableData;
	}
	
	CSV4(csvData) {
		console.log('CSV4');
		console.log(csvData);
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var row = '';
		Object.keys(arrData).map((key, index) => {						
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				row += '"' + linked_key + '",';
				row += '"' + key + '",';
				row += '"' + arrData[key][linked_key] + '",\r\n';
								
			});
			row.slice(0, row.length - 1);			
		});
		tableData += row + '\r\n';
		var heading = '"Research Area","' + csvData.xAxis + '","EU Contribution ('+ csvData.yAxis + ')"\r\n';	
		return heading + tableData;
	}
	
	CSV5(csvData) {
		console.log('CSV5');
		console.log(csvData);
		var legend = [];
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var row = '';
		Object.keys(arrData).map((key, index) => {	
			row += '"' + key + '",';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				if(index == 0) {
					legend.push(linked_key); 
				}				
				row += '"' + arrData[key][linked_key] + '",';								
			});
			row += '\r\n';
			row.slice(0, row.length - 1);			
		});
		tableData += row + '\r\n';
		//var heading = '"Research Area","' + csvData.xAxis + '","EU Contribution ('+ csvData.yAxis + ')"\r\n';
		var heading = '"' + csvData.xAxis + '","' + legend[0] + ' (' + csvData.yAxis + ')","' + legend[1] + ' ('+ csvData.yAxis + ')"\r\n';			
		return heading + tableData;
	}	
	
	CSV6(csvData) {
		console.log('csv6');
		console.log(csvData);
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';		
		Object.keys(arrData).map((key, index) => {
			var row = '';
			row += '"' + key + '","' + arrData[key] + '",';
			row.slice(0, row.length - 1);
			tableData += row + '\r\n';
		});
		var heading = '"' + csvData.yAxis + '","' + csvData.xAxis + '"\r\n';	
		return heading + tableData;
	}
	
	CSV7(csvData) {
		console.log('CSV7');
		console.log(csvData);
		var legend = [];
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var row = '';
		Object.keys(arrData).map((key, index) => {	
			row += '"' + key + '",';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				if(index == 0) {
					legend.push(linked_key); 
				}				
				row += '"' + arrData[key][linked_key] + '",';								
			});
			row += '\r\n';
			row.slice(0, row.length - 1);			
		});
		tableData += row + '\r\n';
		//var heading = '"Research Area","' + csvData.xAxis + '","EU Contribution ('+ csvData.yAxis + ')"\r\n';
		var heading = '"Funder","' + legend[0] + '","' + legend[1] + '","' + legend[2] + '"\r\n';			
		return heading + tableData;
	}	
	
	CSV8(csvData) {
		console.log('CSV8');
		console.log(csvData);
		console.log(csvData.data[0]);
		var arrDataParticipations= typeof csvData.data[0] != 'object' ? JSON.parse(csvData.data[0]) : csvData.data[0];
		var arrDataCollaborations= typeof csvData.data[1] != 'object' ? JSON.parse(csvData.data[1]) : csvData.data[1];
		var tableData = '';
		var row = '';
		Object.keys(arrDataParticipations).map((key, index) => {	
			row += '"' + key + '",';			
			row += '"' + arrDataParticipations[key] + '",';								
			row += '\r\n';
			row.slice(0, row.length - 1);			
		});
		row += '\r\n';
		var headingCollaborations = '"' + csvData.subtitle + '" 1,"' + csvData.subtitle + '"2,"Number of Collaborations"\r\n';
		row += headingCollaborations;
		Object.keys(arrDataCollaborations).map((key, index) => {				
			Object.keys(arrDataCollaborations[key]).map((linked_key, linked_index) => {	
				row += '"' + key + '",';			
				row += '"' + linked_key + '",';
				row += '"' + arrDataCollaborations[key][linked_key] + '",';
				row += '\r\n';
			});		
			row.slice(0, row.length - 1);			
		});
		tableData += row + '\r\n';
		var headingParticipations = '"' + csvData.subtitle + '","Project Participations"\r\n';	
		return headingParticipations + tableData;
	}	
	
	CSV9(csvData) {
		console.log('csv9');
		console.log(csvData);
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';		
		var row = '';
		Object.keys(arrData).map((key, index) => {			
			Object.keys(arrData[key]).map((linked_key, linked_index) => {				
				row += '"' + key + '","' + [linked_key] + '","' + arrData[key][linked_key] + '",';
				row.slice(0, row.length - 1);
				row += '\r\n';
			});
		});
		tableData += row + '\r\n';
		var heading = '"' + csvData.yAxis + '",Topic,"' + csvData.xAxis + '"\r\n';	
		return heading + tableData;
	}

	CSV10(csvData) {
		console.log('CSV10');
		console.log(csvData);
		var legend = [];
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var row = '';
		Object.keys(arrData).map((key, index) => {	
			row += '"' + key + '",';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				if(index == 0) {
					legend.push(linked_key); 
				}				
				row += '"' + arrData[key][linked_key] + '",';								
			});
			row += '\r\n';
			row.slice(0, row.length - 1);			
		});
		tableData += row + '\r\n';
		var heading = '"Topic","' + legend[0] + ' (' + csvData.xAxis + ')","' +  legend[1] + ' (' + csvData.xAxis + ')","' + '"\r\n';			
		return heading + tableData;
	}
	
	CSV11(csvData) {
		console.log('csv11');
		console.log(csvData);
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';		
		Object.keys(arrData).map((key, index) => {
			var row = '';
			row += '"' + key + '","' + arrData[key] + '",';
			row.slice(0, row.length - 1);
			tableData += row + '\r\n';
		});
		var heading = '"Funder","%"\r\n';	
		return heading + tableData;
	}

	CSV12(csvData) {
		console.log('CSV12');
		console.log(csvData);
		var legend = [];
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var row = '';
		Object.keys(arrData).map((key, index) => {	
			row += '"' + key + '",';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				if(index == 0) {
					legend.push(linked_key); 
				}				
				row += '"' + arrData[key][linked_key] + '",';								
			});
			row += '\r\n';
			row.slice(0, row.length - 1);			
		});
		tableData += row + '\r\n';
		var heading = '"Research Area","' + legend[0] + '","' +  legend[1] + '","' +  legend[2] + '","' +  legend[3] + '","' + '"\r\n';			
		return heading + tableData;
	}
	
	CSV13(csvData) {
		console.log('csv13');
		console.log(csvData);
		var arrData1 = typeof csvData.data1 != 'object' ? JSON.parse(csvData.data1) : csvData.data1;
		var arrData2 = typeof csvData.data2 != 'object' ? JSON.parse(csvData.data2) : csvData.data2;
		var tableData1 = '';	
		var tableData2 = ''
		Object.keys(arrData1).map((key, index) => {
			var row = '';
			row += '"' + key + '","' + arrData1[key] + '",';
			row.slice(0, row.length - 1);
			tableData1 += row + '\r\n';
		});
		var heading1 = '"' + csvData.subtitle1 + '","Fraction"\r\n';
		Object.keys(arrData2).map((key, index) => {
			var row = '';
			row += '"' + key + '","' + arrData2[key] + '",';
			row.slice(0, row.length - 1);
			tableData2 += row + '\r\n';
		});
		var heading2 = '"' + csvData.subtitle2 + '","Number"\r\n';
		return heading1 + tableData1 + '\r\n' + heading2 + tableData2;
	}	
	
	CSV14(csvData) {
		console.log('CSV14');
		console.log(csvData);
		var legend = [];
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var row = '';
		Object.keys(arrData).map((key, index) => {	
			row += '"' + key + '",';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				if(index == 0) {
					legend.push(linked_key); 
				}				
				row += '"' + arrData[key][linked_key] + '",';								
			});
			row += '\r\n';
			row.slice(0, row.length - 1);			
		});
		tableData += row + '\r\n';
		var heading = '"' + csvData.yAxis + '","' + legend[0] + ' (' + csvData.xAxis + ')","' +  legend[1] + ' (' + csvData.xAxis + ')","' + legend[2] + ' (' + csvData.xAxis + ')","' + legend[3] + ' (' + csvData.xAxis + ')","' + '"\r\n';			
		return heading + tableData;
	}	
	
	CSV15(csvData) {
		console.log('csv15');
		console.log(csvData);
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';		
		Object.keys(arrData).map((key, index) => {
			var row = '';
			row += '"' + arrData[key]['Company Name'] + '","' + arrData[key]['Uptake Score'] + '",';
			row.slice(0, row.length - 1);
			tableData += row + '\r\n';
		});
		var heading = '"Company","Uptake Score"\r\n';	
		return heading + tableData;
	}

	CSV16(csvData) {
		console.log('CSV16');
		console.log(csvData);
		var legend = [];
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var row = '';
		Object.keys(arrData).map((key, index) => {	
			row += '"' + key + '",';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				if(index == 0) {
					legend.push(linked_key); 
				}				
				row += '"' + arrData[key][linked_key] + '",';								
			});
			row += '\r\n';
			row.slice(0, row.length - 1);			
		});
		tableData += row + '\r\n';
		var heading = '"' + csvData.xAxis + '","' + legend[0] + '","' + legend[1] + '"\r\n';			
		return heading + tableData;
	}
	

	CSV18(csvData) {
		console.log('CSV18');
		console.log(csvData);
		var dataset = [];
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';
		var row = '';
		var keys = Object.keys(arrData);
		keys.map((key, index) => {
			if(index == 0) {
				dataset.push(keys[index], keys[(index+1)] );
				
				Object.keys(arrData[key]).map((linked_key, linked_index) => {	
					row += '"' + linked_key + '",';				
					row += '"' + arrData[keys[index]][linked_key] + '",';
					row += '"' + arrData[keys[(index+1)]][linked_key] + '"\r\n';					
				});
				row += '\r\n';
				row.slice(0, row.length - 1);	
			}			
		});
		tableData += row + '\r\n';
		var heading = '"Research Area","' + dataset[0] + '","' + dataset[1] + '"\r\n';			
		return heading + tableData;
	}	
	
	CSV19(csvData) {
		console.log('csv19');
		console.log(csvData);
		var arrData = typeof csvData.data != 'object' ? JSON.parse(csvData.data) : csvData.data;
		var tableData = '';		
		var row = '';
		Object.keys(arrData).map((key, index) => {						
			row += '"' + arrData[key]['topic_name'] + '","' + arrData[key]['Number of Projects'] + '","' + arrData[key]['Funding'] + '","' + arrData[key]['Social Media Buzz'] + '",';
			row.slice(0, row.length - 1);
			row += '\r\n';
		});
		tableData += row + '\r\n';
		var heading = '"Topic","Number of Projects","' + csvData.xAxis + '","' + csvData.yAxis + '"\r\n';	
		return heading + tableData;
	}
	

	
	CSVTable(csvData) {
		console.log('csvData');
		console.log(csvData);
		var arrData = typeof csvData.data.D4I_Dataset != 'object' ? JSON.parse(csvData.data.D4I_Dataset) : csvData.data.D4I_Dataset;
		var tableData = '';
		var headData = [];
		Object.keys(arrData).map((key, index) => {
			var row = '';
			Object.keys(arrData[key]).map((linked_key, linked_index) => {
				if(index == 0) {
					headData.push(linked_key); 
				}
				row += '"' + arrData[key][linked_key] + '",';				
			});
			row.slice(0, row.length - 1);
			//add a line break after each row
			tableData += row + '\r\n';
		});
		var heading = '"' + headData[0] + '","' + headData[1] + '"\r\n';
		return heading + tableData;
	}
		
	
	
}