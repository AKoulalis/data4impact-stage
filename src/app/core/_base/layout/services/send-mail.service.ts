// Angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// RxJS
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const API_URL = 'https://d4iapi.sociality.gr/v1/communication';

@Injectable()
export class SendMailService {
	private test: string;
	
    constructor(private http: HttpClient) {}
	
	public sendMail(body): Observable<any> {
		console.log('email service init');
		return this.http.post(API_URL, body).pipe(
			map((result:any) => {
				console.log('email service worked');
				console.log(result);
				return result;
			})
		);

    }

	
}