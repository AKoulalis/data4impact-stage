// Angular
import { Injectable } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
import { of } from 'rxjs';


@Injectable()
export class DownloadJSONService {
	
	constructor() {}
	
	public DownloadJSON(JSONData): Observable<any> { 
		console.log('DownloadJSON');	
		var title = JSONData.title;
		/*var title = 'JSONData.title';
		var indicator = 'JSONData.indicator';
		var filters = 'JSONData.filters';
		var source = 'JSONData.source';*/
		var JSONFile = '';    
		//Set Report title in first row or line   
		JSONFile += '\r\n';
		var dData = this[JSONData.json](JSONData);
		//var dData = '';
		JSONFile += dData;
		
		if (JSONFile == '') {        
			alert("Invalid data");
			return;
		}
		
		//Generate a file name
		var fileName = "Data4Impact_";
		//this will remove the blank-spaces from the title and replace it with an underscore
		fileName += title.replace(/ /g,"_");   
		
		//Initialize file format you want json or xls
		var uri = 'data:text/json;charset=utf-8,' + escape(JSONFile);
		console.log(JSONFile);
		var link = document.createElement("a");    
		link.href = uri;
		
		//set the visibility hidden so it will not effect on your web-layout
		//link.style = "visibility:hidden";
		link.download = fileName + ".json";
		
		//this part will append the anchor tag and remove it after automatic click
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}
	
	JSONTable(jsonData) {
		console.log('jsonData');
		console.log(jsonData);
		delete jsonData.json;
		delete jsonData.filters;
		var tableData = JSON.stringify(jsonData, null, "\t");
		return tableData;
	}
	
	
}