// Angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// RxJS
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';

const API_URL = 'https://d4iapi.sociality.gr/v1/portfolio';

@Injectable()
export class PortfolioService {
	private test: string;
	
    constructor(private http: HttpClient) {}
	
	public getPortfolio(callType, callData): Observable<any> {
       // return this.http.get("./assets/json/response1.json");
		let request = 'https://d4iapi.sociality.gr/v1/entities/';
		switch(callType){
			case 'organization':
				request += 'organizations';
			break;
			case 'program':
				//request += 'programs/' + callData;
				request += 'all/programs';
			break;
			case 'call':
				//request += 'calls/ORG:EC/' + callData;
				request += 'all/calls';
			break;
			case 'project':
				//request += 'projects/ORG:EC/PROG:FP7-ENVIRONMENT/' + callData;
				request += 'all/projects';
			break;
			default:
				request = 'https://d4iapi.sociality.gr/v1/entities/organizations/';
			break;
		}
		console.log(request);
		return this.http.get(request);
	   /*.pipe(tap(data => console.log('data.comment :', data.data)),
                                 map(data => data.data));*/
    }
	
	public getPortfolioPerItem(callType, callData): Observable<any> {
       // return this.http.get("./assets/json/response1.json");
		let request = 'https://d4iapi.sociality.gr/v1/entities/one/';
		switch(callType){
			case 'organization':
				request += 'organizations';
			break;
			case 'program':
				request += 'programs/' + callData;
				//request += 'all/programs';
			break;
			case 'call':
				request += 'calls/' + callData;
				//request += 'all/calls';
				
			break;
			case 'project':
				request += 'projects/' + callData;
				//request += 'all/projects';
				//request = 'https://d4iapi.sociality.gr/v1/entities/projects/ORG:EC/PROG:FP7-ENERGY/[%22CALL:FP7-ENV-2009-1%22]';
				//request = 'https://d4iapi.sociality.gr/v1/entities/one/projects/CALL:FP7-ENV-2009-1';
			break;
			default:
				request = 'https://d4iapi.sociality.gr/v1/entities/organizations/';
			break;
		}
		console.log(request);
		return this.http.get(request);
	   /*.pipe(tap(data => console.log('data.comment :', data.data)),
                                 map(data => data.data));*/
    }


	public removePortfolioSubItem(callType, callData): Observable<any> {
		console.log(callType, callData);
       // return this.http.get("./assets/json/response1.json");
		let request = 'https://d4iapi.sociality.gr/v1/entities/one/';
		switch(callType){
			case 'organization':
				request += 'organizations';
			break;
			case 'program':
				request += 'programs/' + callData;
				//request += 'all/programs';
			break;
			case 'call':
				request += 'calls/' + callData;
				//request += 'all/calls';
				
			break;
			case 'project':
				request += 'projects/' + callData;
				//request += 'all/projects';
				//request = 'https://d4iapi.sociality.gr/v1/entities/projects/ORG:EC/PROG:FP7-ENERGY/[%22CALL:FP7-ENV-2009-1%22]';
				//request = 'https://d4iapi.sociality.gr/v1/entities/one/projects/CALL:FP7-ENV-2009-1';
			break;
			default:
				request = 'https://d4iapi.sociality.gr/v1/entities/organizations/';
			break;
		}
		console.log(request);
		return this.http.get(request);
	   /*.pipe(tap(data => console.log('data.comment :', data.data)),
                                 map(data => data.data));*/
    }

	
	public savePortfolio(requestBody): Observable<any> {
		console.log('service savePortfolio');
		/*let body= {
			"portfolio":
			{
			"organizations": [{"name": "EU", "slug":"EU_SLUG"}],
			"programs": [{"name": "new name", "slug":"fsfsd"}, {"name": "o", "slug":"EU_SfdfffLUG"}],
			"calls": [{"name": "t", "slug":"dsfsdf"}],
			"projects": [{"name": "new name k!", "slug":"ssdsafsd"}, {"name": "k", "slug":"dssdd"}]
			}
		};*/
		let body = requestBody;
		const userToken = localStorage.getItem(environment.authTokenKey);
		//const userId = localStorage.getItem(environment.authUserIdKey);
        const httpHeaders = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', 'Bearer ' + userToken);
		console.log('savePortfolio userToken');
		console.log(userToken);
		//console.log(environment.authTokenKey);
       // httpHeaders.set('Authorization', 'Bearer ' + userToken);
		console.log('httpHeaders');
		console.log(httpHeaders);
		return this.http.put((API_URL), body, { headers: httpHeaders}).pipe(
			map((result:any) => {
				console.log('http.put service result');
				console.log(result);
				return result;
			})
	   );
    }
	
}