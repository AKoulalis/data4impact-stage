export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
			/*
				{
					title: 'Dashboards',
					root: true,
					alignment: 'left',
					page: '/dashboard',
					translate: 'MENU.DASHBOARD',
				},
				{
					title: 'Applications',
					root: true,
					alignment: 'left',
					toggle: 'click',
					submenu: [
						{
							title: 'eCommerce',
							bullet: 'dot',
							icon: 'flaticon-business',
							permission: 'accessToECommerceModule',
							submenu: [
								{
									title: 'Customers',
									page: '/ecommerce/customers'
								},
								{
									title: 'Products',
									page: '/ecommerce/products'
								},
							]
						},
						{
							title: 'User Management',
							bullet: 'dot',
							icon: 'flaticon-user',
							submenu: [
								{
									title: 'Users',
									page: '/user-management/users'
								},
								{
									title: 'Roles',
									page: '/user-management/roles'
								}
							]
						},
					]
				},*/
			]
		},
		aside: {
			self: {},
			items: [
				/*{
					title: 'Dashboard',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: '/dashboard',
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
				},*/
				{
					title: 'Search',
					root: true,
					icon: 'flaticon2-search-1',
					handler: 'openSidebar',
					id: 'kt_search_panel_toggle'
					//page: '/portfolio'
				},
				{
					title: 'My Portfolio',
					root: true,
					icon: 'flaticon-folder',
					page: '/portfolio',
					permission: 'loggedIn',
				},							
				{
					title: 'My Favorites',
					icon: 'flaticon-star',
					permission: 'inactive',
				},
				{
					title: 'Recent Searches',
					icon: 'flaticon-time-1',
					permission: 'inactive',
				},
				{section: ' '},
				{
					title: 'Analyse my data',
					root: true,
					icon: 'flaticon2-analytics-2',
					page: '/analyze-data',
				},
				{
					title: 'Framework & Indicators',
					root: true,
					icon: 'flaticon-interface-10',
					handler: 'openFramework',
				},
			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}
