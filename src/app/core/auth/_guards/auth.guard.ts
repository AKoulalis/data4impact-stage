// Angular
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
// RxJS
import { Observable, of } from 'rxjs';
import { tap, map, catchError, skipWhile } from 'rxjs/operators';
// NGRX
import { select, Store } from '@ngrx/store';
// Auth reducers and selectors
import { AppState} from '../../../core/reducers/';
import { isLoggedIn, dtUserChecked } from '../_selectors/auth.selectors';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private store: Store<AppState>, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>  {
		this.store.pipe(
			select((state: any) => state)) // the complete state this time!!!
			.subscribe((completeState: any) => {console.log('!-------AuthGuard completeState------!!');console.log(completeState)})
		;
		return this.store
            .pipe(
                select(dtUserChecked),
				tap((loggedIn) => {
					console.log('tap AuthGuard');
					console.log(loggedIn);
				}),
				skipWhile(loggedIn => loggedIn[1] !== true),
                map(loggedIn => {
					console.log('AuthGuard');
					console.log(loggedIn);
                    if (loggedIn[1]) {
						if (!loggedIn[0]) {
							//console.log('AuthGuard checked but not loggedIn');
							this.router.navigateByUrl('/auth/login');
							return false;
						}
						//console.log('AuthGuard checked and loggedIn');
						return true;
                    }
					this.router.navigateByUrl('/auth/login');
					return false;
                }),
				catchError(val => {console.log('val');console.log(val);throw val;})

            );
	/*	return this.store
            .pipe(
                select(dtUserChecked),
                tap((loggedIn) => {
					console.log('AuthGuard');
					console.log(loggedIn);
					console.log(dtUserChecked);
					if (dtUserChecked) {
						if (!loggedIn) {
							console.log(loggedIn);
							console.log(dtUserChecked);
							this.router.navigateByUrl('/auth/login');
						}
					}
                })
            );*/
			

        /*return this.store
            .pipe(
                select(isLoggedIn),
                tap(loggedIn => {
					console.log('AuthGuard');
					console.log(loggedIn);
                    if (!loggedIn) {
                        this.router.navigateByUrl('/auth/login');
                    }
                })
            );*/
			
    }
}
