import { BaseModel } from '../../_base/crud';
import { Address } from './address.model';
import { SocialNetworks } from './social-networks.model';

export class User extends BaseModel {
   /* id: number;
    username: string;
    password: string;
    email: string;
    accessToken: string;
    refreshToken: string;
    roles: number[];
    pic: string;
    fullname: string;
    occupation: string;
	companyName: string;
	phone: string;
    address: Address;
    socialNetworks: SocialNetworks;
	
	firstname: string;
	portfolio:any;
	lastname: string;
	_id: any;
*/
	username: string;
    accessToken: string;
    refreshToken: string;
    roles: number[];
    pic: string;
    fullname: string;
    occupation: string;
	companyName: string;
	phone: string;
    address: Address;
    socialNetworks: SocialNetworks;
	
	
	
	id: any;
	_id: any;
	email: string;
	firstname: string;
	lastname: string;
	portfolio:any;
	access: string;
	state: string;
	password: string;


    clear(): void {
		this.firstname = '';
		this.email = '';
		this.password = '';
		this.portfolio = {};
		this.lastname = '';
        this.id = undefined;
		this._id = undefined;
        /*this.username = '';
        this.password = '';
        this.email = '';
        this.roles = [];
        this.fullname = '';
        this.accessToken = 'access-token-' + Math.random();
        this.refreshToken = 'access-token-' + Math.random();
        this.pic = './assets/media/users/default.jpg';
        this.occupation = '';
        this.companyName = '';
        this.phone = '';
        this.address = new Address();
        this.address.clear();
        this.socialNetworks = new SocialNetworks();
        this.socialNetworks.clear();*/
    }
}
