// Angular
import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
// RxJS
import { filter, mergeMap, tap, withLatestFrom, switchMap } from 'rxjs/operators';
import { defer, Observable, of } from 'rxjs';
// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, select, Store } from '@ngrx/store';
// Auth actions
import { AuthActionTypes, Login, Logout, Register, UserLoaded, UserRequested , InvalidToken} from '../_actions/auth.actions';
import { AuthService } from '../_services/index';
import { AppState } from '../../reducers';
import { environment } from '../../../../environments/environment';
import { isUserLoaded } from '../_selectors/auth.selectors';
// Extras
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const USERS_URL = 'https://d4iapi.sociality.gr/v1/';

@Injectable()
export class AuthEffects {
    @Effect({dispatch: false})
    login$ = this.actions$.pipe(
        ofType<Login>(AuthActionTypes.Login),
        tap(action => {
			//console.log('Effects: login$, then new UserRequested()');
			//console.log(action.payload.authToken);
			//console.log(action.payload);
            localStorage.setItem(environment.authTokenKey, action.payload.authToken);
			localStorage.setItem(environment.authUserIdKey, action.payload.userId);
			//console.log('action.payload.userId');
			//console.log(action.payload);
			//console.log(action.payload.userId);
            this.store.dispatch(new UserRequested());
        })

    );

    @Effect({dispatch: false})
    logout$ = this.actions$.pipe(
        ofType<Logout>(AuthActionTypes.Logout),
        tap(() => {
			//console.log('Auth effect Log out');
            localStorage.removeItem(environment.authTokenKey);
			localStorage.removeItem(environment.authUserIdKey);
			this.router.navigate(['/auth/login'], {queryParams: {returnUrl: this.returnUrl}});
        })
    );

    @Effect({dispatch: false})
    register$ = this.actions$.pipe(
        ofType<Register>(AuthActionTypes.Register),
        tap(action => {
            localStorage.setItem(environment.authTokenKey, action.payload.authToken);
        })
    );

    @Effect({dispatch: false})
    loadUser$ = this.actions$
    .pipe(
        ofType<UserRequested>(AuthActionTypes.UserRequested),
        withLatestFrom(this.store.pipe(select(isUserLoaded))),
        filter(([action, _isUserLoaded]) => !_isUserLoaded),
        mergeMap(([action, _isUserLoaded]) => {
			//console.log('Effects: UserRequested getUserByToken----'); 
			return this.auth.getUserByToken()
			}),
        tap(_user => {
            if (_user) {
				//console.log('Effects: UserRequested loadUser$ _user, then: new UserLoaded');
				//console.log(_user);
				_user.id = _user._id;
				//console.log(_user);
                this.store.dispatch(new UserLoaded({ user: _user }));
            } else {
				//console.log('Effects: UserRequested loadUser$ NO _user, then: new Logout');
                this.store.dispatch(new Logout());
            }
        })
      );

    @Effect()
    init$: Observable<Action> = defer(() => {
		//console.log('Effect init$');
        const userToken = localStorage.getItem(environment.authTokenKey);
		const userId = localStorage.getItem(environment.authUserIdKey);
        let observableResult:Observable<Action> = of({type: 'NO_ACTION'});
        /*if (userToken) {
            observableResult = of(new Login({  authToken: userToken, userId: userId }));
        }
        return observableResult;*/
		const httpHeaders = new HttpHeaders().set('Authorization', 'Bearer ' + userToken);
		//console.log('httpHeaders in Effect init$');
		//console.log(httpHeaders);
		/*return this.http.get((USERS_URL+'/v1/auth/is_logged_in'), { headers: httpHeaders }).subscribe((result:any) => {
			console.log('Effect is_logged_in');
			console.log(result);
			if (userToken) {
				observableResult = of(new Login({  authToken: userToken, userId: userId }));
			}
			observableResult.subscribe(
               res => {
				   return observableResult;
			   }

           );
		   
			return observableResult;
		});*/
	  
		return this.http.get<string>((USERS_URL+'auth/is_logged_in'), { headers: httpHeaders }).pipe(
			/*map((result:any) => {
				console.log('Effect is_logged_in');
				console.log(result);
				if (userToken) {
					observableResult = of(new Login({  authToken: userToken, userId: userId }));
				}
				return observableResult;
			}),*/
			switchMap((result:any) => {
				//console.log('Effect is_logged_in');
				//console.log(result);
				if (userToken) {
					observableResult = of(new Login({  authToken: userToken, userId: userId }));
				}
				return observableResult;
			}),
			/*catchError(err => {
				console.log('error');
				console.log(err);
				localStorage.removeItem(environment.authTokenKey);
				localStorage.removeItem(environment.authUserIdKey);
				this.router.navigate(['/auth/login'], {queryParams: {returnUrl: this.returnUrl}});
				return observableResult;
			})*/
			catchError(err => {
				this.handleError('invalid token', []);
				//console.log('---------------error---------------');
				//console.log(err);
				localStorage.removeItem(environment.authTokenKey);
				localStorage.removeItem(environment.authUserIdKey);
				
				this.store.dispatch(new InvalidToken());
				//this.router.navigate(['/auth/login'], {queryParams: {returnUrl: this.returnUrl}});
				return observableResult;
			})
	   );
    });

    private returnUrl: string;

    constructor(private actions$: Actions,
        private router: Router,
        private auth: AuthService,
        private store: Store<AppState>,
		private http: HttpClient) {

		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				this.returnUrl = event.url;
			}
		});
	}
	
	private handleError<T>(operation = 'operation', result?: any) {
		console.log('handleError');
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            //console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            //return of(result);
			return of(error.error);
        };
    }
}
