import { Action } from '@ngrx/store';
import { User } from '../_models/user.model';

export enum AuthActionTypes {
    Login = '[Login] Action',
    Logout = '[Logout] Action',
    Register = '[Register] Action',
    UserRequested = '[Request User] Action',
    UserLoaded = '[Load User] Auth API',
		CurrentUserUpdated = '[Edit User Dialog] Current User Updated',
	InvalidToken = '[Invalid Token] Action'
}

export class Login implements Action {
    readonly type = AuthActionTypes.Login;
    constructor(public payload: { authToken: string, userId: string }) { }
}

export class Logout implements Action {
    readonly type = AuthActionTypes.Logout;
}

export class Register implements Action {
    readonly type = AuthActionTypes.Register;
    constructor(public payload: { authToken: string }) { }
}


export class UserRequested implements Action {
	constructor() {
		//console.log('user requested action'); 
	}
    readonly type = AuthActionTypes.UserRequested;
}

export class UserLoaded implements Action {
    readonly type = AuthActionTypes.UserLoaded;
    constructor(public payload: { user: User }) { }
}

export class InvalidToken implements Action {
    readonly type = AuthActionTypes.InvalidToken;
}

export class CurrentUserUpdated implements Action {
    readonly type = AuthActionTypes.CurrentUserUpdated;
    constructor(public payload: {
        //auth[user]: User
				//user: Update<User>,
				user: User,
    }) { }
}

export type AuthActions = Login | Logout | Register | UserRequested | UserLoaded | InvalidToken | CurrentUserUpdated;
