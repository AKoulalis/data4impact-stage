import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/user.model';
import { Permission } from '../_models/permission.model';
import { Role } from '../_models/role.model';
import { catchError, map } from 'rxjs/operators';
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';

//const API_USERS_URL = 'api/users';
const API_USERS_URL = 'https://d4iapi.sociality.gr/v1/users/';
const USERS_URL = 'https://d4iapi.sociality.gr/v1/'
const API_USERS_REGISTER_URL = 'https://d4iapi.sociality.gr/v1/auth/register';
const API_PERMISSION_URL = 'api/permissions';
const API_ROLES_URL = 'api/roles';

@Injectable()
export class AuthService {
	roles: any = [
        {
            id: 1,
            title: 'Administrator',
            isCoreRole: true,
            permissions: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        },
        {
            id: 2,
            title: 'Manager',
            isCoreRole: false,
			permissions: [3, 4, 10]
        },
        {
            id: 3,
            title: 'Subscriber',
            isCoreRole: false,
			permissions: [3]
        }
    ];
	
	permissions: any = [
                {
            id: 1,
            name: 'accessToECommerceModule',
            level: 1,
            title: 'eCommerce module',
			parentId : undefined,
			isSelected : false,
			_children : []
        },
        {
            id: 2,
            name: 'accessToAuthModule',
            level: 1,
            title: 'Users Management module',
			parentId : undefined,
			isSelected : false,
			_children : []
        },
        {
            id: 3,
            name: 'loggedIn',
            level: 1,
            title: 'Mail module',
			parentId : undefined,
			isSelected : false,
			_children : []
        },
       
    ];
	
    constructor(private http: HttpClient) {}
	
    // Authentication/Authorization
    login(email: string, password: string): Observable<any> {
		//console.log('logging in');
		//console.log(email, password);
       // return this.http.post<User>(API_USERS_URL, { email, password });
	   return this.http.post((USERS_URL+'auth/authenticate'), { email, password }).pipe(
			map((result:any) => {
				//console.log('result.data on login');
				//console.log(result.data);
				return result.data;
			})
	   );
    }

    getUserByToken(): Observable<User> {
		//console.log('starting getUserByToken');
        //const userToken = localStorage.getItem(environment.authTokenKey);
		const userToken = localStorage.getItem(environment.authTokenKey);
		const userId = localStorage.getItem(environment.authUserIdKey);
		//console.log('localStorage authUserIdKey');
		//console.log(userId);
        //const httpHeaders = new HttpHeaders();
		const httpHeaders = new HttpHeaders().set('Authorization', 'Bearer ' + userToken);
		//console.log('getUserByToken userToken');
		//console.log(userToken);
		//console.log(environment.authTokenKey);
		// httpHeaders.set('Authorization', 'Bearer ' + userToken);
		//console.log('httpHeaders');
		//console.log(httpHeaders);
		// return this.http.get<User>(API_USERS_URL, { headers: httpHeaders });
		// return this.http.post((API_USERS_URL), { headers: httpHeaders }).pipe(
		// 5ddfcf3dc7a348798f889259
	  return this.http.get((API_USERS_URL + userId), { headers: httpHeaders }).pipe(
			map((result:any) => {
				//console.log('result on getUserByToken prod!');
				//console.log(result);
				return result.data;
			})
	   );
    }

    register(user: User): Observable<any> {
		//console.log('registering');
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post<User>(API_USERS_REGISTER_URL, user, { headers: httpHeaders })
            .pipe(
                map((res: User) => {
					//console.log('registered...');
                    return res;
                }),
                catchError(err => {
					console.log('error...');
                    return null;
                })
            );
    }
	testlogin(credentials) {
		//console.log('test logging in');
		//console.log(credentials);
		return fetch( "http://192.168.1.22:3000/v1/auth/authenticate", {
			method: 'post',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(credentials)
			
		})
		.then(response => response.json())
		.then((json) => {	
			//console.log(json);
			//return json;
		}).catch(function(error) {
			//console.log('error:');
			//console.log(error);
			return error;
		}).then( response => {
			return 'ok';
		})
	}
    testRegister(user) {
		//console.log('test registering');
		//console.log(user);
		//return fetch( "https://2jsonplaceholder.typicode.com/todos/1", {
		/*	return fetch( "http://192.168.1.22:3000/v1/users", {
			method: 'get',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			
		})
		.then(response => response.json())
		.then((json) => {	
			console.log(json);
			//return json;
		}).catch(function(error) {
			console.log('error:');
			console.log(error);
			return error;
		}).then( response => {
			return 'ok';
		})
		*/
        return fetch( "http://192.168.1.22:3000/v1/auth/register", {
			method: 'post',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(user)
			
		})
		.then(response => response.json())
		.then((json) => {	
			//console.log(json);
			//return json;
		}).catch(function(error) {
			//console.log('error:');
			//console.log(error);
			return error;
		}).then( response => {
			return 'ok';
		})
    }
    /*
     * Submit forgot password request
     *
     * @param {string} email
     * @returns {Observable<any>}
     */
    public requestPassword(email: string): Observable<any> {
    	//return this.http.get(API_USERS_URL + '/forgot?=' + email)
		return this.http.get(USERS_URL + 'auth/forgot_pass/' + email)
    		.pipe(catchError(this.handleError('forgot-password', []))
	    );
    }

    public resetPassword(body, resetToken: string,): Observable<any> {
    	//return this.http.get(API_USERS_URL + '/forgot?=' + email)	
		//const httpHeaders = new HttpHeaders().set('Authorization', 'Bearer ' + resetToken);
		//console.log('resetPassword resetToken');
		//console.log(resetToken);
		//console.log(body);

		return this.http.put((USERS_URL + 'auth/forgot_pass/' + resetToken), body)
    		.pipe(catchError(this.handleError('reset-password', []))
	    );
    }


    getAllUsers(): Observable<User[]> {
		return this.http.get<User[]>(API_USERS_URL);
    }

    getUserById(userId: number): Observable<User> {
		return this.http.get<User>(API_USERS_URL + `/${userId}`);
	}


    // DELETE => delete the user from the server
	deleteUser(userId: number) {
		const url = `${API_USERS_URL}/${userId}`;
		return this.http.delete(url);
    }

    // UPDATE => PUT: update the user on the server
	updateUser(_user: User): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		return this.http.put(API_USERS_URL, _user, { headers: httpHeaders });
	}

    // CREATE =>  POST: add a new user to the server
	createUser(user: User): Observable<User> {
    	const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		return this.http.post<User>(API_USERS_URL, user, { headers: httpHeaders});
	}

    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
	// items => filtered/sorted result
	findUsers(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		return this.http.post<QueryResultsModel>(API_USERS_URL + '/findUsers', queryParams, { headers: httpHeaders});
    }

    // Permission
    getAllPermissions(): Observable<Permission[]> {
	//getAllPermissions(): Observable<any> {
		//return this.http.get<Permission[]>(API_PERMISSION_URL);
		//console.log('-----------getAllPermissions----------------');
		//console.log(this.permissions);
		return of(this.permissions);
		//return this.http.get<User[]>(API_USERS_URL);
    }

    getRolePermissions(roleId: number): Observable<Permission[]> {
        return this.http.get<Permission[]>(API_PERMISSION_URL + '/getRolePermission?=' + roleId);
    }

    // Roles
    getAllRoles(): Observable<Role[]> {
		//console.log('-----------getAllRoles----------------');
		//console.log(this.roles);
		return of(this.roles);
        //return this.http.get<Role[]>(API_ROLES_URL);
    }

    getRoleById(roleId: number): Observable<Role> {
		//console.log('--getRoleById--');
		return this.http.get<Role>(API_ROLES_URL + `/${roleId}`);
    }

    // CREATE =>  POST: add a new role to the server
	createRole(role: Role): Observable<Role> {
		// Note: Add headers if needed (tokens/bearer)
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		return this.http.post<Role>(API_ROLES_URL, role, { headers: httpHeaders});
	}

    // UPDATE => PUT: update the role on the server
	updateRole(role: Role): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		return this.http.put(API_ROLES_URL, role, { headers: httpHeaders });
	}

	// DELETE => delete the role from the server
	deleteRole(roleId: number): Observable<Role> {
		const url = `${API_ROLES_URL}/${roleId}`;
		return this.http.delete<Role>(url);
	}

    // Check Role Before deletion
    isRoleAssignedToUsers(roleId: number): Observable<boolean> {
        return this.http.get<boolean>(API_ROLES_URL + '/checkIsRollAssignedToUser?roleId=' + roleId);
    }

    findRoles(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
        // This code imitates server calls
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
		return this.http.post<QueryResultsModel>(API_ROLES_URL + '/findRoles', queryParams, { headers: httpHeaders});
		return this.http.post<QueryResultsModel>(API_ROLES_URL + '/findRoles', queryParams, { headers: httpHeaders});
	}

 	/*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
    private handleError<T>(operation = 'operation', result?: any) {
		console.log('handleError');
        return (error: any): Observable<any> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            //return of(result);
			return of(error.error);
        };
    }
}
