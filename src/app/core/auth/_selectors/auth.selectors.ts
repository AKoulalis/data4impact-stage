// NGRX
import { createSelector } from '@ngrx/store';
// Lodash
import { each, find, some } from 'lodash';
// Selectors
import { selectAllRoles } from './role.selectors';
import { selectAllPermissions } from './permission.selectors';
// Models
import { Role } from '../_models/role.model';
import { Permission } from '../_models/permission.model';

export const selectAuthState = state => state.auth;

export const isLoggedIn = createSelector(
    selectAuthState,
    auth => auth.loggedIn
);

export const isLoggedOut = createSelector(
    isLoggedIn,
    loggedIn => !loggedIn
);


export const currentAuthToken = createSelector(
    selectAuthState,
    auth => auth.authToken
);

export const isUserLoaded = createSelector(
    selectAuthState,
    auth => auth.isUserLoaded
);

export const dtUserChecked = createSelector(
   /* selectAuthState.isUserLoaded, selectAuthState.dtUserChecked,
    (isUserLoaded, dtUserChecked) => {
		return isUserLoaded;
	
	}*/
	selectAuthState,
    auth => {return [auth.isUserLoaded, auth.dtUserChecked]}
);

export const currentUser = createSelector(
    selectAuthState,
    auth => {
		//console.log('currentUser selector: auth.user');
		//console.log(auth.user);
	return auth.user;
	}
);

export const currentUserRoleIds = createSelector(
    currentUser,
    user => {
        if (!user) {
            return [];
        }
		//console.log('currentUserRoleIds selector: auth.user');
		let role: number = 1;
		const access = user.access;
		if(access == 'Subscriber') {
			role = 3;
		}
       // return user.roles;
		// console.log(role);
	   return role;
    }
);

export const currentUserPermissionsIds = createSelector(
    currentUserRoleIds,
    selectAllRoles,
    (userRoleIds: number[], allRoles: Role[]) => {
		//console.log('auth currentUserPermissionsIds');
		//console.log(currentUserRoleIds);
		//console.log(userRoleIds);
		//console.log(allRoles);
		var userRoleIdArray = [];
		userRoleIdArray.push(userRoleIds);
        //const result = getPermissionsIdsFrom(userRoleIds, allRoles);
		const result = getPermissionsIdsFrom(userRoleIdArray, allRoles);
        return result;
    }
);

export const checkHasUserPermission = (permissionId: number) => createSelector(
    currentUserPermissionsIds,
    (ids: number[]) => {
        return ids.some(id => id === permissionId);
    }
);

export const currentUserPermissions = createSelector(
    currentUserPermissionsIds,
    selectAllPermissions,
    (permissionIds: number[], allPermissions: Permission[]) => {
        const result: Permission[] = [];
		//console.log('auth - currentUserPermissions');
        each(permissionIds, id => {
			//console.log(id);
            const userPermission = find(allPermissions, elem => elem.id === id);
            if (userPermission) {
                result.push(userPermission);
            }
        });
        return result;
    }
);

function getPermissionsIdsFrom(userRolesIds: number[] = [], allRoles: Role[] = []): number[] {
    const userRoles: Role[] = [];
	//console.log(userRolesIds);
    each(userRolesIds, (_id: any) => {
		//console.log(userRolesIds);
       const userRole = find(allRoles, (_role: Role) => _role.id === _id);
       if (userRole) {
           userRoles.push(userRole);
       }
    });

    const result: number[] = [];
    each(userRoles, (_role: Role) => {
        each(_role.permissions, id => {
            if (!some(result, _id => _id === id)) {
                result.push(id);
            }
        });
    });
	//console.log('getPermissionsIdsFrom');
	//console.log(userRoles);
	//console.log(result);
    return result;
}
