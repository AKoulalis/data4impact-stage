// Actions
import { AuthActions, AuthActionTypes } from '../_actions/auth.actions';
// Models
import { User } from '../_models/user.model';

export interface AuthState {
    loggedIn: boolean;
    authToken: string;
    user: User;
    isUserLoaded: boolean;
	dtUserChecked: boolean;
}

export const initialAuthState: AuthState = {
    loggedIn: false,
    authToken: undefined,
    user: undefined,
    isUserLoaded: false,
	dtUserChecked: false
};

export function authReducer(state = initialAuthState, action: AuthActions): AuthState {
    switch (action.type) {
        case AuthActionTypes.Login: {
            const _token: string = action.payload.authToken;
            return {
                loggedIn: true,
                authToken: _token,
                user: undefined,
                isUserLoaded: false,
				dtUserChecked: false
            };
        }

        case AuthActionTypes.Register: {
            const _token: string = action.payload.authToken;
            return {
                loggedIn: true,
                authToken: _token,
                user: undefined,
                isUserLoaded: false,
				dtUserChecked: false
            };
        }

        case AuthActionTypes.Logout:
            return initialAuthState;

        case AuthActionTypes.UserLoaded: {
            const _user: User = action.payload.user;
			//console.log('Auth Reducers -- AuthActionTypes.UserLoaded');
			//console.log(_user);
            return {
                ...state,
                user: _user,
                isUserLoaded: true,
				dtUserChecked: true
            };
        }
		
		 case AuthActionTypes.InvalidToken: {
			//console.log('Auth Reducers -- InvalidToken');
            return {
                ...state,
				dtUserChecked: true
            };
        }
				
				case AuthActionTypes.CurrentUserUpdated: 
					//console.log(action.payload.user);
					return {
            ...state, user: action.payload.user
        };					

        default:
            return state;
    }
}
