// Angular
import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	OnInit,
	OnDestroy,
	Renderer2
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
// RxJS
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
// Object-Path
import * as objectPath from 'object-path';
// Layout
import {
	LayoutConfigService,
	MenuConfigService,
	MenuHorizontalService,
	MenuOptions,
	OffcanvasOptions,
	SubheaderService
} from '../../../../core/_base/layout';
import { Breadcrumb } from '../../../../core/_base/layout/services/subheader.service';
// HTML Class
import { HtmlClassService } from '../../html-class.service';
// GuidedTour
import {GuidedTourModule, GuidedTourService} from 'ngx-guided-tour';
import { GuidedTour, Orientation } from 'ngx-guided-tour';


import { BehaviorSubject, Subject } from 'rxjs';

@Component({
	selector: 'kt-menu-horizontal',
	templateUrl: './menu-horizontal.component.html',
	styleUrls: ['./menu-horizontal.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuHorizontalComponent implements OnInit, OnDestroy, AfterViewInit {
	// Public properties
	currentRouteUrl: any = '';
	chartId: any;
	rootArrowEnabled: boolean;

	title: string = '';
	desc: string = '';
	breadcrumbs: Breadcrumb[] = [];
	titleEmitter$ = new BehaviorSubject<string>(this.title);

	menuOptions: MenuOptions = {
		submenu: {
			desktop: 'dropdown',
			tablet: 'accordion',
			mobile: 'accordion'
		},
		accordion: {
			slideSpeed: 200, // accordion toggle slide speed in milliseconds
			expandAll: false // allow having multiple expanded accordions in the menu
		}
	};

	offcanvasOptions: OffcanvasOptions = {
		overlay: true,
		baseClass: 'kt-header-menu-wrapper',
		closeBy: 'kt_header_menu_mobile_close_btn',
		toggleBy: {
			target: 'kt_header_mobile_toggler',
			state: 'kt-header-mobile__toolbar-toggler--active'
		}
	};
	
	    public dashboardTour: GuidedTour = {
        tourId: 'report-tour',
        useOrb: false,
        steps: [
			{
                title: 'Welcome!',
                content: 'The Data4Impact Monitor is an end-to-end Business Intelligence data and visualization tool with easy integration to third-party platforms.',
			},
			{
                title: 'Report Page',
				content: 'This is a Report page. Here you can see the analysis related to the entity you have selected.',
			},
			{
                title: 'Report Title',
                selector: '.kt-header_menu__title',
				content: 'The title of the report contains the entity you have selected and in parenthesis its level.',
				orientation: Orientation.Bottom
			},
			{
                title: 'Visualizations',
                content: 'Each visualization presents the values of an indicator. In most, you can zoom in and out by scrolling, placing the mouse over the area of interest. You can filter by selecting/deselecting legends, or moving the ends of range bars, when available. On the top right of each visualization, there are buttons for downloading it in PNG, and the data behind it in CSV or JSON file format. The PNG download will be of the filtered and/or zoomed in version.',
                orientation: Orientation.Bottom
			},		
			{
                title: 'Report Sections',
                selector: '.nav-pills li:nth-child(3)',
                content: ' Through these tabs you can navigate to all the sections of the report.',
                orientation: Orientation.Bottom
			},
			{
                title: 'Print to PDF',
                selector: '.kt-menu__link-icon.flaticon-download',
                content: 'You can also download a PDF version of the entire report.',
                orientation: Orientation.Bottom
			},
			{
                title: 'Additional Filtering',
                selector: '.kt-sticky-toolbar',
                content: 'Clicking on this, opens a set of filters (time, country, participating organisation) that when selected will be applied to the entire report. (Not activated)',
                orientation: Orientation.Right
            },
			{
                title: 'Search Toolbar',
                selector: '.kt-menu__nav li:nth-child(1)',
                content: 'You can view other Reports by clicking on the search toolbar. You can search by topic (e.g. diabetes) OR view an item from your portfolio (e.g. FP7-Health).',
				orientation: Orientation.Right
			},
            {
                title: 'My Portfolio',
                selector: '.kt-menu__nav li:nth-child(2)',
                content: 'Registered users can populate and edit their portfolio with entities such as Organisation, Programmes, Calls and Projects, and view our analysis for them (pending data access confirmation).',
				orientation: Orientation.Right
			},
			{
                title: 'Favorites and Recent Searches',
                selector: '.kt-menu__nav li:nth-child(3)',
                content: 'Registered users can also add a report to My Favorites and view their Recent Searches. (Not activated)',
				orientation: Orientation.Right
			},
			{
                title: 'Analyse my data',
                selector: '.kt-menu__nav li:nth-child(6)',
                content: 'You can also request to have your data analysed and the analysis uploaded on the platform.',
				orientation: Orientation.Right
            },
            {
                title: 'Framework & Indicators',
                selector: '.kt-menu__nav li:nth-child(7)',
                content: 'Finally, you can click here to find out more about approach, including a brief explanation of how we used big data and artificial intelligence in our analysis.',
                orientation: Orientation.Right
            },
        ]
    };
	
	
	
	// Private properties
	private subscriptions: Subscription[] = [];
	/**
	 * Component Conctructor
	 *
	 * @param el: ElementRef
	 * @param htmlClassService: HtmlClassService
	 * @param menuHorService: MenuHorService
	 * @param menuConfigService: MenuConfigService
	 * @param layoutConfigService: LayouConfigService
	 * @param router: Router
	 * @param render: Renderer2
	 * @param cdr: ChangeDetectorRef
	 */
	constructor(
		private el: ElementRef,
		public htmlClassService: HtmlClassService,
		public menuHorService: MenuHorizontalService,
		private menuConfigService: MenuConfigService,
		private layoutConfigService: LayoutConfigService,
		private router: Router,
		private render: Renderer2,
		private cdr: ChangeDetectorRef,
		public subheaderService: SubheaderService,
		private guidedTourService: GuidedTourService
	) {
	}
	
	 public restartTour(): void {
		console.log('restartTour');
        this.guidedTourService.startTour(this.dashboardTour);
    }
	

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * After view init
	 */
	ngAfterViewInit(): void {
		
		this.subscriptions.push(this.subheaderService.title$.subscribe(bt => {
			// breadcrumbs title sometimes can be undefined
			if (bt) {
				Promise.resolve(null).then(() => {
					this.title = bt.title;
					this.desc = bt.desc;
					this.titleEmitter$.next(this.title);
					this.cdr.markForCheck();
				});
			}
		}));

		this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(bc => {
			Promise.resolve(null).then(() => {
				this.breadcrumbs = bc;
			});
		}));
		
		
	}

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.rootArrowEnabled = this.layoutConfigService.getConfig('header.menu.self.root-arrow');

		this.currentRouteUrl = this.router.url;
		this.chartId = this.currentRouteUrl.substring(this.currentRouteUrl.lastIndexOf("/") + 1);
		this.router.events
			.pipe(filter(event => event instanceof NavigationEnd))
			.subscribe(event => {
				this.currentRouteUrl = this.router.url;
				this.cdr.markForCheck();
			});
	}

	/**
	 * Use for fixed left aside menu, to show menu on mouseenter event.
	 * @param e Event
	 */
	mouseEnter(e: Event) {
		// check if the left aside menu is fixed
		if (!document.body.classList.contains('kt-menu__item--hover')) {
			this.render.addClass(document.body, 'kt-menu__item--hover');
		}
	}

	/**
	 * Mouse Leave event
	 * @param event: MouseEvent
	 */
	mouseLeave(event: MouseEvent) {
		this.render.removeClass(event.target, 'kt-menu__item--hover');
	}

	/**
	 * Return Css Class Name
	 * @param item: any
	 */
	getItemCssClasses(item) {
		let classes = 'kt-menu__item';

		if (objectPath.get(item, 'submenu')) {
			classes += ' kt-menu__item--submenu';
		}

		if (!item.submenu && this.isMenuItemIsActive(item)) {
			classes += ' kt-menu__item--active kt-menu__item--here';
		}

		if (item.submenu && this.isMenuItemIsActive(item)) {
			classes += ' kt-menu__item--open kt-menu__item--here';
		}

		if (objectPath.get(item, 'resizer')) {
			classes += ' kt-menu__item--resize';
		}

		const menuType = objectPath.get(item, 'submenu.type') || 'classic';
		if ((objectPath.get(item, 'root') && menuType === 'classic')
			|| parseInt(objectPath.get(item, 'submenu.width'), 10) > 0) {
			classes += ' kt-menu__item--rel';
		}

		const customClass = objectPath.get(item, 'custom-class');
		if (customClass) {
			classes += ' ' + customClass;
		}

		if (objectPath.get(item, 'icon-only')) {
			classes += ' kt-menu__item--icon-only';
		}

		return classes;
	}

	/**
	 * Returns Attribute SubMenu Toggle
	 * @param item: any
	 */
	getItemAttrSubmenuToggle(item) {
		let toggle = 'hover';
		if (objectPath.get(item, 'toggle') === 'click') {
			toggle = 'click';
		} else if (objectPath.get(item, 'submenu.type') === 'tabs') {
			toggle = 'tabs';
		} else {
			// submenu toggle default to 'hover'
		}

		return toggle;
	}

	/**
	 * Returns Submenu CSS Class Name
	 * @param item: any
	 */
	getItemMenuSubmenuClass(item) {
		let classes = '';

		const alignment = objectPath.get(item, 'alignment') || 'right';

		if (alignment) {
			classes += ' kt-menu__submenu--' + alignment;
		}

		const type = objectPath.get(item, 'type') || 'classic';
		if (type === 'classic') {
			classes += ' kt-menu__submenu--classic';
		}
		if (type === 'tabs') {
			classes += ' kt-menu__submenu--tabs';
		}
		if (type === 'mega') {
			if (objectPath.get(item, 'width')) {
				classes += ' kt-menu__submenu--fixed';
			}
		}

		if (objectPath.get(item, 'pull')) {
			classes += ' kt-menu__submenu--pull';
		}

		return classes;
	}

	/**
	 * Check Menu is active
	 * @param item: any
	 */
	isMenuItemIsActive(item): boolean {
		if (item.submenu) {
			return this.isMenuRootItemIsActive(item);
		}

		if (!item.page) {
			return false;
		}

		return this.currentRouteUrl.indexOf(item.page) !== -1;
	}

	/**
	 * Check Menu Root Item is active
	 * @param item: any
	 */
	isMenuRootItemIsActive(item): boolean {
		if (item.submenu.items) {
			for (const subItem of item.submenu.items) {
				if (this.isMenuItemIsActive(subItem)) {
					return true;
				}
			}
		}

		if (item.submenu.columns) {
			for (const subItem of item.submenu.columns) {
				if (this.isMenuItemIsActive(subItem)) {
					return true;
				}
			}
		}

		if (typeof item.submenu[Symbol.iterator] === 'function') {
			for (const subItem of item.submenu) {
				const active = this.isMenuItemIsActive(subItem);
				if (active) {
					return true;
				}
			}
		}

		return false;
	}
	
	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.subscriptions.forEach(sb => sb.unsubscribe());
	}
}
