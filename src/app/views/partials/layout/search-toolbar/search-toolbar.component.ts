// Angular
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { currentUser, User } from '../../../../core/auth';
// NGRX
import { select, Store } from '@ngrx/store';
// State
import { AppState } from '../../../../core/reducers';
// Layout
import { OffcanvasOptions } from '../../../../core/_base/layout';
//Bootstrap
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
//Autocomplete Input
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { catchError, tap, map, filter, debounceTime, distinctUntilChanged, switchMap, startWith } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent, MatExpansionPanelHeader } from '@angular/material';
// UI
import { SubheaderService } from '../../../../core/_base/layout';
import { OffcanvasDirective } from '../../../../core/_base/layout';
import { SearchKeywordService, SearchSidebarService } from "../../../../core/_base/layout/";
//Export Data
export interface Level {
	value: string;
	viewValue: string;
}
export interface Item {
	value: string;
	title: string;
}


@Component({
	selector: 'kt-search-toolbar',
	templateUrl: './search-toolbar.component.html',
	styleUrls: ['./search-toolbar.component.scss',
	],
	providers: [NgbModal, NgbActiveModal]
})

export class SearchToolbarComponent {
	@ViewChild('slider', { read: ElementRef, static: false }) slider: ElementRef;
	@ViewChild(OffcanvasDirective, { static: true }) offcanvasDirective;
	sliderRange;
	someKeyboardConfig: any = {
		connect: [false, true, false],
		start: [2010, 2020],
		tooltips: [true, true],
		step: 1,
		range: {
			min: 2000,
			max: 2030
		},
		behaviour: 'drag',
	};
	demoPanelOptions: OffcanvasOptions = {
		overlay: true,
		baseClass: 'kt-demo-panel',
		closeBy: 'kt_search_panel_close',
		toggleBy: 'kt_search_panel_toggle',
		closeByExternal: 'kt_search_panel_close_external',
	};
	keywordSearch: any = '';
	baseHref: string;
	selectedTopics: string;
	selectedFields: string;
	selectedTopicsOption: string = '';
	selectedFieldsOption: string = '';
	fieldgroup: string;
	entitygroup: string;
	fieldDisabled: Boolean = false;
	entityDisabled: Boolean = false;
	selectedFilter: Boolean = false;
	organizations: any;
	programs: any;
	calls: any;
	projects: any;
	loggedIn: Boolean = false;
	levelIsSelected: Boolean = false;
	portfolioTooltip: string = '';
	//Select Levels
	levels: Level[] = [
		{ value: 'organizations', viewValue: 'Organisation' },
		{ value: 'programs', viewValue: 'Programme' },
		{ value: 'calls', viewValue: 'Call' },
		{ value: 'projects', viewValue: 'Project' }
	];

	//Autocomplete Items #THIS MUST BECOME DYNAMIC FROM MONGODB
	itemCtrl = new FormControl();
	$filteredItems: Observable<Item[]>;
	items: Item[] = [
		{ value: 'fp7', title: 'FP7 Program' },
		{ value: 'anpr', title: 'AnotherONE project' },
	];
	constructor(private searchKeywordService : SearchKeywordService, private store: Store<AppState>, private router: Router, private searchSidebarService: SearchSidebarService) {
		this.$filteredItems = this.itemCtrl.valueChanges
			.pipe(
				startWith(''),
				map(item => item ? this._filterItems(item) : this.items.slice())
			);
	}

	private _filterItems(value): Item[] {
		let filterValue;
		if(value.value) {
			filterValue = value.value.toLowerCase();
		} else {
			filterValue = value.toLowerCase();
		}
		return this.items.filter(item => item.title.toLowerCase().indexOf(filterValue) === 0);
	}
	
	ngOnInit() {
		this.searchSidebarService.getEventSubject().subscribe((m:any) => {
			if(m) {			
				this.toggleSidebar(); 
			}			
		});
		
		//console.log('userInitialPortfolio -  userData1-');
		this.store.pipe(select(currentUser)).subscribe(userData => {
			if(userData) {
				this.portfolioTooltip = 'Your portfolio is empty.';
				if(userData.portfolio) {	
					this.organizations = userData.portfolio.organizations;
					this.programs = userData.portfolio.programs;
					this.calls = userData.portfolio.calls;
					this.projects = userData.portfolio.projects;
					this.loggedIn = true;
				}
			} else {
				this.portfolioTooltip = 'Please log in to view item.';
				//return;
			}			
		});
	}
	
	closeSidebar() {
		this.offcanvasDirective.hideSidebar();
		this.searchKeywordService.changeKeyword(this.keywordSearch);
	}

	//Asset Functions
	levelSelected(event){
		this.itemCtrl.reset();
		const level = event.value;
		const portfolioItems = this[level];
		this.items.length = 0;
		portfolioItems.map(item => {
			this.items.push({value: item.slug, title: item.title});
		});
		//console.log(this.items);	
		this.$filteredItems = this.itemCtrl.valueChanges
			.pipe(
				startWith(''),
				map(item => item ? this._filterItems(item) : this.items.slice())
			);
		this.levelIsSelected = true;
	}

	itemSelected(event){
		//console.log(event.source.value);
		const selection = event.source.value.value;
		this.offcanvasDirective.hideSidebar();
		this.router.navigateByUrl('/report/' + selection);
	}
	
	displayFn(item) {
		return item ? item.title : undefined;
	}
	
	toggleSidebar() {
		this.offcanvasDirective.showSidebar();
	}

	/*
	fieldValueChange(event) {
		//console.log('--field ValueChange');
		const inputValue = event.target.value;
		if (inputValue.length || this.selectedFields) {
			this.entitygroup = 'disabled';
			//console.log('entityDisabled');
			this.entityDisabled = true;
			this.selectedFilter = true;
		} else {
			//console.log('not! entityDisabled');
			this.entitygroup = '';
			this.entityDisabled = false;
			this.selectedFilter = false;
		}
		//console.log(this.selectedFilter);
	}

	entityValueChange(event) {
		console.log('--entity ValueChange');
		//const inputValue = event.target.value;
		const organizationField = (<HTMLInputElement>document.getElementById('organizationField')).value;
		const programField = (<HTMLInputElement>document.getElementById('programField')).value;
		const callField = (<HTMLInputElement>document.getElementById('callField')).value;
		const projectField = (<HTMLInputElement>document.getElementById('projectField')).value;
		if (organizationField.length || programField.length || callField.length || projectField.length || this.selectedTopics) {
			//console.log('fieldDisabled');
			this.fieldgroup = 'disabled';
			this.fieldDisabled = true;
			this.selectedFilter = true;
		} else {
			this.fieldgroup = '';
			this.fieldDisabled = false;
			this.selectedFilter = false;
		}
		//console.log(this.selectedFilter);
	}

	onFieldSelectionChanged(event: MatAutocompleteSelectedEvent) {
		console.log('onFieldSelectionChanged');
		console.log(event.option.value);
		const inputValue = event.option.value;
		if (inputValue.length || this.selectedFields) {
			//console.log('entityDisabled');
			this.entityDisabled = true;
			this.selectedFilter = true;
		} else {
			//console.log('not! entityDisabled');
			this.entityDisabled = false;
			this.selectedFilter = false;
		}
	}

	updateReport() {
		console.log('updateReport');
		const newDescription = '(time range: 2004 - 2005)'
		this.subheaderService.setDescription('Report', 'Indicators for Health ' + newDescription);
	}*/

}
