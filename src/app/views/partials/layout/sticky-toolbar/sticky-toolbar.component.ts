// Angular
import {Component, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
// Layout
import {LayoutConfigService, OffcanvasOptions} from '../../../../core/_base/layout';
//Bootstrap
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
//Autocomplete Input
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import { catchError, tap, map, filter, debounceTime, distinctUntilChanged, switchMap, startWith } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material';
// UI
import { SubheaderService } from '../../../../core/_base/layout';

import { Subscription, BehaviorSubject, Subject } from 'rxjs';

export interface StateGroup {
  letter: string;
  names: string[];
}

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
	selector: 'kt-sticky-toolbar',
	templateUrl: './sticky-toolbar.component.html',
	styleUrls: ['./sticky-toolbar.component.scss',
	// '../../../../../../node_modules/nouislider/distribute/nouislider.min.css'
	],
	providers: [NgbModal, NgbActiveModal]
})

export class StickyToolbarComponent {
	
	@ViewChild('slider', {read: ElementRef, static: false}) slider: ElementRef;
	//pageTitle: any;
	sliderRange;
    someKeyboardConfig: any = {
		connect: [false, true, false ],
		start: [2010, 2020],
		tooltips: [true, true],
		step: 1,
		range: {
		  min: 2000,
		  max: 2030
		},
		behaviour: 'drag',
	};
	// Public properties
	demoPanelOptions: OffcanvasOptions = {
		overlay: true,
		baseClass: 'kt-demo-panel',
		closeBy: 'kt_demo_panel_close',
		toggleBy: 'kt_demo_panel_toggle',
		closeByExternal: 'kt_demo_panel_close_external',
	};

	baseHref: string;
	selectedTopics: string;
	selectedFields: string;
	selectedTopicsOption: string = '';
	selectedFieldsOption: string = '';
	fieldgroup: string;
	entitygroup: string;
	fieldDisabled: Boolean = false;
	entityDisabled: Boolean = false;
	selectedFilter: Boolean = false;
	
	pageTitle: string = '';
	titleEmitter$ = new BehaviorSubject<string>(this.pageTitle);
	private subscriptions: Subscription[] = [];
	
	//Autocomplete
	stateForm: FormGroup = this._formBuilder.group({
		stateGroup: '',
	});
	//myControl: FormControl = new FormControl();
	stateGroups: StateGroup[]  = [
		{
    letter: 'A',
    names: ['Alabama', 'Alaska', 'Arizona', 'Arkansas']
  }, {
    letter: 'C',
    names: ['California', 'Colorado', 'Connecticut']
  }, {
    letter: 'D',
    names: ['Delaware']
  }, {
    letter: 'F',
    names: ['Florida']
  }, {
    letter: 'G',
    names: ['Georgia']
  }, {
    letter: 'H',
    names: ['Hawaii']
  }, {
    letter: 'I',
    names: ['Idaho', 'Illinois', 'Indiana', 'Iowa']
  }, {
    letter: 'K',
    names: ['Kansas', 'Kentucky']
  }, {
    letter: 'L',
    names: ['Louisiana']
  }, {
    letter: 'M',
    names: ['Maine', 'Maryland', 'Massachusetts', 'Michigan',
      'Minnesota', 'Mississippi', 'Missouri', 'Montana']
  }, {
    letter: 'N',
    names: ['Nebraska', 'Nevada', 'New Hampshire', 'New Jersey',
      'New Mexico', 'New York', 'North Carolina', 'North Dakota']
  }, {
    letter: 'O',
    names: ['Ohio', 'Oklahoma', 'Oregon']
  }, {
    letter: 'P',
    names: ['Pennsylvania']
  }, {
    letter: 'R',
    names: ['Rhode Island']
  }, {
    letter: 'S',
    names: ['South Carolina', 'South Dakota']
  }, {
    letter: 'T',
    names: ['Tennessee', 'Texas']
  }, {
    letter: 'U',
    names: ['Utah']
  }, {
    letter: 'V',
    names: ['Vermont', 'Virginia']
  }, {
    letter: 'W',
    names: ['Washington', 'West Virginia', 'Wisconsin', 'Wyoming']
  }
	];
	
	stateGroupOptions: Observable<StateGroup[]>;
	/*filter(val: string): string[] {
		return this.options.filter(option =>
			option.toLowerCase().indexOf(val.toLowerCase()) === 0);
	}*/
	
	//Modal
	private modalRef: NgbModalRef;
	closeResult: string;
	openModal(content) {

		this.modalRef = this.modalService.open(content);
		this.modalRef.result.then((result) => {
       // this.modalService.open(content).result.then((result) => {
       // this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
	
	constructor(private layoutConfigService: LayoutConfigService,private modalService: NgbModal, private _formBuilder: FormBuilder, private activeModal: NgbActiveModal, private subheaderService: SubheaderService, private cd: ChangeDetectorRef) {

		
		//$( '.js-btns' ).offcanvasTrigger( {
		//  offcanvas: "left"
		//});
		//(document.getElementsByClassName("example")).offcanvasTrigger( {offcanvas: "left"});;
		//$( document ).trigger( "enhance" );
		// Add an event listener
		document.addEventListener("enhance", function(e) {
		  //console.log(e); // Prints "Example of an event"
		});

		// Create the event
		var event = new CustomEvent("enhance", { "detail": "Example of an event" });

		// Dispatch/Trigger/Fire the event
		document.dispatchEvent(event);
	}

	ngOnInit() {
		
		//this.subheaderService.setDescription('Report', 'Indicators for Health');
		/*this.stateGroupOptions = this.myControl.valueChanges
		.pipe(
			startWith(''),
			//map(val => val.length >= 2 ? this.filter(val): [])
			map(val => val.length >= 2 ? val: [])
		);*/
		this.stateGroupOptions = this.stateForm.get('stateGroup')!.valueChanges
		.pipe(
			startWith(''),
			map(value => this._filterGroup(value))
		);
		
		
		//let canvastt = [document.getElementById('leftid')] as any;
		//console.log(canvastt);
		/*canvastt.offcanvas({
			modifiers: 'left, overlay', // default options
			triggerButton: '#triggerButton' // btn to open offcanvas
		});*/
	  
	}
	
	
	/**
	 * After view init
	 */
	ngAfterViewInit(): void {
		
		this.subscriptions.push(this.subheaderService.title$.subscribe(bt => {
			// breadcrumbs title sometimes can be undefined
			//console.log('bt-hor');
			//console.log(bt);
			if (bt) {
				Promise.resolve(null).then(() => {
					this.pageTitle = bt.title;
					//this.desc = bt.desc;
					//console.log('this.title');
					//console.log(this.title);
					
					//this.testVariable += '-bar';
					this.titleEmitter$.next(this.pageTitle);
					this.cd.markForCheck();
				});
			}
		}));


		
		
	}
	
	private _filterGroup(value: string): StateGroup[] {
		if (value) {
			return this.stateGroups
			.map(group => ({letter: group.letter, names: _filter(group.names, value)}))
			.filter(group => group.names.length > 0);
		}

		return this.stateGroups;
	}
	isActiveDemo(demo) {
		return demo === this.layoutConfigService.getConfig('demo');
	}
	
	closeModal(selected) {	
		//console.log(selected);
		let selectedOption  = selected + 'Option';
		if(this[selected] !=='none') {
			this[selected] = this[selectedOption];
		}
		//this[selected] = this.selectedTopicsOption;
		this.modalRef.close();
		if(this[selected].length && this[selected] !=='none' ) {
			//console.log('selected');
			if(selected = 'selectedFields') {
				//console.log('entitygroup disabled');
				this.entitygroup = 'disabled';
				this.selectedFilter = true;
				this.entityDisabled = true;
			} else {
				this.fieldgroup = 'disabled';
				this.selectedFilter = true;
				this.fieldDisabled = true;
			}
		} else if(!(this.selectedTopicsOption.length && this.selectedTopicsOption !=='none' && this.selectedFieldsOption.length && this.selectedFieldsOption  !=='none')){
			//console.log('nothing selected');
		}
		this.subheaderService.setDescription('Report', 'Indicators for Health');
		//console.log(disabled);
		
		
	}
	
	fieldValueChange(event) {
		//console.log('--field ValueChange');
		const inputValue = event.target.value;
		if (inputValue.length || this.selectedFields) {
			this.entitygroup = 'disabled';
			//console.log('entityDisabled');
			this.entityDisabled = true;
			this.selectedFilter = true;
		} else {
			//console.log('not! entityDisabled');
			this.entitygroup = '';
			this.entityDisabled = false;
			this.selectedFilter = false;
		}
		//console.log(this.selectedFilter);
	}
	
	entityValueChange(event) {
		//console.log('--entity ValueChange');
		//const inputValue = event.target.value;
		const organizationField = (<HTMLInputElement>document.getElementById('organizationField')).value;
		const programField = (<HTMLInputElement>document.getElementById('programField')).value;
		const callField =(<HTMLInputElement> document.getElementById('callField')).value;
		const projectField = (<HTMLInputElement>document.getElementById('projectField')).value;
		if (organizationField.length || programField.length || callField.length || projectField.length || this.selectedTopics ) {
			//console.log('fieldDisabled');
			this.fieldgroup = 'disabled';
			this.fieldDisabled = true;
			this.selectedFilter = true;
		} else {
			this.fieldgroup = '';
			this.fieldDisabled = false;
			this.selectedFilter = false;
		}
		//console.log(this.selectedFilter);
	}
	
	
	
	onFieldSelectionChanged(event: MatAutocompleteSelectedEvent) {
		//console.log('onFieldSelectionChanged');
		//console.log(event.option.value);
		const inputValue = event.option.value;
		if (inputValue.length || this.selectedFields) {
			//console.log('entityDisabled');
			this.entityDisabled = true;
			this.selectedFilter = true;
		} else {
			//console.log('not! entityDisabled');
			this.entityDisabled = false;
			this.selectedFilter = false;
		}
	}
		
	removeField() {
		this.selectedFields = '';
	}
	
	removeTopic() {
		this.selectedTopics = '';
	}
	
	updateReport() {
		//console.log('updateReport');
		const newDescription = '(time range: 2004 - 2005)'
		this.subheaderService.setDescription('Report', 'Indicators for Health ' + newDescription);
	}
	
}
