// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts 
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart6',
	templateUrl: './echart6.component.html',
	styleUrls: ['./echart6.component.scss'],
})
export class Echart6Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	yTitle = 'type';
	xTitle = 'innovations';
	source = '';
	title = 'Project Innovations';
	rawData: any;
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.yTitle = this.chartData['y_axis'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d2_type_number_of_innovations(this.chartData);
			this.initEChartJS(chartOptions);
		};
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV6',
			data: this.rawData,
			reportTitle: this.chartTitle,
			xAxis: this.xTitle,
			yAxis: this.yTitle,
			title: this.title,
			source: this.source,
			indicator: 'Throughput/Output',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',	
			reportTitle: this.chartTitle,		
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d2_type_number_of_innovations(dData) {
		var l1_d2_type_number_of_innovations =  dData["D4I_Dataset"];
		this.rawData = l1_d2_type_number_of_innovations;
		var innovation_type = [];
		var number_of_innovations = [];
		innovation_type = Object.keys(l1_d2_type_number_of_innovations).sort(function(a,b){return l1_d2_type_number_of_innovations[a]-l1_d2_type_number_of_innovations[b]});
		//number_of_innovations = Object.keys(l1_d2_type_number_of_innovations).sort(function(a,b){return l1_d2_type_number_of_innovations[a]-l1_d2_type_number_of_innovations[b]}).map(key => l1_d2_type_number_of_innovations[key]);
		const colors = [ '#bcbddc', '#9e9ac8', '#756bb1', '#c7e9c0', '#a1d99b', '#31a354', '#fdd0a2', '#fdae6b', '#ed9e6a' ,'#dadaeb',  '#fd8d3c', '#e6550d', '#c6dbef', '#3182bd', '#de9ed6', '#ce6dbd', '#a55194', '#e7969c', '#d6616b', '#e7cb94', '#e7ba52', '#8c6d31', '#cedb9c', '#b5cf6b', '#8ca252', '#637939'  ];
		number_of_innovations = Object.keys(l1_d2_type_number_of_innovations).sort(function(a,b){
		return l1_d2_type_number_of_innovations[a]-l1_d2_type_number_of_innovations[b]}).map((key, index ) => {
			var topic = {  value: l1_d2_type_number_of_innovations[key],  itemStyle: {color: colors[index]} };
			return topic;
			});
			
		//var myChart_l1_d2_type_number_of_innovations = echarts.init(document.getElementById('l1_d2_type_number_of_innovations'));
		
		var chartOptions = {
			title: {
				text: this.title,
			},
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'shadow'
				}
			},
			grid: {
				left: '3%',
				right: '8%',
				bottom: 40,
				containLabel: true
			},
			xAxis: {
				name: this.xTitle,
				type: 'value',
				//boundaryGap: [0, 0.01]
			},
			yAxis: {
				name: this.yTitle,
				type: 'category',
				data: innovation_type,
				axisTick: {
					alignWithLabel: true,
				},
			},
			dataZoom: [{
				type: 'inside',
				//xAxisIndex: 0,
				orient: 'vertical',
			}],
			series: [
				{
					type: 'bar',
					data: number_of_innovations
				}
			]	
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['grid']['right'] = '88';
		} 
		if (this.windowWidth < 500){
			chartOptions['xAxis']['splitNumber'] = 1;
		}
		return chartOptions;
		
	}
	
	
	
	
	
	
	
	
}
