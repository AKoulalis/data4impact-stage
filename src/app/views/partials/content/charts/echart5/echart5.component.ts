// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart5',
	templateUrl: './echart5.component.html',
	styleUrls: ['./echart5.component.scss'],
})
export class Echart5Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	//@Input() desc: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	yTitle = 'publications';
	xTitle = 'year';
	source = '';
	title = 'Project Publications';
	rawData: any;
	windowWidth: number;
	pixelRatio: number = 3;
	
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.yTitle = this.chartData['y_axis'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d2_nof_pubs_per_year(this.chartData);
			this.initEChartJS(chartOptions);
		};
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV5',
			data: this.rawData,
			reportTitle: this.chartTitle,
			xAxis: this.xTitle,
			yAxis: this.yTitle,
			title: this.title,
			source: this.source,
			indicator: 'Throughput/Output',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}	
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',		
			reportTitle: this.chartTitle,	
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d2_nof_pubs_per_year(dData) {
		var number_of_publications_per_year =  dData["D4I_Dataset"];
		this.rawData = number_of_publications_per_year;
		//console.log(number_of_publications_per_year);
		var number_of_publications = []; 
		var number_of_other_publications = []; 
		var year_data = [];
		var series = {};
		Object.keys(number_of_publications_per_year).map((key, index) => {
			year_data.push(key);
			Object.keys(number_of_publications_per_year[key]).map((innerKey, innerIndex) => {
				if(index==0) {
					series[innerKey] = 
					{
						name: innerKey,
						type: 'bar',
						stack: 'one',
						itemStyle: itemStyle,
						data:[]
					};
				}
				series[innerKey]['data'].push(number_of_publications_per_year[key][innerKey]);
				//number_of_publications.push(number_of_publications_per_year[key]["number_of_publications"]);
				//number_of_other_publications.push(number_of_publications_per_year[key]["number_of_other_publications"]);
			});
				//console.log(single_series);
		})
		var seriesChart = [];
		Object.keys(series).map((key, index) => {
			seriesChart.push(series[key]);
		});
		//console.log('year_data');
		//console.log(year_data);
		//console.log(single_series);
		var itemStyle = {
			normal: {
			},
			emphasis: {
				barBorderWidth: 1,
				shadowBlur: 10,
				shadowOffsetX: 0,
				shadowOffsetY: 0,
				shadowColor: 'rgba(0,0,0,0.5)'
			}
		};

		var chartOptions = {
			title: {
				text: this.title
			},
			/*color: [ '#003070','#008ac1','#00c7f9','#9ef2ff',   '#9e9ac8', '#ce6dbd', '#a55194', '#6b6ecf', '#393b79', '#17becf', '#9edae5'],*/
			color: ["#d48265", "#61a0a8"],
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			legend: {
				data: ['Pubmed', 'Other'],
				//align: 'right',
				//left: 0,
				bottom: 30
			},
			dataZoom: [{
				type: 'slider',
				start: 0,
				end: 100,
				bottom: 65
			}, {
				type: 'inside'
			}],
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						}
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			tooltip: {
				
			},
			xAxis: {
				data: year_data,
				name: 'year',
				silent: this.xTitle,
				axisTick: {
					alignWithLabel: true,
					interval: 0
				},
				axisLine: {onZero: true},
				splitLine: {show: false},
				splitArea: {show: false}
			},
			yAxis: {
				//inverse: true,
				name: this.yTitle,
				splitArea: {show: false}
			},
			grid: {
				left: 50,
				//top: 130,
				bottom: 125,
			},
			series: seriesChart
			/*series: [
				{
					name: 'Pubmed',
					type: 'bar',
					stack: 'one',
					itemStyle: itemStyle,
					data: number_of_publications
				},
				{
					name: 'Other',
					type: 'bar',
					stack: 'one',
					itemStyle: itemStyle,
					data: number_of_other_publications
				},

			]	*/
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['grid']['right'] = 40;
		}		
		return chartOptions;
	}
	
	

	
	
}
