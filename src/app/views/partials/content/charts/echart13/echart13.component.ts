// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart13',
	templateUrl: './echart13.component.html',
	styleUrls: ['./echart13.component.scss'],
})
export class Echart13Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	seriesCenterLeft: string = '30%';
	seriesCenterTop: string = '53%';
	gridRight: any = 90;
	gridTop: string = '20%';
	gridWidth: string = '40%';
	gridHeight: string = '70%';
	subtitle1Left: string = '26%';
	subtitle2Left: string = '75%';
	subtitle2Top: any = '50';
	gridLeft: any = 'auto';
	source = '';
	title = 'Innovations by Participant Companies';
	subtitle1 = 'Activities';
	subtitle2 = 'Outputs';
	yTitle = 'type';
	xTitle = 'innovations';
	rawData1: any;
	rawData2: any;
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.seriesCenterLeft = '50%';
				this.seriesCenterTop = '30%';
				this.gridTop = '60%';
				this.gridRight = 83;
				this.gridLeft = 0;
				this.gridWidth = 'auto';
				this.gridHeight = '35%';
				this.subtitle1Left = '43%';
				this.subtitle2Left = '43%';
				this.subtitle2Top = '52%';		
				this.pixelRatio = 2;				
			}
			this.title = this.chartData['title'];
			this.subtitle1 = this.chartData['D4I_Dataset']['dataset1']['title'];
			this.subtitle2 = this.chartData['D4I_Dataset']['dataset2']['title'];
			this.xTitle = this.chartData['D4I_Dataset']['dataset2']['x_axis'];
			this.yTitle = this.chartData['D4I_Dataset']['dataset2']['y_axis'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d4_innovationtype_innovationsubtype_share(this.chartData['D4I_Dataset']);
			this.initEChartJS(chartOptions);
		}
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV13',
			data1: this.rawData1,
			data2: this.rawData2,
			reportTitle: this.chartTitle,
			title: this.title,
			subtitle1: this.subtitle1,
			subtitle2: this.subtitle2,
			source: this.source,
			indicator: 'Economic Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',
			reportTitle: this.chartTitle,			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d4_innovationtype_innovationsubtype_share(dData) {
		var l1_d4_innovationtype_innovationsubtype_share = dData["dataset1"];
		var l1_d4_innovationtype_innovationsubtype_share_2 = dData["dataset2"]["outputs"];
		//console.log('l1_d4_innovationtype_innovationsubtype_share');
		//console.log(l1_d4_innovationtype_innovationsubtype_share);
		this.rawData1 = l1_d4_innovationtype_innovationsubtype_share["activities"];
		this.rawData2 = l1_d4_innovationtype_innovationsubtype_share_2;
		var types = [];
		var subTypes = [];
		var outputs_total = 0;
		var activities_total = 0;
		
		var barData = [];
		var barTypeData = [];
		var barValueData = [];
		
		/*Object.keys(l1_d4_innovationtype_innovationsubtype_share["innovation outputs"]).map((key, index) => {
			outputs_total = outputs_total + l1_d4_innovationtype_innovationsubtype_share['innovation outputs'][key];
			subTypes.push({value:l1_d4_innovationtype_innovationsubtype_share['innovation outputs'][key], name: key});	
			
		});
		types.push({value:outputs_total, name: "innovation outputs"});*/
		Object.keys(l1_d4_innovationtype_innovationsubtype_share["activities"]).map((key, index) => {
			activities_total = activities_total+ l1_d4_innovationtype_innovationsubtype_share['activities'][key];
			subTypes.push({value:l1_d4_innovationtype_innovationsubtype_share['activities'][key], name: key});
			//barData.push([key,l1_d4_innovationtype_innovationsubtype_share["innovation activities"][key]]);	
			//barValueData.push(key);	
			//barData.push(l1_d4_innovationtype_innovationsubtype_share["innovation activities"][key]);		
		});
		
		Object.keys(l1_d4_innovationtype_innovationsubtype_share_2).map((key, index) => {	
			barData.push([key,l1_d4_innovationtype_innovationsubtype_share_2[key]]);	
		});
		types.push({value:1, name: "activities"});
		const colors = [ '#de9ed6', '#ce6dbd', '#a55194', '#e7969c', '#d6616b', '#cedb9c', '#b5cf6b', '#8ca252', '#637939' ];
		barData = barData.sort(function(a,b){return a[1]-b[1]});
		barData.map((single, index)=>{
			barTypeData.push(single[0]);
			barValueData.push({ value:single[1], itemStyle: {color: colors[index]} });
		});
		//var myChart_l1_d4_innovationtype_innovationsubtype_share = echarts.init(document.getElementById('l1_d4_innovationtype_innovationsubtype_share'));
		//console.log(subTypes);
		const chartOptions = {
			title: {
				text: this.title,
			},
			graphic: [{
				type: 'text',
				z: 100,
				left: this.subtitle1Left,
				top: '50',
				style: {
					fill: '#333',
					text: 'Activities',
					font: '16px Poppins'
				}
			},{
				type: 'text',
				z: 100,
				left: this.subtitle2Left,
				top: this.subtitle2Top,
				style: {
					fill: '#333',
					text: 'Outputs',
					font: '16px Poppins'
				}
			},{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			tooltip: {
				trigger: 'item',

			},
			/*legend: {
				orient: 'vertical',
				left: 'right',
				top: 35
			},*/
			grid: {
				//top: 50,
				top: this.gridTop,
				width: this.gridWidth,
				height: this.gridHeight,
				right: this.gridRight,
				left: this.gridLeft,
				containLabel: true
			},
			dataZoom: [{
				type: 'inside',
				orient: 'vertical',
			},
			],
			xAxis: {
				name: this.xTitle,
				type: 'value',
				//boundaryGap: [0, 0.01]
			},
			yAxis: {
				name: this.yTitle,
				type: 'category',
				data: barTypeData,
				axisTick: {
					alignWithLabel: true,
				},
			},
			series: [
			{
				name:'share',
				type:'pie',
				selectedMode: 'single',
				//radius: [0, '30%'],
				radius: [0, '24%'],
				center: [this.seriesCenterLeft, this.seriesCenterTop],
				tooltip: {
					show: false
				},
				label: {
					normal: {
						position: 'center',
						fontSize: 14,
						color: "#fff",
						formatter: function(seriesName) {
							//console.log('seriesName');
							//console.log(seriesName);
							//console.log(seriesName.name);
							var s = seriesName.name.substr(seriesName.name.indexOf(" ") + 1);
							//console.log(s);
							return s;
							
						}
					}
				},
				labelLine: {
					normal: {
						//show: false
					}
				},
				data: types
			},
			{
				name:'share',
				type:'pie',
				//radius: ['40%', '55%'],
				radius: ['30%', '42%'],
				center: [this.seriesCenterLeft, this.seriesCenterTop],
				label: {
					normal: {
						formatter: ' {b|{b}：}{c} ',
						backgroundColor: '#eee',
						borderColor: '#aaa',
						borderWidth: 1,
						borderRadius: 4,
						// shadowBlur:3,
						// shadowOffsetX: 2,
						// shadowOffsetY: 2,
						// shadowColor: '#999',
						// padding: [0, 7],
						rich: {
							a: {
								color: '#999',
								lineHeight: 20,
								align: 'center'
							},
							// abg: {
							//     backgroundColor: '#333',
							//     width: '100%',
							//     align: 'right',
							//     height: 22,
							//     borderRadius: [4, 4, 0, 0]
							// },
							hr: {
								borderColor: '#aaa',
								width: '100%',
								borderWidth: 0.5,
								height: 0
							},
							b: {
								fontSize: 12,
								lineHeight: 26
							},
							per: {
								color: '#eee',
								backgroundColor: '#334455',
								padding: [2, 4],
								borderRadius: 2
							}
						}
					}
				},
				data: subTypes
			},
			{
				type: 'bar',
				data: barValueData
			}
			]			
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['xAxis']['splitNumber'] = 3;
		}		
		return chartOptions;
	}
	
	
	
}