// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';
//import { map } from 'rxjs/operators';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart19',
	templateUrl: './echart19.component.html',
	styleUrls: ['./echart19.component.scss']
})
export class Echart19Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	chartY: any = 65;
	chartY2: any = '10%';
	visualMap1: number = 170;
	visualMap1Top: any = '10%';
	visualMap1Bottom: any = 'auto';
	yTitle = 'Media Buzz';
	xTitle = 'EUR';
	source = '';
	title = 'Correlation between Funding and (Social) Media Buzz';
	rawData: any;
	chartX = '8%';
	chartX2 = 150;
	visualMap1Left = 'right';
	//splitNumberX: any = 'auto';
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.chartX = '25%'
				this.chartX2 = 50;
				this.chartY = 85;
				//this.chartY2 = '15%';
				this.chartY2 = '55%';
				this.visualMap1 = 140;
				this.visualMap1Top = 'auto';
				this.visualMap1Bottom = '50';	
				this.visualMap1Left = '50';	
				//this.splitNumberX = 3;	
				this.pixelRatio = 2;				
			}
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.yTitle = this.chartData['y_axis'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d5_topic_funding(this.chartData['D4I_Dataset']);
			this.initEChartJS(chartOptions);	
        };
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV19',
			data: this.rawData,
			reportTitle: this.chartTitle,
			xAxis: this.xTitle,
			yAxis: this.yTitle,
			title: this.title,
			source: this.source,
			indicator: 'Societal Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',
			reportTitle: this.chartTitle,			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
			
	//Chart options
	l1_d5_topic_funding(dData) {		
		var eu_contribution_per_research_area_over_time =  dData;
		this.rawData = eu_contribution_per_research_area_over_time;
		var contribution_per_research_series = [];
		var topic_data;
		var seriesData = [];
		var single_series = []; 
		var labels = [];
		var mediaBuzz = [];
		var funding = [];
		/*var item4Style = {
			normal: {
				opacity: 0.8,
				shadowBlur: 10,
				shadowOffsetX: 0,
				shadowOffsetY: 0,
				shadowColor: 'rgba(0, 0, 0, 0.1)'
			}
		};*/
		Object.keys(eu_contribution_per_research_area_over_time).map((topic, index) => {		
			topic_data = eu_contribution_per_research_area_over_time[topic];
			//console.log('---topic----');
			//console.log(topic);
			//console.log(topic_data);
			var singleData = [];
			Object.keys(eu_contribution_per_research_area_over_time[topic]).map((innerKey, innerIndex) => {
				if(index==0) {
					labels.push(innerKey);
					
				};
				singleData.push(eu_contribution_per_research_area_over_time[topic][innerKey]);
			});
			
			seriesData.push([singleData[0],singleData[2],singleData[3],singleData[1]]);
			mediaBuzz.push(singleData[2]);
			funding.push(singleData[0]);
			
			//console.log(singleData);
			//seriesData.push( [topic_data["Funding"],topic_data["Social Media Buzz"],topic_data["Number of Projects"],topic_data["topic_name"]]);
			/*Object.keys(topic_data).map((sector, index) => {
				console.log('sector');
				console.log(sector);
				console.log(topic_data[sector]);
				if(single_series[sector]) {
					single_series[sector].data.push([topic,topic_data[sector]]);
				}
				else {
					single_series[sector] = {
						name: sector,
						data: [[topic,topic_data[sector]]], 
						type: 'scatter',
						itemStyle: item4Style
					}
				}
				//console.log(single_series[sector]);
			})	
			console.log(year_data);
			console.log(single_series);		*/			
		});
		//console.log(seriesData);
		//console.log(mediaBuzz);
		//console.log(funding);
		var visualMaxMedia = Math.max(...mediaBuzz);
		var visualMaxFunding = Math.max(...funding);
		var research_area_series = Object.keys(single_series).map(function(key) {
		  return single_series[key];
		});
		single_series = [{
				name: 'sector',
				data: seriesData, 
				type: 'scatter',
				symbolSize: function (data) {
					//return (data[2]*10);
					return (data[2]);
				},
				//itemStyle: item4Style
			}];
		const chartOptions = {
			title: {
				text: this.title
			},
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			grid: {
				x: this.chartX,
				x2: this.chartX2,
				y: this.chartY,
				y2: this.chartY2
			},
			color: ['#c93c6b', '#7e57c2', '#42a5f5', '#26c6da', '#26a69a', '#66bb6a',  '#9ccc65', '#d4e157', '#ffee58', '#ffca28', '#ffa726', '#ff7043'],
			tooltip: {
				padding: 10,
				formatter: function (obj) {				
					var value = obj.value;
					//console.log(value);
					/*return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 14px;padding-bottom: 7px;margin-bottom: 7px">' + value[3].replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>') + '</div>' + 'Funding：' + value[0].toLocaleString() + '<br>' + 'Social Media Buzz：' + value[1].toLocaleString() + '<br>' + 'Number of Projects：' + value[2] + '<br>';*/
					return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 14px;padding-bottom: 7px;margin-bottom: 7px">' + value[3].replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>') + '</div>' + labels[0] + '：' + value[0].toLocaleString() + '<br>' + labels[2] + '：' + value[1].toLocaleString() + '<br>'  + labels[3] + '：' + value[2] + '<br>';
					//return 'sdfgsdfgsdf';	
				}
			},
			dataZoom: [{
				type: 'inside'
			}],
			xAxis: {
				type: 'value',
				name: this.xTitle,
				nameGap: 16,
				nameTextStyle: {
					color: '#222',
					//fontSize: 14
				},
				//min: 2007,
				max: 'dataMax',
				splitLine: {
					show: false
				},
				axisLine: {
					lineStyle: {
						color: '#222'
					}
				},
				//splitNumber: this.splitNumberX,
			},
			yAxis: {
				type: 'value',
				name: this.yTitle,
				nameLocation: 'end',
				nameGap: 20,
				nameTextStyle: {
					color: '#222',
					//fontSize: 16
				},
				axisLine: {
					lineStyle: {
						color: '#222'
					}
				},
				splitLine: {
					show: false
				}
			},
			visualMap: [
				{
					left: this.visualMap1Left,
					top: this.visualMap1Top,
					bottom: this.visualMap1Bottom,
					dimension: 1,
					min: 0,
					//max: 1500000,
					max: visualMaxMedia,
					itemWidth: 20,
					itemHeight: this.visualMap1,
					calculable: true,
					text: ['Media Buzz'],
					textGap: 30,
					textStyle: {
						color: '#222'
					},
					inRange: {
						//symbolSize: [10, 70],
						colorLightness: [0.9, 0.5]
					},
					outOfRange: {
						symbolSize: [70, 70],
						color: ['rgba(255,255,255,.5)']
					},
					controller: {
						inRange: {
							//color: ['#ec407a']
							color: ['#c93c6b']
						},
						outOfRange: {
							color: ['#444']
						}
					},
					formatter: function (value) {
						return parseInt(value).toLocaleString();
					}
				},
				{
					left: 'right',
					bottom: '50',
					dimension: 0,
					min: 0,
					//max: 10000000,
					max: visualMaxFunding,
					itemWidth: 20,
					itemHeight: 150,
					calculable: true,
					precision: 0.1,
					text: ['Funding'],
					textGap: 30,
					textStyle: {
						color: '#222'
					},
					inRange: {
						colorLightness: [0.8, 0.5]
					},
					outOfRange: {
						color: ['rgba(255,255,255,.7)']
					},
					controller: {
						inRange: {
							//color: ['#ec407a']
							color: ['#c93c6b']
						},
						outOfRange: {
							color: ['#444']
						}
					},
					formatter: function (value) {
						return parseInt(value).toLocaleString();
					}
				}

			],
				
			//series: research_area_series      
			series:single_series
		};
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['xAxis']['splitNumber'] = 3;
		}
		return chartOptions;
	}
	
	
	
	
	
	
	
	
	
	
}
