// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

// Layout
import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart18',
	templateUrl: './echart18.component.html',
	styleUrls: ['./echart18.component.scss'],
})
export class Echart18Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	radius1Outer: number = 180;
	radius2Outer: number = 170;
	radius2Inner: number = 30;
	center1: Array<string> = ['25%', '48%'];
	center2: Array<string> = ['75%', '48%'];
	source = '';
	title = 'Clinical Trials Linked to Projects per Research Area';
	rawData: any;
	legendType: string = 'plain';
	legendHeight: any = 'auto';
	orient: string = 'horizontal';
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.radius1Outer = 110;
				this.radius2Outer = 100;
				this.radius2Inner = 10;
				this.legendType = 'scroll';
				this.legendHeight = 115;
				this.orient = 'vertical';
				this.center1 = ['25%', '42%'];
				this.center2 = ['75%', '42%'];
				this.pixelRatio = 2;
			}
			/*this.loadJsonService.getTitle(this.chartTitle);
			this.loadJsonService.getJSON().subscribe(data => {			
				var chartOptions = this.l1_d5_activitytype_trialtype_trials(data);
				this.initEChartJS(chartOptions);
			
			});*/			
			this.title = this.chartData['title'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d5_activitytype_trialtype_trials(this.chartData['D4I_Dataset']);
			this.initEChartJS(chartOptions);
		
        };
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV18',
			data: this.rawData,
			reportTitle: this.chartTitle,
			title: this.title,
			source: this.source,
			indicator: 'Societal Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',
			reportTitle: this.chartTitle,			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
		
	//Chart options
	l1_d5_activitytype_trialtype_trials(dData) {
		var l1_d5_activitytype_trialtype_trials =  dData;
		this.rawData = l1_d5_activitytype_trialtype_trials;
		var abbreviationList = this.chartData["abbreviations"];
		//console.log(abbreviationList);
		var trial_data = [];
		var trial_data_nm = [];
		var activities = [];
		var series = [];
		//console.log('dataMax');
		//console.log(dataMaxArray);

		//console.log(dataMax);
		Object.keys(l1_d5_activitytype_trialtype_trials).map((key, index) => {
			//activities.push({"text": key, max: 19237});		
			activities.push(key);
			trial_data[index] = [];
			Object.keys(l1_d5_activitytype_trialtype_trials[key]).map((inner_key, inner_index) => {
				//trial_data[index].push(l1_d5_activitytype_trialtype_trials[key]);
				trial_data[index].push({value:l1_d5_activitytype_trialtype_trials[key][inner_key], name:inner_key});
			});
		});
		//console.log('trial_data');
		//console.log(trial_data);
		//console.log(activities);

		var seriesData = [];
		//var myChart_l1_d1_year_activitytype_funding = echarts.init(document.getElementById('l1_d5_activitytype_trialtype_trials'));
		var source = 'Source text';
		const chartOptions = {
			title: {
				text: this.title,
				//top: 10,
				left: 10
			},
			color: [ '#bcbddc', '#9e9ac8', '#756bb1', '#c7e9c0', '#a1d99b', '#31a354', '#fdd0a2', '#fdae6b', '#ed9e6a' ,'#dadaeb',  '#fd8d3c', '#e6550d', '#c6dbef', '#3182bd', '#de9ed6', '#ce6dbd', '#a55194', '#e7969c', '#d6616b', '#e7cb94', '#e7ba52', '#8c6d31', '#cedb9c', '#b5cf6b', '#8ca252', '#637939'  ],
			graphic: [
			{
				type: 'text',
				z: 100,
				left: '20%',
				top: '50',
				style: {
					fill: '#333',
					text: activities[0],
					font: '16px Poppins'
				}
			},{
				type: 'text',
				z: 100,
				left: '70%',
				top: '50',
				style: {
					fill: '#333',
					text: activities[1],
					font: '16px Poppins'
				}
			},{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			tooltip : {
				trigger: 'item',
				formatter: function(params) {
					//console.log(params);	
					var label = '';	
					label += params.seriesName.replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>');
					label += '<br>';
					label += params.marker + params.name.replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>') + ': ' + params.value.toLocaleString() + ' (' + params.percent + '%)';
					label += '<br>';
					return label;
				}
				//formatter: "{a} <br/>{b} : {c} ({d}%)"
			},
			legend: {
				//data: year_data,
				type: this.legendType,
				orient: this.orient,
				bottom: '40',
				height: this.legendHeight,
				formatter: function(params) {
					//console.log(params);	
					//console.log(abbreviationList[params]);
					//return params.substring(0, params.indexOf(':'));
					return abbreviationList[params];
				}
			},
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			calculable : true,
			series : [
				{
					name:activities[0],
					type:'pie',
					radius : [20, this.radius1Outer],
					center : this.center1,
					roseType : 'radius',
					label: {
						normal: {
							show: false
						}
					},
					lableLine: {
						normal: {
							show: false
						},
					},
					data: trial_data[0]
				},
				{
					name:activities[1],
					type:'pie',
					radius : [this.radius2Inner, this.radius2Outer],
					center : this.center2,
					roseType : 'area',
					label: {
						normal: {
							show: false
						},
					},
					lableLine: {
						normal: {
							show: false
						},
					},
					data: trial_data[1]
				}
			]
			
		};
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['graphic'][0]['left'] = '10%';
			chartOptions['graphic'][1]['left'] = '60%';
		}
		return chartOptions;
	}
	
	
	
	
}
