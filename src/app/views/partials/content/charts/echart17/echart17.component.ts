// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

// Layout
import { LayoutConfigService } from '../../../../../core/_base/layout';
import { LoadJsonService } from '../../../../../core/_base/layout';
// Charts
import { HttpClient } from '@angular/common/http'; 
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart17',
	templateUrl: './echart17.component.html',
	styleUrls: ['./echart17.component.scss'],
	providers: [LayoutConfigService],
})
export class Echart17Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	//@Input() desc: string;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;

	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private layoutConfigService: LayoutConfigService,  private loadJsonService : LoadJsonService, private http: HttpClient ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.loadJsonService.getTitle(this.chartTitle);
		this.loadJsonService.getJSON().subscribe(data => {			
			//console.log('getJSON funding data');
            //console.log(data);
			var chartOptions = this.l1_d5_medicinetype_medicinesubtype_medicinenumber(data);
			this.initEChartJS(chartOptions);
		
        });	
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	
	//Chart options
	l1_d5_medicinetype_medicinesubtype_medicinenumber(dData) {
		var l1_d5_medicinetype_medicinesubtype_medicinenumber = dData;
		var funders = [];
		var medicineData = [];
		var orphanData = [];
		Object.keys(l1_d5_medicinetype_medicinesubtype_medicinenumber['medicine']).map((key, index) => {
			medicineData.push({value: l1_d5_medicinetype_medicinesubtype_medicinenumber['medicine'][key], name: key});
		});
		Object.keys(l1_d5_medicinetype_medicinesubtype_medicinenumber['orphan drugs']).map((key, index) => {
			orphanData.push({value: l1_d5_medicinetype_medicinesubtype_medicinenumber['orphan drugs'][key], name: key});
		});
			
		//var myChart_l1_d5_funder_nof_citations = echarts.init(document.getElementById('l1_d5_medicinetype_medicinesubtype_medicinenumber'));
		var source = 'Source text';
		const chartOptions = {
			title: {
				text: 'Medicines & Orphan Drugs Linked to Projects',
			},
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			tooltip: {
				trigger: 'item',
				formatter: "{a} <br/>{b} : {c}%",
				
			},
			toolbox: {
				right: 20,
				feature: {
					saveAsImage: {title: 'Save image',}
				}
			},
			/*legend: {
				orient: 'vertical',
				left: 'right',
				top: 30,
			},*/
			calculable: true,
			series: [
				{
					name: 'Medicines',
					type:'funnel',
					width: '40%',
					height: '45%',
					left: '30%',
					top: '5%',
					label: {
						normal: {
							position: 'left',
							fontSize: 16
						}
					},
					data: medicineData
				},
				{
					name: 'Orphan Drugs',
					type:'funnel',
					width: '40%',
					height: '45%',
					left: '30%',
					top: '49%',
					sort: 'ascending',
					label: {
						normal: {
							position: 'left',
							fontSize: 16
						},
						
					},
					data: orphanData
				}
			]

			
		};
		return chartOptions;
	}
	
	
	
	
	
	
	
	
	
	
}
