// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart14',
	templateUrl: './echart14.component.html',
	styleUrls: ['./echart14.component.scss'],
})
export class Echart14Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	xTitle = '';
	yTitle = '';
	source = '';
	title = 'Company Innovativeness & Project Participations by Research Area';
	rawData: any;
	windowWidth: number;
	legendBottom: number = 30;
	gridBottom: number = 60;
	pixelRatio: number = 3;
	showLabelLimit: number = 4;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (this.windowWidth < 800) {
			this.gridBottom = 100;
			this.legendBottom = 20;
			this.pixelRatio = 2;
			this.showLabelLimit = 15;
		}
		if(this.chartData){
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.yTitle = this.chartData['y_axis'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d4_icdclass_rankgroup_shareinrank(this.chartData);
			this.initEChartJS(chartOptions);		
        };
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV14',
			data: this.rawData,
			reportTitle: this.chartTitle,
			xAxis: this.xTitle,
			yAxis: this.yTitle,
			title: this.title,
			source: this.source,
			indicator: 'Academic Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',	
			reportTitle: this.chartTitle,		
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d4_icdclass_rankgroup_shareinrank(dData) {
		var that = this;
		var l1_d4_icdclass_rankgroup_shareinrank =  dData["D4I_Dataset"];
		this.rawData = l1_d4_icdclass_rankgroup_shareinrank;
		var abbreviationList = dData["abbreviations"];
		//var abbreviationList = dData["D4I_Dataset"];
		var abbreviations = [];
		//console.log(l1_d4_icdclass_rankgroup_shareinrank);
		var l1_d2_type = []; 
		var research_areas = [];
		var l1_d2_number_data = [];
		var counterType = 0;
		var counterRArea = 0;
		var numberDatum = 0;
		/*var data1 = [];
		var data10 = [];
		var data20 = [];
		var data50 = [];*/
		var seriesData = [];
		var series = {};
		//var types = ['Most Innovative', 'Highly Innovative', 'Very Innovative', 'Moderately Innovative' ];
		Object.keys(abbreviationList).map((key, index) => {
			//console.log(key);
			//console.log(abbreviationList[key]);
			abbreviations.push(abbreviationList[key]);
		});
		//abbreviations.pop();
		Object.keys(l1_d4_icdclass_rankgroup_shareinrank).map((key, index) => {	
			research_areas.push(key);
			Object.keys(l1_d4_icdclass_rankgroup_shareinrank[key]).map((innerKey, innerIndex) => {
				
				var showLabel = true;
				if(l1_d4_icdclass_rankgroup_shareinrank[key][innerKey]<=this.showLabelLimit) {
					showLabel = false;
				}
				
				if(index==0) {
					series[innerKey] = 
					{
						name: innerKey,
						type: 'bar',
						stack: 'astack',
						label: {
							normal: {
								show: true,
								position: 'insideRight'
							}
						},
						data: []
					}
				};
				seriesData.push([key,l1_d4_icdclass_rankgroup_shareinrank[key][innerKey],innerKey]);
				series[innerKey]['data'].push({value:l1_d4_icdclass_rankgroup_shareinrank[key][innerKey], label: {normal: {show: showLabel,position: 'insideRight'}}});
				/*switch(innerKey) {
					case "Most Innovative":
						//data1.push(l1_d4_icdclass_rankgroup_shareinrank[key][innerKey]);				
						data1.push({value:l1_d4_icdclass_rankgroup_shareinrank[key][innerKey], label: {normal: {show: showLabel,position: 'insideRight'}}});
					break;
					case "Highly Innovative":
						data10.push({value:l1_d4_icdclass_rankgroup_shareinrank[key][innerKey], label: {normal: {show: showLabel,position: 'insideRight'}}});
					break;
				   case "Very Innovative":
						data20.push({value:l1_d4_icdclass_rankgroup_shareinrank[key][innerKey], label: {normal: {show: showLabel,position: 'insideRight'}}});
					break;
					case "Moderately Innovative":
						data50.push({value:l1_d4_icdclass_rankgroup_shareinrank[key][innerKey], label: {normal: {show: showLabel,position: 'insideRight'}}});
					break;
				}*/ 
				
			});
			counterRArea++;
		});
		
		var seriesChart = [];
		Object.keys(series).map((key, index) => {
			seriesChart.push(series[key]);
		});
		const chartOptions = {
			title: {
				text: this.title
			},
			color: ['#3a6064','#4d8086','#61a0a8','#80b3b9'],	
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			dataZoom: [{
				type: 'inside',
				orient: 'vertical',
			}],
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			tooltip : {
				trigger: 'axis',
				axisPointer : { 
					type : 'shadow'    
				},
				formatter: function (params) {
					var label = '';				
					//console.log(params);
					//var fullName = Object.keys(abbreviationList).find(key => abbreviationList[key] === params[0]['axisValue']);
					var fullName = params[0]['name'].replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>');;
					label += fullName;
					label += '<br>';
					params.map((single)=> {			
						label += single.marker + " " + single.seriesName + " " + single.value;
						label += '<br>';
					});
					return label;
				},
			},
			legend: {
				bottom: this.legendBottom,
			},
			grid: {			
				left: '3%',
				right: '7%',
				bottom: this.gridBottom,
				containLabel: true
			},
			xAxis:  {
				name: this.xTitle,
				type: 'value'
			},
			yAxis: {
				type: 'category',
				name: this.yTitle,
				data: research_areas,
				//data: abbreviations,
				axisLabel: {
					formatter: (function(value){
						//console.log(value);
						if (that.windowWidth < 800) {
							//return abbreviationList[value].substring(0, abbreviationList[value].indexOf(':'));
							return '';
						} else {
							return abbreviationList[value];
						}
					})
				},
			},
			series : seriesChart,
			/*series: [
				{
					name: 'Most Innovative',
					type: 'bar',
					stack: 'astack',
					label: {
						normal: {
							show: true,
							position: 'insideRight'
						}
					},
					data: data1
				},
				{
					name: 'Highly Innovative',
					type: 'bar',
					stack: 'astack',
					label: {
						normal: {
							show: true,
							position: 'insideRight'
						}
					},
					data: data10
				},
				{
					name: 'Very Innovative',
					type: 'bar',
					stack: 'astack',
					label: {
						normal: {
							show: true,
							position: 'insideRight'
						}
					},
					data: data20
				},
				{
					name: 'Moderately Innovative',
					type: 'bar',
					stack: 'astack',
					label: {
						normal: {
							show: true,
							position: 'insideRight'
						}
					},
					data: data50
				},
			]*/
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
		}	
		
		
		return chartOptions;
	}
	
	
	
	
	
	
	
	
	
	
}
