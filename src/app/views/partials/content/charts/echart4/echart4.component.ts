// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart4',
	templateUrl: './echart4.component.html',
	styleUrls: ['./echart4.component.scss'],
})
export class Echart4Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	//@Input() desc: string;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	gridRight: number = 290;
	gridBottom: number = 75;
	legendTop: any = 47;
	legendBottom: any = 'auto';
	sliderBottom: number = 35;
	legendType: string = 'plain';
	legendHeight: any = 420;
	legendRight: any = 0;
	legendWrap: boolean = true;
	yTitle = 'EUR';
	xTitle = 'year';
	source = '';
	title = 'EU Contribution per Research Area';
	rawData: any;
	windowWidth: number;
	pixelRatio: number = 3;
	
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.gridRight = 40;
				this.gridBottom = 165;
				this.legendTop = 'auto';
				this.legendBottom = 30;
				this.sliderBottom = 130;
				this.legendType = 'scroll';
				this.legendHeight = 83;
				this.legendRight = 'auto';
				this.legendWrap = false;
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.yTitle = this.chartData['y_axis'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d1_year_researcharea_funding(this.chartData);
			this.initEChartJS(chartOptions);		
        };
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV4',
			data: this.rawData,
			reportTitle: this.chartTitle,
			xAxis: this.xTitle,
			yAxis: this.yTitle,
			title: this.title,
			source: this.source,
			indicator: 'Input',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}	
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',
			reportTitle: this.chartTitle,			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d1_year_researcharea_funding(dData) {
		var that = this;
		var l1_d1_year_researcharea_funding = dData["D4I_Dataset"];
		this.rawData = l1_d1_year_researcharea_funding;
		var abbreviationList = this.chartData["abbreviations"];
		//var abbreviationList = this.chart4["abbreviations"];
		var research_areas = [];
		var ccc = 0;
		var seriesData = [];
		var series = {};
		var year_data = [];
		/*Object.keys(l1_d1_year_researcharea_funding['2011']).map((key, index) => {
			series[key] = {
				name: key,
				type: 'line',
				stack: 'main-stack',
				areaStyle: {},
				symbolSize: 3,
				data:[]
			};
		});	*/
		Object.keys(l1_d1_year_researcharea_funding).map((key, index) => {
			year_data.push(key);
			
			Object.keys(l1_d1_year_researcharea_funding[key]).map((innerKey, innerIndex) => {
				if(index==0) {
					series[innerKey] = 
					{
						name: innerKey,
						type: 'line',
						stack: 'main-stack',
						areaStyle: {},
						symbolSize: 3,
						data:[]
					};
				}
				seriesData.push([key,l1_d1_year_researcharea_funding[key][innerKey],innerKey]);
				series[innerKey]['data'].push(l1_d1_year_researcharea_funding[key][innerKey]);
				if (!research_areas.includes(innerKey)) {
					research_areas.push(innerKey);
				}
			});
			

		});
		var seriesChart = [];
		Object.keys(series).map((key, index) => {
			seriesChart.push(series[key]);
		});
		//console.log(seriesChart);

		const chartOptions = {
			title: {
				text: this.title
			},
			color: [ '#bcbddc', '#9e9ac8', '#756bb1', '#c7e9c0', '#a1d99b', '#31a354', '#fdd0a2', '#fdae6b',  '#fd8d3c', '#e6550d', '#c6dbef', '#3182bd', '#de9ed6', '#ce6dbd', '#a55194', '#e7969c', '#d6616b', '#e7cb94', '#e7ba52', '#8c6d31', '#cedb9c', '#b5cf6b', '#8ca252', '#637939',  '#9c9ede',  '#6b6ecf', '#393b79', '#17becf', '#9edae5', '#dbdb8d', '#c7c7c7', '#e377c2', '#f7b6d2', '#8c554a', '#c49c94', '#c5b0d5', '#d62728', '#ff9896', '#2ca02c', '#98df8a', '#ff7f0e', '#ffbb78' ],	
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],	
			tooltip : {
				formatter: function(params) {
					var label = '';	
					label += params.seriesName.replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>');
					//label += abbreviationList[params.seriesName];
					label += '<br>';
					label += params.marker + params.value.toLocaleString();
					label += '<br>';
					return label;
				}
			},
			legend: {
				data:research_areas,
				type: 'scroll',
				orient: 'vertical',
				y: 'top',
				top: this.legendTop,
				bottom: this.legendBottom,
				height: this.legendHeight,
				right: this.legendRight,
				formatter: function(params) {
					//console.log('params');
					//console.log(params);
					//return params.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
					if(abbreviationList[params]) {						
						if (that.legendWrap) {
							return (abbreviationList[params]).replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
						}
						return abbreviationList[params];
						
					} else {
						return abbreviationList[params];
					}
				}
			},
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			grid: {
				left: '1%',
				//right: '4%',
				right: this.gridRight,
				bottom: this.gridBottom,
				containLabel: true
			},
			dataZoom: [{
				type: 'slider',
				start: 0,
				end: 100,
				bottom: this.sliderBottom
			}, {
				type: 'inside'
			}],
			xAxis : [
				{
					type : 'category',
					name: this.xTitle,
					boundaryGap : false,
					data : year_data
				}
			],
			yAxis : [
				{
					type : 'value',
					name: this.yTitle,
				}
			],
			series : seriesChart,			
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['grid']['top'] = 90;
		}
		return chartOptions;
	}
	
	
}
