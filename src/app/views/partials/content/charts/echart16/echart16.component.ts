// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

// Layout
import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart16',
	templateUrl: './echart16.component.html',
	styleUrls: ['./echart16.component.scss'],
})
export class Echart16Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	xTitle = '';
	yTitle1 = 'Publications';	
	yTitle2 = 'Guidelines';
	source = '';
	title = 'Number of Clinical Guidelines Citing Project Publications';
	rawData: any;
	filterData: Array<Object> = [
		{
		  display: '11One',
		  value: '1'
		}
	];
	ClickCounter: any;
	chartOptions: any;
	funders = [];
	uptake_score = [];
	number_of_citations = []
	number_of_guidelines = [];
	sliderEnd: number = 63;
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}
	
	countChangedHandler(filteredData) {
		this.ClickCounter = filteredData;
		//console.log(filteredData);
		//console.log(this.companies);
		var indices = [];
		var newNumberOfCitations = [];
		var newnumberOfGuidelines = [];
		if(filteredData=='all') {
				this.chartOptions.series[0]["data"] = this.number_of_citations;
				this.chartOptions.series[1]["data"] = this.number_of_guidelines;
				this.chartOptions.xAxis.data = this.funders;
		} else {
			(filteredData).map((single) => {
				indices.push(this.funders.indexOf(single));
			});
			//console.log('indices');
			//console.log(indices);
			newNumberOfCitations = (indices).map(i => this.number_of_citations[i]);
			newnumberOfGuidelines = (indices).map(i => this.number_of_guidelines[i]);
			//newUptakeScore.push();
			/*(filteredData).map((single) => {
				indices.push(this.funders.indexOf(single));
			});*/
			//console.log(newNumberOfCitations);
			//console.log(this.chartOptions);
			this.chartOptions.series[0]["data"] = newNumberOfCitations;
			this.chartOptions.series[1]["data"] = newnumberOfGuidelines;
			//console.log(this.chartOptions.series[0]["data"]);
			this.chartOptions.xAxis.data = filteredData;
		}
		//console.log(this.chartOptions);
		this.initEChartJS(this.chartOptions);
	}
	
	resetChartHandler() {
		this.countChangedHandler('all');
		this.initEChartJS(this.chartOptions);
	}
	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (this.windowWidth < 800) {
			this.sliderEnd = 20;
			this.pixelRatio = 2;
		}
		if(this.chartData){			
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.yTitle1 = this.chartData['y_axis1'];
			this.yTitle2 = this.chartData['y_axis2'];
			this.source = this.chartData['source'];
			this.chartOptions = this.l1_d5_funder_nof_citations(this.chartData['D4I_Dataset']);
			this.initEChartJS(this.chartOptions);
		
        };
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV16',
			data: this.rawData,
			reportTitle: this.chartTitle,
			xAxis: this.xTitle,
			title: this.title,
			source: this.source,
			indicator: 'Societal Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}	
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',	
			reportTitle: this.chartTitle,		
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d5_funder_nof_citations(dData) {
		var l1_d5_funder_nof_citations = dData;
		this.rawData = l1_d5_funder_nof_citations;
		var companiesFilterData = [];
		var abbreviationList = this.chartData["abbreviations"];
		var labels = this.chartData["labels"];
		var abbreviations = [];
		//funders = Object.keys(l1_d5_funder_nof_citations).sort(function(a,b){return l1_d5_funder_nof_citations[b]-l1_d5_funder_nof_citations[a]});
		//number_of_citations = Object.keys(l1_d5_funder_nof_citations).sort(function(a,b){return l1_d5_funder_nof_citations[b]-l1_d5_funder_nof_citations[a]}).map(key => l1_d5_funder_nof_citations[key]);
		Object.keys(l1_d5_funder_nof_citations).map((key, index) => {
			this.funders.push(key);
			companiesFilterData.push({id:key, itemName:key});
			//number_of_citations.push(l1_d5_funder_nof_citations[key]);
			this.number_of_citations.push(l1_d5_funder_nof_citations[key]['publications']);
			this.number_of_guidelines.push(l1_d5_funder_nof_citations[key]['guidelines']);
		});
		this.filterData = companiesFilterData;
		Object.keys(abbreviationList).map((key, index) => {
			//console.log(key);
			//console.log(abbreviationList[key]);
			abbreviations.push(abbreviationList[key]);
		});
		//var myChart_l1_d5_funder_nof_citations = echarts.init(document.getElementById('l1_d5_funder_nof_citations'));
		const chartOptions = {
			title: {
				text: this.title,
			},
			//color: ['#00838f','#bb838f'],
			color: ["#d48265", "#61a0a8"],
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			toolbox: {
				right: 40,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'cross'
				},
				formatter: function (params) {
					var label = '';				
					//console.log(params);
					label += params[0]['name'].replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>');
					label += '<br>';
					params.map((single)=> {
						//console.log('single');
						//console.log(single);
						label += single.marker + single.seriesName + ': ' + single.value.toLocaleString();
						label += '<br>';
					});
					return label;
				},
			},
			dataZoom: [{
				type: 'slider',
				start: 0,
				end: this.sliderEnd,
				bottom: 65
			}, {
				type: 'inside',
				start: 0,
				end: 100
			}],
			grid: {
				left: '3%',
				right: '4%',
				bottom: '100',
				containLabel: true,
			},
			legend: {
				bottom: 30
			},
			xAxis: {
				type: 'category',
				name: this.xTitle,
				//boundaryGap: [0, 0.01]
				data: this.funders,
				axisTick: {
					alignWithLabel: true,
					interval: 0
				},
				axisLabel: {
					showMinLabel: true, 
					interval: 0,
					rotate: 33,
					formatter: (function(value){
						//console.log(value);
						return abbreviationList[value];
					})
				},
				splitNumber: 0,
				//boundaryGap : false,
		
			},
			yAxis: [{
				name: this.yTitle1,
				axisTick: {
					alignWithLabel: true,
					//interval: 0
				}				
			},
			{
				type: 'value',
				name: this.yTitle2,
				min: 0,
				//max: 125,
				position: 'right',
				axisLine: {
					lineStyle: {
						
					}
				},
				splitLine: {
					show: false,
				}
				/*axisLabel: {
					formatter: '{value} °C'
				}*/
			}],
			series: [
				{
					type: 'bar',
					name: labels.publications,
					data: this.number_of_citations
				},
				{
					type: 'bar',
					name: labels.guidelines,
					yAxisIndex: 1,
					data: this.number_of_guidelines
				},
			]

			
		};
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['grid']['top'] = 90;
			chartOptions['toolbox']['right'] = 10;
		}
		return chartOptions;
	}
	
	
}
