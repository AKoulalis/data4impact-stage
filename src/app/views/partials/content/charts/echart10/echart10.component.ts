// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart10',
	templateUrl: './echart10.component.html',
	styleUrls: ['./echart10.component.scss'],
})
export class Echart10Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	xTitle = '%';
	source = '';
	title = 'Topic Strength - Field vs EC (most common topics)';
	rawData: any;
	gridBottom = '60';
	windowWidth: number;
	//splitNumberX: any = 'auto';
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.gridBottom = '100';
				//this.splitNumberX = 3;
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d3_topic_strengthDB_strengthprojects(this.chartData['D4I_Dataset']);
			this.initEChartJS(chartOptions);
		};
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV10',
			data: this.rawData,
			reportTitle: this.chartTitle,
			xAxis: this.xTitle,
			title: this.title,
			source: this.source,
			indicator: 'Academic Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}

	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',
			reportTitle: this.chartTitle,			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d3_topic_strengthDB_strengthprojects(dData) {
		var l1_d3_topic_strengthDB_strengthprojects = dData;
		this.rawData = l1_d3_topic_strengthDB_strengthprojects;
		var topics = [];
		//var strength_in_DB = [];
		//var strength_in_projects = [];
		var seriesData = [];
		var series = {};
		Object.keys(l1_d3_topic_strengthDB_strengthprojects).map((key, index) => {
			topics.push(key);
			Object.keys(l1_d3_topic_strengthDB_strengthprojects[key]).map((innerKey, innerIndex) => {
				if(index==0) {
					series[innerKey] = 
					{
						name: innerKey,
						type: 'bar',
						data:[]
					};
				}
				seriesData.push([key,l1_d3_topic_strengthDB_strengthprojects[key][innerKey],innerKey]);
				series[innerKey]['data'].push(l1_d3_topic_strengthDB_strengthprojects[key][innerKey]);
			});
			//strength_in_DB.push(l1_d3_topic_strengthDB_strengthprojects[key]['strength_in_DB']);
			//strength_in_projects.push(l1_d3_topic_strengthDB_strengthprojects[key]['strength_in_projects']);	
		})
		var seriesChart = [];
		Object.keys(series).map((key, index) => {
			seriesChart.push(series[key]);
		});
		//var myChart_l1_d2_type_number_of_innovations = echarts.init(document.getElementById('l1_d3_topic_strengthDB_strengthprojects'));
		var chartOptions = {
			title: {
				text: this.title,
			},
			//color: ['#003070','#008ac1'],
			color: ["#d48265", "#61a0a8"],
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'shadow'
				}
			},
			legend: {
				//top: 'bottom',
				padding: [25,1,1,1],
				height: 100,
				bottom: 30,		
			},
			grid: {
				left: '3%',
				right: '4%',
				bottom: this.gridBottom,
				containLabel: true,
			},
			xAxis: {
				name: this.xTitle,
				type: 'value',
				//splitNumber: this.splitNumberX,
			},
			yAxis: {
				type: 'category',
				data: topics,
				axisTick: {
					alignWithLabel: true,
				},
				axisLabel: {
					interval : 0,
					formatter: (params) => {
						//console.log(params);
						if (this.windowWidth < 800) {
							return params.replace(/(.*?\s.*?\s)/g, '$1'+'\n');
						} else {
							return params.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
						}
					}
				},
			},
			dataZoom: [{
				type: 'inside',
				//xAxisIndex: 0,
				orient: 'vertical',
			}],
			series: seriesChart
			/*series: [
				{
					type: 'bar',
					name: 'Strength in Field',
					data: strength_in_DB
				},
				{
					type: 'bar',
					name: 'Strength in EC Projects',
					data: strength_in_projects
				}
			]*/		
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['grid']['top'] = 80;
			chartOptions['xAxis']['splitNumber'] = 3;
		}
		return chartOptions;
	}
	
	
	
	
	
	
	
	
	
}
