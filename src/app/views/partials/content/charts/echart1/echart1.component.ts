// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

//import { LoadJsonService } from '../../../../../core/_base/layout';
import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;

@Component({
	selector: 'kt-echart1',
	templateUrl: './echart1.component.html',
	styleUrls: ['./echart1.component.scss'],
	//providers: [LayoutConfigService],
})
export class Echart1Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	//@Input() desc: string;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart1', {static: true}) echart: ElementRef;
	chart: any;
	eu_contribution_total_cost_per_year: any;
	gridRight: any = '4%';
	yTitle = 'EUR';
	xTitle = 'year';
	source = '';
	title = 'EU Contribution & Total Cost of Projects';
	pixelRatio: number = 3;
	windowWidth: number;

	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private router: Router, private downloadJSONService : DownloadJSONService ) {
		
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		/*this.loadJsonService.getTitle(this.chartTitle);
		this.loadJsonService.getJSON().subscribe(data => {			
			console.log('getJSON data');
            console.log(data);
			var chartOptions = this.loadJsonService.getChartOptions(this.chartTitle, data);
			this.initEChartJS(chartOptions);
        });*/

		//console.log('chartData');
		//console.log(this.chartData);
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.gridRight = '7%';
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.yTitle = this.chartData['y_axis'];
			this.source = this.chartData['source'];			
			var chartOptions = this.l1_d1_funding_year(this.chartData['D4I_Dataset']);			
			this.initEChartJS(chartOptions);
			
			
			/*this.loadJsonService.getTitle(this.chartTitle);
			this.loadJsonService.getJSON().subscribe(data => {			
				//console.log('getJSON data');
				//console.log(data);
				var chartOptions = this.l1_d1_funding_year(data);
				this.initEChartJS(chartOptions);
			});
			*/
		}
		
	}

	/** Init chart */
	initEChartJS(options) {
		//const chart = echarts.init(this.echart.nativeElement);
		this.chart = echarts.init(this.echart.nativeElement);
		this.chart.setOption(options);
	}
	
	
	downloadCSV() {
		console.log('icon clicked');
		console.log(this.eu_contribution_total_cost_per_year);	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV1',
			data: this.eu_contribution_total_cost_per_year,
			reportTitle: this.chartTitle,
			title: this.title,
			xAxis: this.xTitle,
			yAxis: this.yTitle,
			source: this.source,
			indicator: 'Input',
			filters: this.filters.join(', ')
		};	
		//this.JSONToCSVConvertor(fileContents, 'title', false);
		this.downloadCSVService.JSONToCSVConvertor(fileContents);
		
	}
	
	downloadJSON() {
		console.log('JSON icon clicked');
		var filters = ["Field: Health","Time Range: 2005 - 2010"];
		var fileContents = 	{
			json: 'JSONTable',	
			reportTitle: this.chartTitle,		
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	loadInstructions() {
		console.log('load clicked');
		this.router.navigate(['/instructions']);
	}
	/*
	showTooltip(){
		//console.log('tooltip');
		this.chart.setOption({
			graphic: {
				id: 'download_tooltip',
				invisible: false
			}
		});
	}
	
	hideTooltip(){
		this.chart.setOption({
			graphic: {
				id: 'download_tooltip',
				invisible: true	
			}
		});
	}*/
	
	//Chart options
	
	l1_d1_funding_year(dData) {
		//var eu_contribution_total_cost_per_year =  dData["Sheet1"];
		//var eu_contribution_total_cost_per_year = dData["D4I_Dataset"];
		var eu_contribution_total_cost_per_year = dData;
		var abbreviationList = this.chartData["abbreviations"];
		this.eu_contribution_total_cost_per_year = eu_contribution_total_cost_per_year;
		var labels = [];
		//var eu_funding = []; 
		//var total_funding = []; 
		var funding_1 = [];
		var funding_2 = [];
		var year_data = [];
		var ccc = 0
		Object.keys(eu_contribution_total_cost_per_year).map((key, index) => {
			year_data.push(key);
			Object.keys(eu_contribution_total_cost_per_year[key]).map((innerKey, innerIndex) => {
				if(index==0) {
					labels.push(innerKey);
				}
				if(innerIndex==0) {
					funding_1.push(eu_contribution_total_cost_per_year[key][innerKey]);
				} else if(innerIndex==1)  {
					funding_2.push(eu_contribution_total_cost_per_year[key][innerKey]);
				}
			})
			/*eu_funding.push(eu_contribution_total_cost_per_year[key]["EU funding"]);
			total_funding.push(eu_contribution_total_cost_per_year[key]["Total funding"]);
			*/
		})
			
		//var myChart_l1_d1_funding_researcharea_year = echarts.init(document.getElementById('eu_contribution_total_cost_per_year'));
		var source = this.source;
		var chartOptions = {
			title: {
				text: this.title
			},
			color: ["#61a0a8", "#a0c6ca"],
			textStyle: {
				fontFamily: 'Poppins',
			},
			graphic: [
				{
					type: 'text',
					z: 100,
					//left: 'center',
					top: 'bottom',
					style: {
						fill: '#333',
						text: [
							'Description/Source: '+ source
						].join('\n'),
						//font: '14px Microsoft YaHei'
					}
				},
				/*{
					type: 'image',
					id: 'logo',
					right: 150,
					top: 'top',
					z: -10,
					bounding: 'raw',
					origin: [75, 75],
					style: {
						image: '/assets/media/files/csv.svg',
						width: 20,
						height: 30,
						opacity: 0.4
					},
					onclick:(evt)=> {
						console.log(evt.target.id);
						console.log(this);
						this.downloadCSV()
					},
					onmouseover:(evt)=> {
						this.showTooltip()
					},
					onmouseout:(evt)=> {
						this.hideTooltip()
					},
					
				},
				{
					type: 'text',
					id: 'download_tooltip',
					right: 25,
					top: 31,
					z: 100,
					style: {
						fill: '#078fcb',
						text: [
							'Download CSV',
						].join('\n'),
						font: '10px Microsoft YaHei'
					},
					invisible: true					
				}*/
			],
			tooltip : {
				trigger: 'axis',
				/*axisPointer: {
					type: 'cross',
					label: {
						backgroundColor: '#6a7985'
					}
				}*/
			},
		    legend: {
				//data:['EU funding','Total funding'],
				data:labels,
				bottom: '15px',
				formatter(params) {
					//return params.replace(/ .*/,'');
					return abbreviationList[params].replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
				}
			},
			toolbox: {
				right: 10,
				top: 'top',
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
					/*myToolIfo: {
						show: true,
						title: 'Info',	
						icon: 'image:///assets/media/icons/index.png',
						onclick:(evt)=> {
							//console.log(this);
							this.loadInstructions();
						},
					}*/
				}
			},
			grid: {
				left: '3%',
				right: this.gridRight,
				bottom: '90px',
				containLabel: true
			},
			dataZoom: [{
				type: 'slider',
				start: 0,
				end: 100,
				bottom: 45
			}, {
				type: 'inside'
			}],
			xAxis : [			
				{
					name: this.xTitle, 
					type : 'category',
					boundaryGap : false,
					data : year_data
				}
			],
			yAxis : [				
				{
					name: this.yTitle,
					type : 'value'
				}
			],
			series : [
			{
				//name: 'EU funding',
				name: labels[0],
				type: 'line',
		   
				areaStyle: {},
				itemStyle: {
					normal: {
					}
				},
				//data: eu_funding,
				data: funding_1,
				z: 10
			},
			{
				//name: 'Total funding',
				name: labels[1],
				type: 'line',
		 
				areaStyle: {},
				itemStyle: {
					normal: {
					}
				},
				//data: total_funding
				data: funding_2
			}
			]
	
		};
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['grid']['top'] = 90;
		}
		return chartOptions;
	}
	
	
	
	
	
	
	
	
	
	
	
}
