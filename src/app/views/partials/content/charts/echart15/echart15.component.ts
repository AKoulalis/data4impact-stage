// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';
import {FormGroup} from '@angular/forms';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;

//Autocomplete Chips
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

@Component({
	selector: 'kt-echart15',
	templateUrl: './echart15.component.html',
	styleUrls: ['./echart15.component.scss']
})
export class Echart15Component implements OnInit {
	filterData: Array<Object> = [
		{
		  display: '11One',
		  value: '1'
		}
	];
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	source = '';
	title = 'Company Uptake Score';
	chartOptions: any;
	companies = [];
	uptake_score = [];
	rawData: any;
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	countChangedHandler(filteredData) {
		//console.log(filteredData);
		//console.log(this.companies);
		var indices = [];
		var newUptakeScore = [];
		if(filteredData=='all') {
			this.rawData.sort((a,b) => (a['Company Name'] > b['Company Name']) ? 1 : ((b['Company Name'] > a['Company Name']) ? -1 : 0)); 
			this.rawData.map((key) => {
				indices.push(key['Company Name']);
				newUptakeScore.push(key['Uptake Score']);
			});
			this.chartOptions.angleAxis.data = indices;
		} else {			
			(filteredData).map((single) => {
				indices.push(this.companies.indexOf(single));
			});
			//console.log('indices');
			//console.log(indices);
			newUptakeScore = (indices).map(i => this.uptake_score[i]);
			//newUptakeScore.push();
			(filteredData).map((single) => {
				indices.push(this.companies.indexOf(single));
			});
			this.chartOptions.angleAxis.data = filteredData;
		}
		//console.log(newUptakeScore);
		//console.log(this.chartOptions);
		this.chartOptions.series[0]["data"] = newUptakeScore;
		//console.log(this.chartOptions.series[0]["data"]);		
		//console.log(this.chartOptions);
		this.initEChartJS(this.chartOptions);
	}

	resetChartHandler() {
		this.countChangedHandler('all');
		this.initEChartJS(this.chartOptions);
	}
	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.source = this.chartData['source'];
			this.chartOptions = this.l1_d4_uptake_score_organization(this.chartData['D4I_Dataset']);
			this.initEChartJS(this.chartOptions);
		
		};
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV15',
			data: this.rawData,
			reportTitle: this.chartTitle,
			title: this.title,
			source: this.source,
			indicator: 'Economic Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',
			reportTitle: this.chartTitle,			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
		
	//Chart options
	l1_d4_uptake_score_organization(dData) {
		var l1_d4_uptake_score_organization_new =  dData;
		this.rawData = l1_d4_uptake_score_organization_new;
		var companiesFilterData = [];
		l1_d4_uptake_score_organization_new.sort((a,b) => (a['Company Name'] > b['Company Name']) ? 1 : ((b['Company Name'] > a['Company Name']) ? -1 : 0)); 
		l1_d4_uptake_score_organization_new.map((key) => {
			//var dividedText = key['Company Name'].replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			//this.companies.push(dividedText);
			this.companies.push(key['Company Name']);
			//companiesFilterData.push({display:dividedText, value:dividedText});
			//companiesFilterData.push({id:dividedText, itemName:dividedText});
			companiesFilterData.push({id:key['Company Name'], itemName:key['Company Name']});
			this.uptake_score.push(key['Uptake Score']);
		});
		this.filterData = companiesFilterData;
		//console.log(companiesFilterData);
		//var myChart_l1_d4_uptake_score_organization = echarts.init(document.getElementById('l1_d4_uptake_score_organization_new'));
		var source = 'Source text';
		const chartOptions = {
			title: {
				text: this.title,
			},
			//color: ['#00838f'],
			color: ['#61a0a8'],
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
				}
			},],
			toolbox: {
				right: 40,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			dataZoom: [{
				type: 'inside'
			}],
			grid: {
				top: 100
			},
			angleAxis: {
				type: 'category',
				data: this.companies,
				axisTick: {
					alignWithLabel: true,
					//interval: 10
					show: false,
				},
				axisLabel: {
					show: false,
					margin: 24,
					align: 'right',
					verticalAlign: 'bottom',
					rich: {
						a: {
							lineHeight: 10,
							height: 50,
							backgroundColor: '#aaa',
							verticalAlign: 'bottom',
							rotate: 20,
						},

					},
					interval: 30,
					rotate: 10,
				},
			},
			
			tooltip: {
				show: true,
				trigger: 'axis',
				axisPointer : {  
					type : 'shadow'      
				},
				formatter: function (params) {
					var label = '';				
					//console.log(params);
					var single = params[0];
					var fullName = single.name.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'<br>');
					label += fullName;
					label += '<br>';				
					//console.log('single');
					//console.log(single);						
					label += single.marker + " " + single.value;
					
					return label;
				},
			},
			radiusAxis: {
				
			},
			polar: {
			},
			series: [{
				type: 'bar',
				itemStyle: {
					normal: {
						//color: 'transparent'
					}
				},
				data: this.uptake_score,
				coordinateSystem: 'polar',
			}],

			
		};	
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['toolbox']['right'] = 10;
		}	
		return chartOptions;
	}
	
	
	
	
	
	
	
	
	
	
}
