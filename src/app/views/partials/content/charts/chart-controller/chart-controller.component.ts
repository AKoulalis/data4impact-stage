// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
//import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
	selector: 'kt-chart-controller',
	templateUrl: './chart-controller.component.html',
	styleUrls: ['./chart-controller.component.scss'],
})
export class ChartControllerComponent implements OnInit {
    selectedItems = [];
    dropdownSettings = {};
	showResetButton: Boolean = false;
	@Output() countChanged: EventEmitter<any> = new EventEmitter();
	@Output() resetChart: EventEmitter<any> = new EventEmitter();
	@Input() chartOptions:Array<Object> = [];
	profileForm = new FormGroup({
		//selected: new FormControl(['1', '2', '3'])
		selected: new FormControl()
	});
	
	formModel = {
		name: '',
        skills: [{ "id": 1, "itemName": "Angular" }]
    };
	
	/**
	 * Component constructor
	 *
	 */
	constructor( ) {
		
	}
	
	toggleSelect() {
		//console.log('toggled');
		var autoElement = document.getElementById("wertwet") as any;
		//console.log(autoElement);
		var elements = autoElement.options;
		for(var i = 0; i < elements.length; i++){
		  elements[i].selected = false;
		}
	}

	ngOnInit() {
		//console.log('ChartControllerComponent ngOnInit');
		/*console.log(this.multiSelect);*/
		//console.log(this.chartOptions);
		/*console.log(this.chartOptions2);
		for (var i=1; i<420; i++) {
			(this.chartOptions2).push({display: i,itemName: i});
		}*/
		
        this.selectedItems = [
			/*{"id":2,"itemName":"Singapore"},
			{"id":5,"itemName":"South Korea"}*/
		];
        this.dropdownSettings = { 
			singleSelection: false, 
			text:"Filter",
			selectAllText:'Select All',
			unSelectAllText:'UnSelect All',
			enableSearchFilter: true,
			classes:"myclass custom-class",
			badgeShowLimit: 3
		};            
		
	}
	
	onItemSelect(item:any){
        console.log(item);
        console.log(this.selectedItems);
    }
    OnItemDeSelect(item:any){
        console.log(item);
        console.log(this.selectedItems);
    }
    onSelectAll(items: any){
        console.log(items);
    }
    onDeSelectAll(items: any){
        console.log(items);
    }

	onSubmit() {
		//console.log(this.profileForm.value);
		//this.countChanged.emit(this.profileForm.value.selected);		
		console.log(this.selectedItems);
		let selected = this.selectedItems.map(single => {
			return single.itemName;
		});
		console.log('selected');
		console.log(selected);
		this.countChanged.emit(selected);
		this.showResetButton = true;
	}

	onResetChart() {
		this.selectedItems = [];
		this.resetChart.emit();
		this.showResetButton = false;
	}

	
}
