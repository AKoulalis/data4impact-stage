// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart12',
	templateUrl: './echart12.component.html',
	styleUrls: ['./echart12.component.scss'],
})
export class Echart12Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	legendTop: any = 'auto';
	legendBottom: any = 22;
	source = '';
	title = 'Emerging Topics: % per Funder after 2015';
	rawData: any;
	radius: any = '80%';
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.legendTop = 'auto';
				this.legendBottom = 30;
				this.radius = '55%';
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d3_researcharea_patenttype_nofpatents(this.chartData['D4I_Dataset']);
			this.initEChartJS(chartOptions);
		}
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV12',
			data: this.rawData,
			reportTitle: this.chartTitle,
			title: this.title,
			source: this.source,
			indicator: 'Academic Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}

	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',	
			reportTitle: this.chartTitle,		
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
		
	//Chart options
	l1_d3_researcharea_patenttype_nofpatents(dData) {
		var l1_d3_researcharea_patenttype_nofpatents = dData;
		this.rawData = l1_d3_researcharea_patenttype_nofpatents;
		var research_areas = [];
		//var research_areas_nums = [];
		var research_areas_text = [];
		var counterRArea = 0;
		var dataMaverics = [];
		var dataAdopters = [];
		var dataEnablers = [];
		var dataPioneers = [];
		var series = {};
		// We do this because this order has been requested.
		var types = [ 'Adopters', 'Enablers', 'Pioneers', 'Maverics' ];
		//var types = [ 'Maverics', 'Pioneers', 'Enablers', 'Adopters' ];
		Object.keys(l1_d3_researcharea_patenttype_nofpatents).map((key, index) => {
			research_areas.push(key);
			var secondSpaceIndex = key.indexOf(' ', key.indexOf(' ') + 5);
			var dividedText = key.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			research_areas_text.push(dividedText);
			//research_areas_nums.push(key.substring(0, key.indexOf(':')));
			Object.keys(l1_d3_researcharea_patenttype_nofpatents[key]).map((innerKey, innerIndex) => {
				if(index==0) {
					series[innerKey] = 
					{
						name: innerKey,
						type: 'scatter',
						coordinateSystem: 'polar',
						symbolSize: function (val) {
							return val[2] * 2;
							//return val[2] * 6;
						},
						data: [],
						animationDelay: function (idx) {
							return idx * 5;
						}
					};					
				}
				/*series[innerKey]['data'].push([innerIndex, counterRArea, l1_d3_researcharea_patenttype_nofpatents[key][innerKey]]);*/
				switch(innerKey) {
					case "Maverics":
						dataMaverics.push([3, counterRArea, l1_d3_researcharea_patenttype_nofpatents[key][innerKey]]);
					break;
					case "Adopters":
						dataAdopters.push([0, counterRArea, l1_d3_researcharea_patenttype_nofpatents[key][innerKey]]);
					break;
				   case "Enablers":
						dataEnablers.push([1, counterRArea, l1_d3_researcharea_patenttype_nofpatents[key][innerKey]]);
					break;
					case "Pioneers":
						dataPioneers.push([2, counterRArea, l1_d3_researcharea_patenttype_nofpatents[key][innerKey]]);
					break;
				}
				
			});
			counterRArea++;
		});
		//console.log(series);
		var seriesChart = [];
		/*Object.keys(series).map((key, index) => {
			seriesChart.push(series[key]);
		});*/
		// We do this because this order has been requested.
		/*seriesChart.push(series["Maverics"]);
		seriesChart.push(series["Pioneers"]);
		seriesChart.push(series["Enablers"]);
		seriesChart.push(series["Adopters"]);
		console.log('seriesChart');
		console.log(seriesChart);*/
		//console.log(research_areas_text);
		//var myChart_l1_d3_researcharea_patenttype_nofpatents = echarts.init(document.getElementById('l1_d3_researcharea_patenttype_nofpatents'));
		const chartOptions = {
			title: {
				text: this.title
			},
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			dataZoom: [{
				type: 'inside'
			}],
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},				
			legend: {
				left: 'right',
				top: this.legendTop,
				data : types,
				bottom: this.legendBottom
			},
			polar: {
				radius: this.radius
			},
			tooltip: {
				formatter: function (params) {
					//console.log(params);
					//return params.value[2] + ' ' + types[params.value[0]] + ' in Research Area:<br>' + research_areas[params.value[1]];
					return params.value[2] + ' ' + params.seriesName + ' in Research Area:<br>' + research_areas[params.value[1]];
				}
			},
			angleAxis: {
				type: 'category',
				//data: research_areas_nums,
				//data: research_areas,
				data: research_areas_text,
				boundaryGap: false,
				splitLine: {
					show: true,
					lineStyle: {
						color: '#999',
						type: 'dashed'
					}
				},
				axisLine: {
					show: false
				},
			},
			radiusAxis: {
				type: 'category',
				data: types,
				axisLine: {
					show: false
				},
				axisLabel: {
					rotate: 45
				},
			},
			//series: seriesChart
			series: [
			{
				name: 'Adopters',
				type: 'scatter',
				coordinateSystem: 'polar',
				symbolSize: function (val) {
					return val[2] * 2;
				},
				data: dataAdopters,
				animationDelay: function (idx) {
					return idx * 5;
				}
			},
			{
				name: 'Enablers',
				type: 'scatter',
				coordinateSystem: 'polar',
				symbolSize: function (val) {
					return val[2] * 2;
				},
				data: dataEnablers,
				animationDelay: function (idx) {
					return idx * 5;
				}
			},
			{
				name: 'Pioneers',
				type: 'scatter',
				coordinateSystem: 'polar',
				symbolSize: function (val) {
					return val[2] * 2;
				},
				data: dataPioneers,
				animationDelay: function (idx) {
					return idx * 5;
				}
			},
			{
				name: 'Maverics',
				type: 'scatter',
				coordinateSystem: 'polar',
				symbolSize: function (val) {
					return val[2] * 2;
				},
				data: dataMaverics,
				animationDelay: function (idx) {
					return idx * 5;
				}
			},
			]
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
		}
		return chartOptions;
	}
	
	
	
	
	
	
	
	
	
	
}
