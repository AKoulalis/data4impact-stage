import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatIconModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule, } from '@angular/material';
import { CoreModule } from '../../../../core/core.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { SelectAutocompleteModule } from 'mat-select-autocomplete';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

// General charts
import { Echart1Component } from './echart1/echart1.component';
import { Echart2Component } from './echart2Map/echart2.component';
import { Echart3Component } from './echart3/echart3.component';
import { Echart4Component } from './echart4/echart4.component';
import { Echart5Component } from './echart5/echart5.component';
import { Echart6Component } from './echart6/echart6.component';
import { Echart7Component } from './echart7/echart7.component';
import { Echart8Component } from './echart8Graph/echart8.component';
import { Echart9Component } from './echart9/echart9.component';
import { Echart10Component } from './echart10/echart10.component';
import { Echart11Component } from './echart11/echart11.component';
import { Echart12Component } from './echart12/echart12.component';
import { Echart13Component } from './echart13/echart13.component';
import { Echart14Component } from './echart14/echart14.component';
import { Echart15Component } from './echart15/echart15.component';
import { Echart16Component } from './echart16/echart16.component';
import { Echart17Component } from './echart17/echart17.component';
import { Echart18Component } from './echart18/echart18.component';
import { Echart19Component } from './echart19/echart19.component';
import { ChartControllerComponent } from './chart-controller/chart-controller.component';
import { Echart8ComponentPrint } from './print/echart8Graph-print/echart8-print.component';

@NgModule({
	declarations: [
		// Widgets
		Echart1Component,
		Echart2Component,
		Echart3Component,
		Echart4Component,
		Echart5Component,
		Echart6Component,
		Echart7Component,
		Echart8Component,
		Echart9Component,
		Echart10Component,
		Echart11Component,
		Echart12Component,
		Echart13Component,
		Echart14Component,
		Echart15Component,
		Echart16Component,
		Echart17Component,
		Echart18Component,
		Echart19Component,
		ChartControllerComponent,
		Echart8ComponentPrint
	],
	exports: [
		// Widgets
		Echart1Component,
		Echart2Component,
		Echart3Component,
		Echart4Component,
		Echart5Component,
		Echart6Component,
		Echart7Component,
		Echart8Component,
		Echart9Component,
		Echart10Component,
		Echart11Component,
		Echart12Component,
		Echart13Component,
		Echart14Component,
		Echart15Component,
		Echart16Component,
		Echart17Component,
		Echart18Component,
		Echart19Component,
		ChartControllerComponent,
		Echart8ComponentPrint
	],
	imports: [
		CommonModule,
		PerfectScrollbarModule,
		MatTableModule,
		CoreModule,
		MatIconModule,
		MatButtonModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatSortModule,
		FormsModule,
		ReactiveFormsModule,
		//SelectAutocompleteModule
		AngularMultiSelectModule
	]
})
export class ChartModule {
}
