// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart7',
	templateUrl: './echart7.component.html',
	styleUrls: ['./echart7.component.scss']
})
export class Echart7Component implements OnInit {
	filterData: Array<Object> = [];
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	source = '';
	title = 'Project Publications Cited in Patents per Funder';
	rawData: any;
	ClickCounter: any;
	chartOptions: any;
	funders = [];
	series = {};
	seriesChart = [];
	radius: any = '75%';
	legendBottom: any = '21px';
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}
	
	countChangedHandler(filteredData:any) {
		this.ClickCounter = filteredData;
		//console.log(filteredData);
		//console.log(this.funders);
		this.funders = [];
		var allData = this.rawData;
		var newData = {};
		if(filteredData=='all') {
			Object.keys(allData).map((key, index) => {
				newData[key] = allData[key];
			});
			
		} else {
			Object.keys(allData).map((key, index) => {
				if (filteredData.includes(key)) {
					newData[key] = allData[key];
				}
			});
		}
		//console.log(newData);
		//console.log(this.series);
		Object.keys(newData).map((key, index) => {
			this.funders.push(key);
			Object.keys(newData[key]).map((innerKey, innerIndex) => {
				if(index==0) {
					this.series[innerKey]['data'] = [];					
				}
				this.series[innerKey]['data'].push(newData[key][innerKey]);
			});
		});
		//console.log(this.series);
		var seriesChart = [];
		Object.keys(this.series).map((key, index) => {
			seriesChart.push(this.series[key]);
		});
		this.chartOptions.series = seriesChart;
		this.chartOptions.angleAxis.data = this.funders;
		this.initEChartJS(this.chartOptions);
	}
	
	resetChartHandler() {
		this.countChangedHandler('all');
		this.initEChartJS(this.chartOptions);
	}
	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.radius = '70%';
				this.legendBottom = '17';
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.source = this.chartData['source'];
			this.chartOptions = this.l1_d2_funder_pubs_cited1time_cited5time(this.chartData);
			this.initEChartJS(this.chartOptions);
		};
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV7',
			data: this.rawData,
			reportTitle: this.chartTitle,
			title: this.title,
			source: this.source,
			indicator: 'Throughput/Output',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}

	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',	
			reportTitle: this.chartTitle,		
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d2_funder_pubs_cited1time_cited5time(dData) {
		var l1_d2_funder_pubs_cited1time_cited5time = dData['D4I_Dataset'];
		this.rawData = l1_d2_funder_pubs_cited1time_cited5time;
		var abbreviationList = this.chartData["abbreviations"];	
		/*var number_of_pubs = [];
		var number_of_pubs_cited_1_time = [];
		var number_of_pubs_cited_5_times = [];*/
		var legendData = [];

		Object.keys(l1_d2_funder_pubs_cited1time_cited5time).map((key, index) => {
			//console.log(l1_d2_funder_pubs_cited1time_cited5time[key]);
			this.funders.push(key);
			this.filterData.push({id:key, itemName:key});
			Object.keys(l1_d2_funder_pubs_cited1time_cited5time[key]).map((innerKey, innerIndex) => {
				if(index==0) {
					this.series[innerKey] = 
					{
						type: 'bar',
						data: [],
						coordinateSystem: 'polar',
						name: innerKey,
						stack: 'a'
					};
					legendData.push(innerKey);					
				}
				this.series[innerKey]['data'].push(l1_d2_funder_pubs_cited1time_cited5time[key][innerKey]);
			});
			
			/*number_of_pubs.push(l1_d2_funder_pubs_cited1time_cited5time[key]["number_of_pubs"]);
			number_of_pubs_cited_1_time.push(l1_d2_funder_pubs_cited1time_cited5time[key]["number_of_pubs_cited_1_time"]);
			number_of_pubs_cited_5_times.push(l1_d2_funder_pubs_cited1time_cited5time[key]["number_of_pubs_cited_5_times"]);*/
		});
		//console.log(number_of_pubs);
		//console.log(number_of_pubs_cited_1_time);
		//console.log(this.series);
		Object.keys(this.series).map((key, index) => {
			this.seriesChart.push(this.series[key]);
		});
			
		//var myChart_l1_d4_uptake_score_organization = echarts.init(document.getElementById('l1_d2_funder_pubs_cited1time_cited5time'));
		var source = 'Source text';
		const chartOptions = {
			title: {
				text: this.title,
			},
			color: [ '#003070','#008ac1','#00c7f9' ],
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				bottom: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],		
			dataZoom: [{
				type: 'inside',
				radiusAxisIndex: 0 
			}],
			toolbox: {
				right: 40,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
	
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			legend: {
				show: true,
				//data: ['pubs not cited', 'pubs cited 1-4', 'pubs cited 5 times or more' ],
				data: legendData,
				bottom: this.legendBottom
			},
			tooltip: {
				show: true,
				trigger: 'axis',
				axisPointer : {  
					type : 'shadow'      
				},
				formatter: function(all_params) {
					//console.log(all_params);
					var label = '';
					//label += all_params[0]['name'];
					label += abbreviationList[all_params[0]['name']].replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>');
					label += '<br>';
					all_params.map(	(params) => {				
						label += params.marker + ' ' + params.seriesName + ' ' + params.value.toLocaleString();
						//label += 
						label += '<br>';
					});
					return label;
					//return params.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');				
					//return abbreviationList[params].replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
				}
			},
			angleAxis: {
				type: 'category',
				data: this.funders,
				z: 10,
				axisLabel: {
					/*formatter: (function(value){
						//console.log(value);
						return abbreviationList[value];
					})*/
				}
			},
			radiusAxis: {
			},
			polar: {
				radius: this.radius
			},
			series: this.seriesChart,
			/*series: [{
				type: 'bar',
				data: number_of_pubs,
				coordinateSystem: 'polar',
				name: 'pubs not cited',
				stack: 'a'
			},{
				type: 'bar',
				data: number_of_pubs_cited_1_time,
				coordinateSystem: 'polar',
				name: 'pubs cited 1-4',
				stack: 'a'
			},{
				type: 'bar',
				data: number_of_pubs_cited_5_times,
				coordinateSystem: 'polar',
				name: 'pubs cited 5 times or more',
				stack: 'a'
			} ],*/

			
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['toolbox']['right'] = 10;
		}
		return chartOptions;	
		
	}

	
	
	
	
}
