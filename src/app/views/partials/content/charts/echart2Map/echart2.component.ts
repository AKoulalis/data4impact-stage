// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';


import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import { HttpClient } from '@angular/common/http'; 
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart2',
	templateUrl: './echart2.component.html',
	styleUrls: ['./echart2.component.scss'],
})
export class Echart2Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	//@Input() desc: string;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	title = 'EU Contribution per Country';
	source = '';
	l1_d1_funding_countryData: any;
	mapTop:any = '50%';
	visualMapBottom:any = '50';
	layoutSize:any = '120%';
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private http: HttpClient, private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		//this.loadJsonService.getTitle(this.chartTitle);
		//this.loadJsonService.getJSON().subscribe(data => {	
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.mapTop = '30%';
				this.visualMapBottom = '20';
				this.layoutSize = '100%';
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.source = this.chartData['source'];
			this.http.get('./assets/json/maps/world-sm.geo.json').subscribe (worldJson => {
				echarts.registerMap('world', worldJson);
				var chartOptions = this.l1_d1_funding_country(this.chartData);
				this.initEChartJS(chartOptions);
			});
		}	
       // });		
	
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSVMap',
			data: this.l1_d1_funding_countryData,
			reportTitle: this.chartTitle,
			title: this.title,
			source: this.source,
			indicator: 'Input',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);
		
	}
	
	downloadJSON() {
				this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var filters = ["Field: Health","Time Range: 2005 - 2010"];
		var fileContents = 	{
			json: 'JSONTable',			
			title: this.title,
			reportTitle: this.chartTitle,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d1_funding_country(d1Data) {
		var l1_d1_funding_countryData = d1Data["D4I_Dataset"]["data"];	
		this.l1_d1_funding_countryData = l1_d1_funding_countryData;
		var funding_country = [];
		var money = [];
		var funding_country_legend = [];		
		var funding_country_map = l1_d1_funding_countryData;
		funding_country_map.map(function(country) {
			money.push(country.value);
		});
		var visualMin = Math.min(...money);
		var visualMax = Math.max(...money);
		if(visualMin == visualMax) { 
			visualMin = 0;			
		}
		var source = 'Source text';
		var chartOptions = {
			title: {
				text: this.title
			},
			graphic: [{
				type: 'text',
				z: 100,
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
				}
			},],
			tooltip: {
				trigger: 'item',
				showDelay: 0,
				transitionDuration: 0.2,
				formatter: function (params) {
					var value: any = (params.value + '').split('.');
					value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
					if(value!=='NaN') {
						return /*params.seriesName + '<br/>' + */params.name + ': ' + value;
					}
					else {
						return /*params.seriesName + '<br/>' + */params.name + ': No data';
					}
				}
			},
			visualMap: {
				left: 'right',
				/*min: 500000,
				max: 40000000,*/
				min: visualMin,
				max: visualMax,
				inRange: {
					color:['#dfeced','#bfd9dc','#a0c6ca','#80b3b9','#61a0a8']
				},
				text:['high','low'],
				bottom: this.visualMapBottom,
				calculable: true,
				formatter: function (value) {
					return parseInt(value).toLocaleString() + ' EUR';
				}
			},
			toolbox: {
				right: 0,
				top: 'top',
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				},
			
			},
			series: [
			{
				name: 'Funding',
				type: 'map',
				roam: true,
				map: 'world',
				//bottom: this.mapBottom,
				layoutCenter: ['50%', this.mapTop],
				layoutSize: this.layoutSize,
				itemStyle:{
					emphasis:{label:{show:false}}
				},
				data: funding_country_map
			}
			]
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
		}		
		return chartOptions;
	};
	

	
	
}
