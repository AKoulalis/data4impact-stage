// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
//import { HttpClient } from '@angular/common/http'; 
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;
//import * as echarts from 'echarts.min.js';
import { BehaviorSubject } from 'rxjs';

@Component({
	selector: 'kt-echart8',
	templateUrl: './echart8.component.html',
	styleUrls: ['./echart8.component.scss'],
})
export class Echart8Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	loadChart: Boolean = false;
	chartOptions: any;
	isWsAvailable: BehaviorSubject<boolean> = new BehaviorSubject(false);
	chartData1: any;
	chartData2: any;
	areaFilter: string;
	source: string = '';
	title: string = 'Project Participation & Collaborations';
	subtitle: string = '';
	rawData: any = [];
	legendType: string = 'scroll';
	legendLeft: any = 'auto';
	legendRight: any = 0;
	legendHeight: any = 483;
	legendTop: any = 40;
	legendBottom: any = 'auto';
	legendWrap: boolean = true;
	seriesLeft: any = 0;
	containerWidth: string = '80%';
	containerTop: any = '20%';
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (this.windowWidth < 800) {
			//this.gridRight = 40;
			//this.gridBottom = 165;
			this.legendTop = 'auto';
			this.legendBottom = 30;
			//this.legendType = 'plain';
			this.legendHeight = 98;
			this.legendLeft = 'left';
			this.legendRight = 'auto';
			this.legendWrap = false;
			//this.seriesLeft = 'auto';
			this.seriesLeft = '20%';
			this.containerWidth = '70%';
			this.containerTop = '40';
			this.pixelRatio = 2;
		}
		
		/*this.loadJsonService.getTitle(this.chartTitle);
		this.loadJsonService.getJSON().subscribe(data => {					
			this.http.get('./assets/json/l1_d1_collaborations_no_dupl.json').subscribe (dData2 => {
				this.chartData1 = data;
				this.chartData2 = dData2;
			});
		
        });	*/
	}


	/** Pick chart across area*/
	pickChart(event) {
		this.loadChart = false;
		let elementId = (event.target as HTMLInputElement);
		var searchVal: string = (elementId).value;
		if(searchVal !== 'none'){
			this.areaFilter = searchVal;
			/* TEMPORARY!!!!
			//this.chartData1 = this.chartData['D4I_Dataset']['all_data'];
			//this.chartData2 = this.chartData['D4I_Dataset']['data_no_dupl'];
			*/

				this.title = this.chartData['title'];
				this.source = this.chartData['source'];
				//console.log(this.chartData);
				this.chartOptions = this.l1_d2_collaborations(this.chartData['D4I_Dataset']['all_data'],this.chartData['D4I_Dataset']['data_no_dupl'],this.chartData['D4I_Dataset']['country_category'],this.chartData['D4I_Dataset']['abbreviations'], this.areaFilter);
				//this.initEChartJS(chartOptions);

			//this.chartOptions = this.l1_d2_collaborations(this.chartData1, this.chartData2, this.areaFilter);
			this.loadChart = true;
			window.setTimeout(()=>{ this.initEChartJS(this.chartOptions) }, 0);			
		}
	}	
		
	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV8',
			data: this.rawData,
			reportTitle: this.chartTitle,
			areaFilter: this.areaFilter,
			subtitle: this.subtitle,
			title: this.title,
			source: this.source,
			indicator: 'Throughput/Output',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',	
			reportTitle: this.chartTitle,		
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d2_collaborations(dData, dData2, country_cats, abbreviations, filter) {
		var that = this;
		//console.log(dData);
		var l1_d2_collaborations =  dData[filter];
		console.log(l1_d2_collaborations);
		var abbreviationList = abbreviations;
		console.log(abbreviationList);
		this.rawData[1] = dData2[filter];
		this.rawData[0] = l1_d2_collaborations;		
		var colors = ['#c23531', '#2f4554',  '#d48265', '#749f83',  '#ca8622', '#91c7ae', '#bda29a', '#61a0a8', '#6e7074', '#546570', '#c4ccd3'];
		var nodes = {};
		var ccc = 0;
		var color = [];
		var categories = [];
		var categories_dupl = [];
		var selectedCat = {};
		var node = {};
		var keyNum = 360 / (Object.keys(l1_d2_collaborations).length -1);
		var linkSize = {
			"countries" : 4000,
			"activity_types" : 15000,
			"icd10categories" : 34,
		}
		var linkSizePlus = {
			"countries" : 300,
			"activity_types" : 13,
			"icd10categories" : 0,
		}
		if (this.windowWidth < 800) {
			var nodeSize = {
				"countries" : 93,
				"activity_types" : 150,
				"icd10categories" : 17,
			}
		} else {
			var nodeSize = {
				"countries" : 53,
				"activity_types" : 120,
				"icd10categories" : 17,
			}
		}
		
		var symbolsize;
		var arrayMax = 1000;
		//Adjust symbolsize for countries
		
		if(filter=="countries"){
			var collaborationArray = [];
			Object.keys(l1_d2_collaborations).map((key, index) => {
				collaborationArray.push(l1_d2_collaborations[key]);
				
			});
			arrayMax = Math.max(...collaborationArray);			
		}
		Object.keys(l1_d2_collaborations).map((key, index) => {
			//color.push(this.hsvToRgb(keyNum * index, 80, 80));
			/*var node = {"id":index, "name":key , "value":l1_d2_collaborations[key], "category":key,"symbolSize":(l1_d2_collaborations[key]+200)/nodeSize[filter], "itemStyle": {"normal":{"color": color[index]}} };
			nodes[key] = node;
			categories.push({"name":key, "itemStyle": {"color": color[index]}});*/
			
			if(filter=="countries"){
				//var countriesByCategory= dData['country_category'];
				var countriesByCategory= country_cats;
				//console.log(countriesByCategory);
				symbolsize = (l1_d2_collaborations[key]+200)/nodeSize[filter];
				if (arrayMax < 1000 ) {
					symbolsize = (l1_d2_collaborations[key]*10+200)/nodeSize[filter];
				}
				if (arrayMax < 400 ) {
					symbolsize = (l1_d2_collaborations[key]*20+200)/nodeSize[filter];
				}
				//node = {"id":index, "name":key , "value":l1_d2_collaborations[key], "category":countriesByCategory[key],"symbolSize":symbolsize, "itemStyle":{"normal":{}}, "label" :{"normal": {"show": (symbolsize > 10)}} };
				node = {"id":index, "name":key , "value":l1_d2_collaborations[key], "category":countriesByCategory[key],"symbolSize":symbolsize, "itemStyle":{"normal":{}}, "label" :{"normal": {"show": true}} };
				nodes[key] = node;
				categories_dupl.push(countriesByCategory[key]);
				
			} else {
				symbolsize = (l1_d2_collaborations[key]+200)/nodeSize[filter];
				if (symbolsize < 3 ) {
					symbolsize = 3;
				}
				node = {"id":index, "name":key , "value":l1_d2_collaborations[key], "category":key,"symbolSize":symbolsize, "itemStyle":{"normal":{}} };
				nodes[key] = node;
				categories.push({"name":key,});
			};
				
		});
		if(filter=="countries"){
			categories = [];
			//console.log(categories_dupl);
			var categories_no_dup = categories_dupl.sort().filter(function(item, pos, ary) {
				return !pos || item != ary[pos - 1];
			});
			categories = categories_no_dup.map(single => {	
				if (single=="Northern Europe" || single=="Southern Europe" ) {
					selectedCat[single] = true;
				} else {
					selectedCat[single] = false;
				}
				return {"name":single};
			});			
		}
		//console.log(categories);
		//console.log(selectedCat);
		var nodesArray = [];
		Object.keys(nodes).map((key, index) => {
			nodesArray.push(nodes[key]);
		})
		//console.log(nodesArray);
		//console.log('nodes');
		//console.log(nodes);
		
		var links = [];
		var l1_d2_collaboration_links =  dData2[filter];
		//console.log(l1_d2_collaboration_links);
		Object.keys(l1_d2_collaboration_links).map((key, index) => {
			Object.keys(l1_d2_collaboration_links[key]).map((linked_key, linked_index) => {
				var formatter = '';
				//console.log('nodes[key]');
				//console.log(nodes[key]);
				//console.log(linked_key);
				//console.log(nodes[linked_key]);
				var lineWidth = (l1_d2_collaboration_links[key][linked_key]+linkSizePlus[filter])/linkSize[filter];
				if (lineWidth < 0.5 ) {
					lineWidth = 0.5;
				}
				var link = {"source":nodes[key]["id"], "target":nodes[linked_key]["id"], "value": l1_d2_collaboration_links[key][linked_key], "lineStyle": {"width": lineWidth, "opacity": 1}, "sourceName":nodes[key]["name"], "targetName":nodes[linked_key]["name"] };
				links.push(link);
				
			})
			
		})
		var source = 'Source text';
		var subtitle = '';
		switch(filter) {
			case 'countries' :
				subtitle = 'Countries';
			break;
			case 'activity_types' :
				subtitle = 'Organization Sectors';
			break;
			case 'icd10categories' :
				subtitle = 'Research Areas';
			break;
		}
		this.title = 'Project Participation & Collaborations across ' + subtitle;
		this.subtitle = subtitle;
		var chartOptions = {
			title: {
				text: this.title,
				left: 'left'
			},
			color: colors,
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			tooltip: {
				formatter: function (params) {
					//console.log(params);
					if (params.dataType == 'edge') {
						//console.log('edge');
						if(filter=="icd10categories"){
							var sourceName = params.data.sourceName.substring(0, params.data.sourceName.indexOf(':'));
							var targetName = params.data.targetName.substring(0, params.data.targetName.indexOf(':'));
						} else {
							var sourceName = params.data.sourceName;
							var targetName = params.data.targetName;
						}
						return 'Collaborations ' + sourceName.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>') + ' - ' + targetName.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>') + ' : ' + params.data.value;
					} else {
						var dataName ='';
						if(filter=="countries"){
							dataName = params.data.name.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>');
						} else {
							dataName = abbreviationList[filter][params.data.name].replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>');
						}
						return 'Number of Projects <br>' + params.marker + dataName + ' : ' + params.data.value.toLocaleString();
					}
				}
			},
			width: this.containerWidth,
			right: 10,
			height: '60%',
			toolbox: {
				right: 10,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},  
			legend: {
				//data: categories,
				type: this.legendType,
				orient: 'vertical',
				top: this.legendTop,
				bottom: this.legendBottom,
				height: this.legendHeight,
				left: this.legendLeft,
				right: this.legendRight,
				formatter: function(params) {
					if (that.legendWrap) {
						return params.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
					}
					return params.replace(/(.*?\s..*?\s.*?\s*?\s.*?\s.*?\s)/g, '$1'+'\n');
				}
			},	
			animationDurationUpdate: 1500,
			animationEasingUpdate: 'quinticInOut',
			series : [
				{
					name: 'Number of Projects',
					type: 'graph',
					layout: 'circular',
					//top: '20%',
					//edgeLabel: {show: true},
					circular: {
						rotateLabel: true
					},
					data: nodesArray,
					links: links,
					categories: categories,
					roam: true,
					//right: 600,
					top: this.containerTop,
					left: this.seriesLeft,
					label: {
						normal: {
							show: true,
							position: 'right',
							//formatter: '{b}'
							formatter: function(seriesName) {
								var sName;
								if(filter=="countries") {
									sName = seriesName.name;
								} else {
									sName = seriesName.name.substring(0, seriesName.name.indexOf(':'));
								}
								
								return sName;
								
							}
						}
					},
					lineStyle: {
						normal: {
							color: 'source',
							curveness: 0.3
						}
					}
				}
			]
		}
		if (filter=='countries') {
			//chartOptions["legend"]["selected"] = {"Western Europe":true, "Eastern Europe":true};
			chartOptions["legend"]["selected"] = selectedCat;
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
		}
		return chartOptions;

	}
	
	
	//Modify colors
	hsvToRgb(h, s, v) {
		var r, g, b;
		var i;
		var f, p, q, t;
	 
		// Make sure our arguments stay in-range
		h = Math.max(0, Math.min(360, h));
		s = Math.max(0, Math.min(100, s));
		v = Math.max(0, Math.min(100, v));
	 
		// We accept saturation and value arguments from 0 to 100 because that's
		// how Photoshop represents those values. Internally, however, the
		// saturation and value are calculated from a range of 0 to 1. We make
		// That conversion here.
		s /= 100;
		v /= 100;
	 
		if(s == 0) {
			// Achromatic (grey)
			r = g = b = v;
			return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
		}
	 
		h /= 60; // sector 0 to 5
		i = Math.floor(h);
		f = h - i; // factorial part of h
		p = v * (1 - s);
		q = v * (1 - s * f);
		t = v * (1 - s * (1 - f));
	 
		switch(i) {
			case 0:
				r = v;
				g = t;
				b = p;
				break;
	 
			case 1:
				r = q;
				g = v;
				b = p;
				break;
	 
			case 2:
				r = p;
				g = v;
				b = t;
				break;
	 
			case 3:
				r = p;
				g = q;
				b = v;
				break;
	 
			case 4:
				r = t;
				g = p;
				b = v;
				break;
	 
			default: // case 5:
				r = v;
				g = p;
				b = q;
		}
	 
		return "rgb("+Math.round(r * 255)+","+ Math.round(g * 255)+","+ Math.round(b * 255)+")";
	}
	
	
	
	
}
