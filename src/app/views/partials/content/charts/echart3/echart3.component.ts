// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

// Services
import { DownloadJSONService, DownloadCSVService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart3',
	templateUrl: './echart3.component.html',
	styleUrls: ['./echart3.component.scss'],
})
export class Echart3Component implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	//@Input() desc: string;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	gridRight: number = 250;
	gridBottom: number = 95;
	legendTop: any = 47;
	legendBottom: any = 'auto';
	sliderBottom: number = 35;
	legendType: string = 'plain';
	legendHeight: any = 'auto';
	legendRight: any = 0;
	yTitle = 'EUR';
	xTitle = 'year';
	source = '';
	title = 'EU Contribution per Organization Sector';
	rawData: any;
	windowWidth: number;
	intervalΧ: any = 1;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(  private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		/*this.loadJsonService.getTitle(this.chartTitle);
		this.loadJsonService.getJSON().subscribe(data => {			
			console.log('getJSON data');
            console.log(data);
			var chartOptions = this.loadJsonService.getChartOptions(this.chartTitle, data);
			this.initEChartJS(chartOptions);
        });*/
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.gridRight = 40;
				this.gridBottom = 165;
				this.legendTop = 'auto';
				this.legendBottom = 30;
				this.sliderBottom = 130;
				this.legendType = 'scroll';
				this.legendHeight = 80;
				this.legendRight = 'auto';
				this.intervalΧ = 'auto';
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.xTitle = this.chartData['x_axis'];
			this.yTitle = this.chartData['y_axis'];
			this.source = this.chartData['source'];
			var chartOptions = this.l1_d1_year_activitytype_funding(this.chartData);				
			this.initEChartJS(chartOptions);
			/*this.loadJsonService.getTitle(this.chartTitle);
			this.loadJsonService.getJSON().subscribe(data => {			
				var chartOptions = this.l1_d1_year_activitytype_funding(data);
				this.initEChartJS(chartOptions);
			});	*/
		}		
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV9',
			data: this.rawData,
			reportTitle: this.chartTitle,
			xAxis: this.xTitle,
			yAxis: this.yTitle,
			title: this.title,
			source: this.source,
			indicator: 'Input',
			filters: this.filters.join(', ')
		};
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		console.log('JSON icon clicked');
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',
			reportTitle: this.chartTitle,			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d1_year_activitytype_funding(dData) {
		var l1_d1_year_activitytype_funding_new2 =  dData['D4I_Dataset'];
		this.rawData = l1_d1_year_activitytype_funding_new2;
		var abbreviationList = this.chartData["abbreviations"];
		var years = [];
		var percentage = [];
		var compareIndex = [];
		var compareIndexTransparent = [];
		var dataHigher = [];
		var dataResearch = [];
		var dataPrivate = [];
		var dataPublic = [];
		var dataOther = [];
		var allData = {};
		var dataSeries = [];
		Object.keys(l1_d1_year_activitytype_funding_new2).map((key, index) => {
			years.push(key);	
			Object.keys(l1_d1_year_activitytype_funding_new2[key]).map((innerKey, innerIndex) => {
				if(index==0) {
					allData[innerKey] = [];
				}
				allData[innerKey].push([key, l1_d1_year_activitytype_funding_new2[key][innerKey]]); 
			});
		});
		Object.keys(allData).map((key, index) => {
			dataSeries.push({
				name:key,
				type:'line',
				smooth:true,
				symbol: 'circle',
				symbolSize: 3,
				stack: 'a',
			   // sampling: 'average',
				itemStyle: {
					normal: {
					   
					}
				},
				areaStyle: {
					normal: {
			 
					}
				},
				data: allData[key]
			});
		});
		/*Object.keys(l1_d1_year_activitytype_funding_new2).map((key, index) => {
			years.push(key);	
			Object.keys(l1_d1_year_activitytype_funding_new2[key]).map((innerKey, innerIndex) => {
				switch(innerKey) {
					case "Higher or Secondary Education Inst.":
						dataHigher.push([key, l1_d1_year_activitytype_funding_new2[key][innerKey]]);
					break;
					case "Research Organisations":
						dataResearch.push([key, l1_d1_year_activitytype_funding_new2[key][innerKey]]);
					break;
				   case "Private for-profit entities":
						dataPrivate.push([key, l1_d1_year_activitytype_funding_new2[key][innerKey]]);
					break;
					case "Public bodies":
						dataPublic.push([key, l1_d1_year_activitytype_funding_new2[key][innerKey]]);
					break;
					case "Other":
						dataOther.push([key, l1_d1_year_activitytype_funding_new2[key][innerKey]]);
					break;
				} 
			});
		});*/
		
		//var myChart_l1_d1_year_activitytype_funding_new2 = echarts.init(document.getElementById('l1_d1_year_activitytype_funding_new2'));
		//console.log(myChart_l1_d1_year_activitytype_funding_new2);
		var chartOptions = {
		title: {
			text: this.title,
		},
		//color: [ '#003070','#0967ba','#008ac1','#00c7f9','#9ef2ff',   '#9e9ac8', '#ce6dbd', '#a55194', '#6b6ecf', '#393b79', '#17becf', '#9edae5', '#dbdb8d', '#c7c7c7', '#e377c2', '#f7b6d2', '#8c554a', '#c49c94', '#c5b0d5', '#d62728', '#ff9896', '#2ca02c', '#98df8a', '#ff7f0e', '#ffbb78' ],
		color: [ '#c23531','#61a0a8','#2f4554','#749f83','#a55194','#d6616b'],	
		graphic: [{
			type: 'text',
			z: 100,
			//left: 'center',
			top: 'bottom',
			style: {
				fill: '#333',
				text: [
					'Description/Source: '+ this.source
				].join('\n'),
				//font: '14px Microsoft YaHei'
			}
		},],
		toolbox: {
			right: 10,
			showTitle: false,
			itemSize: 20,
			feature: {
				saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
				myToolDownloadSVG: {
					show: true,
					title: 'Download CSV',	
					icon: 'image:///assets/media/files/csv.png',
					onclick:(evt)=> {
						this.downloadCSV();
					},
				},
				myToolDownloadJSON: {
					show: true,
					title: 'Download JSON',	
					icon: 'image:///assets/media/files/json.png',
					onclick:(evt)=> {
						//console.log(this);
						this.downloadJSON();
					},
				}
			}
		},
		tooltip: {
			//position: 'center'
			//formatter: "{c} ({d}%)",
			//triggerOn: 'none',
			trigger: 'item',
			formatter: function (params) {
				var label = '';				
				//console.log(params);
				if(Array.isArray(params)) {
					label += params[0]['axisValue'];
					label += '<br>';
					params.map((single)=> {
						//console.log('single');
						//console.log(single);
						label += single.marker + single.seriesName.replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>') + ': ' + single.value[1].toLocaleString();
						label += '<br>';
					});
				} else {
					label += params.value[0];
				label += '<br>';
					label += params.marker + params.value[1].toLocaleString();
						label += '<br>';
				}
				//console.log(params[0]['axisValue']);
				//console.log('EU Contribution params');
				//console.log(params);
				//var stuff =  rArea.substring(0, 20);
				//return stuff;
				return label;
			},
		},
		grid: {
			//top: 100,
			//left: 20,
			left: '1%',
			bottom: this.gridBottom,
			right: this.gridRight,	
			containLabel: true			
		},
		dataZoom: [{
				type: 'slider',
				start: 0,
				end: 100,
				bottom: this.sliderBottom
			}, {
			type: 'inside'
		}],
		legend: {
			//data: research_areas,
			type: this.legendType,
			orient: 'vertical',
			//y: 'bottom',
			top: this.legendTop,
			bottom: this.legendBottom,
			right: this.legendRight,
			//right: 0,
			//top: 50,
			height: this.legendHeight,
			//left: 0,
			/*formatter: function (rArea) {
				var stuff =  rArea.substring(0, 20);
				return stuff;
			},*/
			formatter: function(params) {
				//return params.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
				return abbreviationList[params];
			},
			//backgroundColor: '#fff',
		
		},
		xAxis: {
			name: this.xTitle,
			data: years,
			splitLine: {
				show: false
			},
			splitNumber: 0,
			axisTick: {
				alignWithLabel: true,
				interval: 0
			},
			axisLabel: {
				//showMinLabel: true, 
				interval: this.intervalΧ,
				//rotate: 30,
				margin: 10
			},
			axisPointer: {
				value: '2015',
				//snap: true,
				lineStyle: {
					color: '#004E52',
					opacity: 0.5,
					width: 1
				},
				label: {
					show: true,
					backgroundColor: '#004E52'
				},
				handle: {
					show: true,
					color: '#004E52',
					size: 20,
					margin: 35
				}
			},
	
		},
		yAxis: {
			name: this.yTitle,
			type: 'value',
			/*axisTick: {
				//inside: true
			},
			splitLine: {
				show: false
			},
			axisLabel: {
				//inside: true,
			},
			z: 10*/
		},
		series: dataSeries
	/*
		series: [{
            name:'Higher or Secondary Education Inst.',
            type:'line',
            smooth: true,
            symbol: 'circle',
            symbolSize: 3,
			stack: 'a',
           // sampling: 'average',
            itemStyle: {
                normal: {
                   
                }
            },
            areaStyle: {
                normal: {          
                }
            },
            data: dataHigher
        },
        {
            name:'Research Organisations',
            type:'line',
            smooth:true,
            symbol: 'circle',
            symbolSize: 3,
			stack: 'a',
           // sampling: 'average',
            itemStyle: {
                normal: {
                   
                }
            },
            areaStyle: {
                normal: {
 
                }
            },
            data: dataResearch
        },
		{
            name:'Private for-profit entities',
            type:'line',
            smooth:true,
            symbol: 'circle',
            symbolSize: 3,
			stack: 'a',
           // sampling: 'average',
            itemStyle: {
                normal: {
                   
                }
            },
            areaStyle: {
                normal: {
         
                }
            },
            data: dataPrivate
        },
		{
            name:'Public bodies',
            type:'line',
            smooth:true,
            symbol: 'circle',
            symbolSize: 3,
			stack: 'a',
            //sampling: 'average',
            itemStyle: {
                normal: {
                  
                }
            },
            areaStyle: {
                normal: {
      
                }
            },
            data: dataPublic
        },
		{
            name:'Other',
            type:'line',
            smooth:true,
            symbol: 'circle',
            symbolSize: 3,
			stack: 'a',
           // sampling: 'average',
            itemStyle: {
                normal: {
                   
                }
            },
            areaStyle: {
                normal: {
               
                }
            },
            data: dataOther
        },
		],

		*/		
		};
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['grid']['top'] = 90;
		}
		return chartOptions;
    }
	
	
	
	
	
	
	
	
	
}
