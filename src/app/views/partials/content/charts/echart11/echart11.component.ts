// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;


@Component({
	selector: 'kt-echart11',
	templateUrl: './echart11.component.html',
	styleUrls: ['./echart11.component.scss'],
})
export class Echart11Component implements OnInit {
	filterData: Array<Object> = [];
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() filters: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart', {static: true}) echart: ElementRef;
	field_average = 0.93;
	source = '';
	title = 'Emerging Topics: % per Funder after 2015';
	rawData: any;
	ClickCounter: any;
	chartOptions: any;
	//funders = [];
	//series = {};
	//seriesChart = [];
	radius: any = '73%';
	indicatorBottom: any = '24';
	windowWidth: number;
	pixelRatio: number = 3;
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor( private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}
		
	countChangedHandler(filteredData) {
		this.ClickCounter = filteredData;
		//console.log(filteredData);
		var funders = [];
		var allData = this.rawData;
		var newData = {};
		if(filteredData=='all') {
			Object.keys(allData).map((key, index) => {
				newData[key] = allData[key];
			});
			
		} else {
			Object.keys(allData).map((key, index) => {
				if (filteredData.includes(key)) {
					newData[key] = allData[key];
				}
			});
		}
		//console.log(newData);		
		var percentage = [];
		var compareIndex = [];
		var compareIndexTransparent = [];
		//var line093 = [];
		Object.keys(newData).map((key, index) => {
			funders.push(key);
			/*if (abbreviationList[key]) {
				abbreviations.push(abbreviationList[key]);
			} else {
				abbreviations.push(key);
			};*/
			percentage.push(newData[key]);
			//line093.push (this.field_average);
			if(newData[key]>this.field_average){
				compareIndex.push (this.field_average);
				var transparentValue: any = ((this.field_average)*0.91);
				compareIndexTransparent.push(transparentValue);
			} else {
				compareIndex.push (0);
				compareIndexTransparent.push (0);
			}
		});	
		this.chartOptions.series[0]['data'] = percentage;
		this.chartOptions.series[1]['data'] = compareIndex;
		this.chartOptions.series[2]['data'] = compareIndexTransparent;
		this.chartOptions.angleAxis.data = funders;
		this.initEChartJS(this.chartOptions);
	}
	
	resetChartHandler() {
		this.countChangedHandler('all');
		this.initEChartJS(this.chartOptions);
	}
	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (this.windowWidth < 800) {
				this.radius = '65%';
				this.indicatorBottom = '24';
				this.pixelRatio = 2;
			}
			this.title = this.chartData['title'];
			this.field_average = this.chartData['field_average'];
			this.source = this.chartData['source'];
			this.chartOptions = this.l1_d3_funder_percofemergingtopics(this.chartData['D4I_Dataset']);
			this.initEChartJS(this.chartOptions);
		}
	}

	/** Init chart */
	initEChartJS(options) {
		const chart = echarts.init(this.echart.nativeElement);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			csv: 'CSV11',
			data: this.rawData,
			reportTitle: this.chartTitle,
			title: this.title,
			source: this.source,
			indicator: 'Academic Impact',
			filters: this.filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}

	downloadJSON() {
		this.filters = this.filters.filter(function (el) {
			return el != "";
		});
		var fileContents = 	{
			json: 'JSONTable',
			reportTitle: this.chartTitle,			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: this.filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d3_funder_percofemergingtopics(dData) {
		//var l1_d3_funder_percofemergingtopics = dData["companies"];
		//var abbreviationList = dData["abbreviations"];
		var l1_d3_funder_percofemergingtopics = dData;
		this.rawData = l1_d3_funder_percofemergingtopics;
		//var abbreviationList = dData;	
		//var abbreviations = [];
		var abbreviationList = this.chartData["abbreviations"];
		var funders = [];
		var percentage = [];
		var compareIndex = [];
		var compareIndexTransparent = [];
		//var line093 = [];
		Object.keys(l1_d3_funder_percofemergingtopics).map((key, index) => {
			funders.push(key);
			this.filterData.push({id:key, itemName:key});
			/*if (abbreviationList[key]) {
				abbreviations.push(abbreviationList[key]);
			} else {
				abbreviations.push(key);
			};*/
			percentage.push(l1_d3_funder_percofemergingtopics[key]);
			//line093.push (this.field_average);
			if(l1_d3_funder_percofemergingtopics[key]>this.field_average){
				compareIndex.push (this.field_average);
				//var transparentValue: any = (this.field_average - 0.02);
				var transparentValue: any = ((this.field_average)*0.91);
				compareIndexTransparent.push(transparentValue);
			} else {
				compareIndex.push (0);
				compareIndexTransparent.push (0);
			}
		});
		//line093.push (this.field_average);
		//console.log('abbreviations');
		//console.log(abbreviations);
		//console.log(compareIndexTransparent);
		//var myChart_l1_d3_funder_percofemergingtopics = echarts.init(document.getElementById('l1_d3_funder_percofemergingtopics'));

		var chartOptions = {
			title: {
				text: this.title,
			},
			//color: ['#00c7f9','#9ef2ff',   '#9e9ac8', '#ce6dbd', '#a55194', '#6b6ecf', '#393b79', '#17becf', '#9edae5'],
			graphic: [
			{
				type: 'text',
				z: 100,
				left: 'center',
				bottom: this.indicatorBottom,
				style: {
					fill: '#333',
					text: [
						'Field Average: ' + this.field_average + '%'
					].join('\n'),
					font: '14px Microsoft YaHei'
				}
			},
			{
				type: 'text',
				z: 100,
				//left: 'center',
				bottom: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ this.source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			toolbox: {
				right: 40,
				showTitle: false,
				itemSize: 20,
				feature: {
					saveAsImage: {title: 'Save image', pixelRatio: this.pixelRatio},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},
			dataZoom: [{
				type: 'inside',
				radiusAxisIndex: 0
			}],
			angleAxis: {
				type: 'category',
				data: funders,
				//data: abbreviations,
				axisTick: {
					alignWithLabel: true,
					interval: 0,
					show: false,
				},
				nameRotate:10,
				axisLabel: {
					margin: 16,
					//show: false,
					formatter: (function(value){
						//console.log(value);
						return abbreviationList[value];
					})
				}
			},
			tooltip: {
				show: true,
				trigger: 'axis',
				axisPointer : {  
					type : 'shadow'      
				},
				formatter: function (params) {
					var id = params.dataIndex;
					//console.log('params');
					//console.log(params);
					var single = params[0];
					//return abbreviations[single.axisValue] + '<br>' + single.marker + single.value;
					return single.axisValue.replace(/(.*?\s.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'<br>') + '<br>' + single.marker + single.value;
				}
				/*
				formatter: function (params) {
					var id = params.dataIndex;
					console.log('params');
					console.log(params);
					return funders[id] + '<br>' + data[uptake_score];
				}*/

			},
			radiusAxis: {
				//max: 2,
			},
			polar: {
				radius: this.radius,
			},
			series: [{
				id: 'percentage',
				type: 'bar',
				itemStyle: {
					normal: {
						//color: 'transparent'
						color: '#84c1ff'
					}
				},
				data: percentage,
				coordinateSystem: 'polar',
			},
			{
				id: 'compareIndex',
				type: 'bar',
				itemStyle: {
					normal: {
						color: '#2f4554'
					}
				},
				barGap: '-100%',
				z: 10,
				data: compareIndex,
				coordinateSystem: 'polar',
				silent: true
			},
			{
				id: 'compareIndexTransparent',
				type: 'bar',
				itemStyle: {
					normal: {
						color: '#84c1ff'
					}
				},
				barGap: '-100%',
				z: 10,
				data: compareIndexTransparent,
				coordinateSystem: 'polar',
				silent: true
			},

			],
		
		}
		if (this.windowWidth < 800) {
			chartOptions['title']['text'] = this.title.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
			chartOptions['title']['textStyle'] = {
				fontSize: 14,
				lineHeight: 22,
			};
			chartOptions['angleAxis']['axisLabel']['interval'] = 3;
			chartOptions['toolbox']['right'] = 10;
		}
		return chartOptions;
	}

	
	
}
