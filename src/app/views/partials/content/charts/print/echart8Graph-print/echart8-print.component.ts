// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';

import { DownloadCSVService, DownloadJSONService } from '../../../../../../core/_base/layout';
// Charts
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;
//import * as echarts from 'echarts.min.js';
import { BehaviorSubject } from 'rxjs';

@Component({
	selector: 'kt-echart8-print',
	templateUrl: './echart8-print.component.html',
	styleUrls: ['./echart8-print.component.scss'],
})
export class Echart8ComponentPrint implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('echart_1', {static: true}) echartIcd10categories: ElementRef;
	@ViewChild('echart_2', {static: true}) echartCountries: ElementRef;
	@ViewChild('echart_3', {static: true}) echartActivityTypes: ElementRef;
	loadChart: Boolean = false;
	isWsAvailable: BehaviorSubject<boolean> = new BehaviorSubject(false);
	chartData1: any;
	chartData2: any;
	areaFilter: string;
	source = '';
	title = 'Project Participation & Collaborations';
	subtitle = '';
	rawData: any = [];
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){	
			this.areaFilter ='icd10categories';		
			this.title = this.chartData['title'];
			this.source = this.chartData['source'];
			let chartOptionsIcd10categories = this.l1_d2_collaborations(this.chartData['D4I_Dataset']['all_data'],this.chartData['D4I_Dataset']['data_no_dupl'],this.chartData['D4I_Dataset']['country_category'], 'icd10categories');
			let chartOptionsCountries = this.l1_d2_collaborations(this.chartData['D4I_Dataset']['all_data'],this.chartData['D4I_Dataset']['data_no_dupl'],this.chartData['D4I_Dataset']['country_category'], 'countries');
			let chartOptionsActivityTypes = this.l1_d2_collaborations(this.chartData['D4I_Dataset']['all_data'],this.chartData['D4I_Dataset']['data_no_dupl'],this.chartData['D4I_Dataset']['country_category'], 'activity_types');
			this.initEChartJS(chartOptionsIcd10categories, this.echartIcd10categories);
			this.initEChartJS(chartOptionsCountries, this.echartCountries);
			this.initEChartJS(chartOptionsActivityTypes, this.echartActivityTypes);
        };
	}
		
	/** Init chart */
	initEChartJS(options, chartId) {
		const chart = echarts.init(chartId.nativeElement);
		//createGraphFromNodeEdge.
		//console.log('chart');
		//console.log(chart);
		//console.log(options);
		chart.setOption(options);
	}
	
	downloadCSV() {	
		var filters = ["Field: Health","Time Range: 2005 - 2010" ];
		var fileContents = 	{
			csv: 'CSV8',
			data: this.rawData,
			areaFilter: this.areaFilter,
			subtitle: this.subtitle,
			title: this.title,
			source: this.source,
			indicator: 'Throughput/Output',
			filters: filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);	
	}
	
	downloadJSON() {
		console.log('JSON icon clicked');
		var filters = ["Field: Health","Time Range: 2005 - 2010"];
		var fileContents = 	{
			json: 'JSONTable',			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
	//Chart options
	l1_d2_collaborations(dData, dData2, country_cats, filter) {
		//console.log(dData);
		//var l1_d2_collaborations =  dData["countries"];
		//var l1_d2_collaborations =  dData["activity_types"];
		var l1_d2_collaborations =  dData[filter];
		this.rawData[1] = dData2[filter];
		this.rawData[0] = l1_d2_collaborations;		
		//console.log('l1_d2_collaborations');
		//console.log(l1_d2_collaborations);
		//console.log(dData);
		//console.log(dData2);

		//var nodes = [];
		var colors = ['#c23531', '#2f4554',  '#d48265', '#749f83',  '#ca8622', '#91c7ae', '#bda29a', '#61a0a8', '#6e7074', '#546570', '#c4ccd3'];
		var nodes = {};
		var ccc = 0;
		var color = [];
		var categories = [];
		var categories_dupl = [];
		var selectedCat = {};
		var node = {};
		var keyNum = 360 / (Object.keys(l1_d2_collaborations).length -1);
		var nodeSize = {
			"countries" : 53,
			"activity_types" : 120,
			"icd10categories" : 17,
		}
		var linkSize = {
			"countries" : 4000,
			"activity_types" : 15000,
			"icd10categories" : 34,
		}
		var linkSizePlus = {
			"countries" : 300,
			"activity_types" : 13,
			"icd10categories" : 0,
		}
		var symbolsize;
		//console.log(keyNum);
		Object.keys(l1_d2_collaborations).map((key, index) => {
			color.push(this.hsvToRgb(keyNum * index, 80, 80));
			/*var node = {"id":index, "name":key , "value":l1_d2_collaborations[key], "category":key,"symbolSize":(l1_d2_collaborations[key]+200)/nodeSize[filter], "itemStyle": {"normal":{"color": color[index]}} };
			nodes[key] = node;
			categories.push({"name":key, "itemStyle": {"color": color[index]}});*/
			
			if(filter=="countries"){
				//var countriesByCategory= dData['country_category'];
				var countriesByCategory= country_cats;
				//console.log(countriesByCategory);
				symbolsize = (l1_d2_collaborations[key]+200)/nodeSize[filter];
				node = {"id":index, "name":key , "value":l1_d2_collaborations[key], "category":countriesByCategory[key],"symbolSize":symbolsize, "itemStyle": {"normal":{}}, "label" : {"normal": {"show": (symbolsize > 10)}}  };
				nodes[key] = node;
				categories_dupl.push(countriesByCategory[key]);
				
			} else {
				node = {"id":index, "name":key , "value":l1_d2_collaborations[key], "category":key,"symbolSize":(l1_d2_collaborations[key]+200)/nodeSize[filter], "itemStyle": {"normal":{}} };
				nodes[key] = node;
				categories.push({"name":key,});
			};
				
		})
		if(filter=="countries"){
			categories = [];
			//console.log(categories_dupl);
			var categories_no_dup = categories_dupl.sort().filter(function(item, pos, ary) {
				return !pos || item != ary[pos - 1];
			});
			categories = categories_no_dup.map(single => {	
				if (single=="Western Europe" || single=="Eastern Europe" ) {
					selectedCat[single] = true;
				} else {
					selectedCat[single] = false;
				}
				return {"name":single};
			});			
		}
		//console.log(categories);
		//console.log(selectedCat);
		var nodesArray = [];
		Object.keys(nodes).map((key, index) => {
			nodesArray.push(nodes[key]);
		})
		//console.log(nodesArray);
		//console.log('nodes');
		//console.log(nodes);
		
		var links = [];
		var l1_d2_collaboration_links =  dData2[filter];
		console.log(l1_d2_collaboration_links);
		Object.keys(l1_d2_collaboration_links).map((key, index) => {
			Object.keys(l1_d2_collaboration_links[key]).map((linked_key, linked_index) => {
				var formatter = '';
				//console.log('nodes[key]');
				//console.log(nodes[key]);
				//console.log(linked_key);
				//console.log(nodes[linked_key]);
				var link = {"source":nodes[key]["id"], "target":nodes[linked_key]["id"], "value": l1_d2_collaboration_links[key][linked_key], "lineStyle": {"width": (l1_d2_collaboration_links[key][linked_key]+linkSizePlus[filter])/linkSize[filter], "opacity": 1}, "sourceName":nodes[key]["name"], "targetName":nodes[linked_key]["name"] };
				links.push(link);
				
			})
			
		})
		//console.log(links);
		//console.log(links);
		//var myChart_l1_d2_collaborations = echarts.init(document.getElementById('l1_d2_collaborations'));
		var source = 'Source text';
		var subtitle = '';
		switch(filter) {
			case 'countries' :
				subtitle = 'Countries';
			break;
			case 'activity_types' :
				subtitle = 'Organization Sectors';
			break;
			case 'icd10categories' :
				subtitle = 'Research Areas';
			break;
		}
		this.title = 'Project Participation & Collaborations across ' + subtitle;
		this.subtitle = subtitle;
		var chartOptions = {
			title: {
				text: this.title,
				left: 'left'
			},
			color: colors,
			graphic: [{
				type: 'text',
				z: 100,
				//left: 'center',
				top: 'bottom',
				style: {
					fill: '#333',
					text: [
						'Description/Source: '+ source
					].join('\n'),
					//font: '14px Microsoft YaHei'
				}
			},],
			tooltip: {
				formatter: function (params) {
					//console.log(params);
					if (params.dataType == 'edge') {
						//console.log('edge');
						if(filter=="icd10categories"){
							var sourceName = params.data.sourceName.substring(0, params.data.sourceName.indexOf(':'));
							var targetName = params.data.targetName.substring(0, params.data.targetName.indexOf(':'));
						} else {
							var sourceName = params.data.sourceName;
							var targetName = params.data.targetName;
						}
						return 'Collaborations ' + sourceName + ' - ' + targetName + ' : ' + params.data.value;
					} else {
						return 'Number of Projects <br>' + params.marker + params.data.name + ' : ' + params.data.value.toLocaleString();
					}
				}
			},
			width: '80%',
			height: '60%',
			toolbox: {
				right: 20,
				showTitle: false,
				feature: {
					saveAsImage: {title: 'Save image'},
					myToolDownloadSVG: {
						show: true,
						title: 'Download CSV',	
						icon: 'image:///assets/media/files/csv.png',
						onclick:(evt)=> {
							this.downloadCSV();
						},
					},
					myToolDownloadJSON: {
						show: true,
						title: 'Download JSON',	
						icon: 'image:///assets/media/files/json.png',
						onclick:(evt)=> {
							//console.log(this);
							this.downloadJSON();
						},
					}
				}
			},  
			legend: {
				//data: categories,
				type: 'scroll',
				orient: 'vertical',
				top: '40',
				height: 463,
				right: 0,
				//backgroundColor: '#fff',
				formatter: function(params) {
					//console.log('params');
					//console.log(params);
					return params.replace(/(.*?\s.*?\s.*?\s.*?\s)/g, '$1'+'\n');
				}
			},	
			animationDurationUpdate: 1500,
			animationEasingUpdate: 'quinticInOut',
			series : [
				{
					name: 'Number of Projects',
					type: 'graph',
					layout: 'circular',
					//top: '20%',
					//edgeLabel: {show: true},
					circular: {
						rotateLabel: true
					},
					data: nodesArray,
					links: links,
					categories: categories,
					roam: true,
					//right: 600,
					left: 0,
					label: {
						normal: {
							show: true,
							position: 'right',
							//formatter: '{b}'
							formatter: function(seriesName) {
								//console.log(seriesName);
								var sName;
								if(filter=="countries") {
									sName = seriesName.name;
								} else {
									sName = seriesName.name.substring(0, seriesName.name.indexOf(':'));
								}
								
								return sName;
								
							}
						}
					},
					lineStyle: {
						normal: {
							color: 'source',
							curveness: 0.3
						}
					}
				}
			]
		}
		if (filter=='countries') {
			//chartOptions["legend"]["selected"] = {"Western Europe":true, "Eastern Europe":true};
			chartOptions["legend"]["selected"] = selectedCat;
		}
		//console.log(chartOptions);
		return chartOptions;

	}
	
	
	//Modify colors
	hsvToRgb(h, s, v) {
		var r, g, b;
		var i;
		var f, p, q, t;
	 
		// Make sure our arguments stay in-range
		h = Math.max(0, Math.min(360, h));
		s = Math.max(0, Math.min(100, s));
		v = Math.max(0, Math.min(100, v));
	 
		// We accept saturation and value arguments from 0 to 100 because that's
		// how Photoshop represents those values. Internally, however, the
		// saturation and value are calculated from a range of 0 to 1. We make
		// That conversion here.
		s /= 100;
		v /= 100;
	 
		if(s == 0) {
			// Achromatic (grey)
			r = g = b = v;
			return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
		}
	 
		h /= 60; // sector 0 to 5
		i = Math.floor(h);
		f = h - i; // factorial part of h
		p = v * (1 - s);
		q = v * (1 - s * f);
		t = v * (1 - s * (1 - f));
	 
		switch(i) {
			case 0:
				r = v;
				g = t;
				b = p;
				break;
	 
			case 1:
				r = q;
				g = v;
				b = p;
				break;
	 
			case 2:
				r = p;
				g = v;
				b = t;
				break;
	 
			case 3:
				r = p;
				g = q;
				b = v;
				break;
	 
			case 4:
				r = t;
				g = p;
				b = v;
				break;
	 
			default: // case 5:
				r = v;
				g = p;
				b = q;
		}
	 
		return "rgb("+Math.round(r * 255)+","+ Math.round(g * 255)+","+ Math.round(b * 255)+")";
	}
	
	
	
	
}
