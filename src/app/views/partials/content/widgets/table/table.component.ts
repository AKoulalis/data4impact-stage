// Angular
import { Component, ElementRef, Input, OnInit, OnChanges, OnDestroy, ViewChild } from '@angular/core';
import { map } from 'rxjs/operators';

// Layout
import { LayoutConfigService } from '../../../../../core/_base/layout';
import { LoadJsonService, DownloadCSVService, DownloadJSONService } from '../../../../../core/_base/layout';

import { HttpClient } from '@angular/common/http'; 


@Component({
	selector: 'kt-table',
	templateUrl: './table.component.html',
	styleUrls: ['./table.component.scss'],
	providers: [LayoutConfigService],
})
export class TableComponent implements OnInit {
	// Public properties
	@Input() chartTitle: string;
	@Input() chartData: any;
	source : string = 'Source text';
	//@Input() desc: string;
	@Input() data: { labels: string[]; datasets: any[] };
	@ViewChild('table', {static: true}) table: ElementRef;
	title = '';
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private layoutConfigService: LayoutConfigService, private loadJsonService : LoadJsonService, private http: HttpClient, private downloadCSVService : DownloadCSVService, private downloadJSONService : DownloadJSONService ) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if(this.chartData){
			this.title = this.chartData['title'];
			this.l1_d4_innovations(this.chartData);
		}
		/*this.loadJsonService.getTitle(this.chartTitle);
		this.loadJsonService.getJSON().subscribe(data => {
			this.l1_d4_innovations(data);
        });*/		
	}
	
	//Table options
	l1_d4_innovations(innovations) {
		innovations = innovations['D4I_Dataset'];
		var l1_d4_innovationsHtml = '';
		var l1_d4_TableBody = '';
		var headData = [];
		innovations.map((innovation) => {
			l1_d4_TableBody += '<tr>';
			Object.keys(innovation).map((innerKey, innerIndex) => {
				if(innovations.indexOf(innovation)==0) {
					headData.push(innerKey);
				}
				l1_d4_TableBody += '<td>'+innovation[innerKey]+'</td>';
			});
			//l1_d4_TableBody += '<tr><td>'+innovation[headData[0]]+'</td><td>'+innovation[headData[1]]+'</td></tr>';
			l1_d4_TableBody += '<tr>';
		});
		var tableHead = '<div><h3 class="table-title">'+ this.title +'</h3><table class="table"><thead><tr><th>'+ headData[0] +'</th><th>'+headData[1]+'</th></tr></thead><tbody>';
		l1_d4_innovationsHtml += tableHead;
		l1_d4_innovationsHtml += l1_d4_TableBody;
		l1_d4_innovationsHtml += '</tbody></table></div>';
		let tableContainer : HTMLDivElement | null =  this.table.nativeElement;
		if (tableContainer instanceof HTMLDivElement) {
			tableContainer.innerHTML = l1_d4_innovationsHtml;
		}
	}
	
	downloadCSV() {
		console.log('icon clicked');
		console.log(this.chartData);	
		//var filename = 'tttttt';
		var filters = ["Field: Health","Time Range: 2005 - 2010"];
		var fileContents = 	{
			csv: 'CSVTable',
			data: this.chartData,
			title: this.title,
			source: this.source,
			indicator: 'Input',
			filters: filters.join(', ')
		};	
		this.downloadCSVService.JSONToCSVConvertor(fileContents);		
	}
	
	downloadJSON() {
		console.log('JSON icon clicked');
		var filters = ["Field: Health","Time Range: 2005 - 2010"];
		var fileContents = 	{
			json: 'JSONTable',			
			title: this.title,
			source: this.source,
			indicator: 'Input',
			data: this.chartData,
			filters: filters.join(', ')
		};	
		this.downloadJSONService.DownloadJSON(fileContents);
	}
	
}
