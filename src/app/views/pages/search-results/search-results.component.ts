import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { SearchKeywordService, LoadJsonService } from "../../../core/_base/layout/";


@Component({
  selector: 'kt-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class SearchResultsComponent implements OnInit {
	//@Input() keyword: any;
	keyword: any;
	loading: boolean = false;
	//keywordSearch:any = '';
	response = {
	 /* "success": 1,
	  "message": "received : ear111111111",
	  "results": [
		{
		  "keywords": [
			"hearing loss",
			"middle ear",
			"hearing impairment",
			"cochlear implant",
			"box hmgb",
			"normal hearing",
			"outer hair",
			"spiral ganglion",
			"cochlear implants",
			"cochlear implantation"
		  ],
		  "title": "Auditory system and hearing loss--------",
		  "slug": "topic_294",
		  "desc": "A description!",
		  "score": 2.0,
		  "field": {
			"title": "HEALTH",
			"slug": "field_health"
		  },
		  "categories": [
			{
			  "title": "Physiology",
			  "slug": "category_7"
			}
		  ]
		}
	  ]*/
	};
	
	constructor(private searchKeywordService : SearchKeywordService, private loadJsonService : LoadJsonService, private cdRef: ChangeDetectorRef) {
		//this.response.subscribe();
	}
 
	ngOnInit() {
		
	  	this.searchKeywordService.sKeyword.subscribe(c => {
			//console.log('starting');
            this.keyword = c;
			//console.log( this.keyword);
			if(c) {
				this.loading = true;
				this.loadJsonService.getSearch(c).subscribe(returnedData => {			
					//console.log('SearchResultsComponent data');
					//console.log(this.response);
					if (this.response !== returnedData['data']) {
						this.cdRef.markForCheck();
					}
					this.response = returnedData['data'];
					//this.response = returnedData;
					console.log('New this.response');
					//console.log(returnedData);
					console.log(this.response);
					this.loading = false;
					//console.log(this.loading);
					this.keyword = c;
				});
			}
			//this.loadJsonService.getSearch();
        });
	}
	/*
	search() {
		this.searchKeywordService.changeKeyword(this.keywordSearch);
	}*/

}
