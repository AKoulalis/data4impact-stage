// Angular
import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// Services
import { LoadJsonService } from '../../../core/_base/layout';
//import * as jsPDF from 'jspdf';
//import * as html2canvas from 'html2canvas';
import printJS from 'print-js';

@Component({
	selector: 'kt-print-report',
	templateUrl: './print-report.component.html',
	styleUrls: ['print-report.component.scss'],
})


export class PrintReportComponent implements OnInit {	
	chartData: any;
	chartId: any;
	private sub: any;
	
	constructor(private loadJsonService : LoadJsonService, private cdRef: ChangeDetectorRef, private route: ActivatedRoute) {
	}

	public downloadAsPDF() {		
		/*printJS({
			printable: 'print_area',
			type:'html',
			targetStyles: ['*'] 
		});*/
		//window["html2canvas"]  = html2canvas;
	   window.print();
	   return;
		//var doc = new jsPDF();
		//doc.text('Hello world!', 10, 10);
		//doc.save('a4.pdf');
		//doc.html(document.body, {
		/*doc.addHTML(document.getElementById('print_area'), {
		   callback: function (doc) {
			 doc.save();
		   }
		});*/
		/*doc.addHTML(document.getElementById('print_area')).then(canvas => {

		});*/
	}
  
	ngOnInit(): void {
		this.sub = this.route.params.subscribe(params => {
			this.chartId = params['id'];
			this.loadJsonService.getChart(this.chartId).subscribe(data => {			
				console.log('Dashboard getChart data');
				console.log(data);
				if (this.chartData !== data) {
					this.cdRef.markForCheck();
				}
				this.chartData = data['data']['results']['charts'];
			});
		});
	}
	
	ngOnDestroy() {
		this.sub.unsubscribe();
	}	
	
}
