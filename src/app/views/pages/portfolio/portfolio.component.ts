import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { PortfolioService } from '../../../core/_base/layout';
import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { currentUser, Logout, User, CurrentUserUpdated } from '../../../core/auth';
// RxJS
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
// NGRX
import { select, Store } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// State
import { AppState } from '../../../core/reducers';

@Component({
  selector: 'kt-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
	user$: Observable<any>;
	//currentUser: User;
	currentPortUser: any;
	modalTitle: string = '';
	objectKeys = Object.keys;
	organization: any =  '["ORG:EC"]';
	program: any =  '["PROG:FP7-ENVIRONMENT"]';
	call: any =  '["CALL:FP7-ENV-2012-one-stage"]';
	project: any =  'ORG:EC';
	jsonList:any;
	jsonSubList:any;
	showList = false;
	selectedOptions:any;
	selectedOption: any;
	organizationOptions:any;
	programOptions:any;
	callOptions:any;
	projectOptions:any;
	updateOrganizationList = false;
	updateProgramList = false;
	updateCallList = false;
	updateProjectList = false;
	returnedJSONData = {};
	JSONOrganizationData = {};
	JSONProgramData = {};
	JSONCallData = {};
	JSONProjectData = {};
	organizationList = [{name:''}];
	programData = {};
	newOrganizationOptions = [];
	newProgramOptions = [];
	newCallOptions = [];
	newProjectOptions = [];
	saveOrganizationOptions = [];
	saveProgramOptions = [];
	saveCallOptions = [];
	saveProjectOptions = [];
	//userInitialPortfolio: Observable<any>;
	userInitialPortfolio: any;
	portfolioUpdated = false;
	portfolioNotUpdated = false;
	searchText: any = '';
	searchTextProgram: any = '';
	searchTextCall: any = '';
	searchTextProject: any = '';
	//callData = {};
	//projectData = {};
	itemToBeRemovedName: string = '';
	itemToBeRemovedSlug: string = '';
	itemToBeRemovedLevel: string = '';

	singleProgramOptions = [];
	singleCallOptions = [];
	singleProjectOptions = [];
	@ViewChild('remove_item', {static: false}) remove_item;

	usersPermittedIds = ['5ddbaaf17f4adc336a6bec25',
		'5ddbc97437092744a21c9044',
		'5ddfcf3dc7a348798f889259',
		'5ddfcfb6c7a348798f88925a',
		// '5e20374a2da7bd9e3e3e12f4',
		'5e466637b6e6f00efff443f7',
		'5e4a673d43289b0651549b49',
		'5e4a6cec43289b0651549b4b',
		'5e4fa7fc657d22a2f1d04ebc'];
	permittedAllUser = true;
	tempUserData = null;

	constructor(private modalService: NgbModal, private portfolioService : PortfolioService, private store: Store<AppState>, private cd: ChangeDetectorRef) {

	}

	/* Open Modal and load Portfolio list */
	addToPortfolio(content, title, callType, callData, selected) {
		console.log('addToPortfolio');
		console.log(selected);
		console.log(this[selected]);
		/* This is where organizationOptions etc. get linked to the NgModel */
		this.selectedOption = selected;
		//console.log(this.selectedOption);
		this.modalTitle = title;
		//console.log(title);
		//console.log(callType);
		//console.log(callData);
		//console.log(this.showList);
		this.portfolioService.getPortfolio(callType, callData).subscribe(returnedData => {
			//console.log(returnedData);
			this.jsonList = returnedData.data;
			console.log('this.jsonList');
			console.log(this.jsonList);
			switch(title){
				case 'Organizations':
					this.JSONOrganizationData = this.jsonList;
				break;
				case 'Programs':
					this.JSONProgramData = this.jsonList;
				break;
				case 'Calls':
					this.JSONCallData = this.jsonList;
				break;
				case 'Projects':
					this.JSONProjectData = this.jsonList;
				break;
			}
			this.selectedOption = selected;
			this.showList = true;
        });


		this.modalService.open(content).result.then((result) => {
			this.searchText = '';
			console.log('closed');
			this.showList = false;
			}, (reason) => {
				console.log('dismissed');
				this.showList = false;
        });

	}

	/* Open Modal and load Portfolio list PER ITEM */
	addToPortfolioPerItem(content, title, callType, callData, selected) {
		console.log('addToPortfolioPerItem');
		console.log(selected);
		console.log(this[selected]);
		/* This is where organizationOptions etc. get linked to the NgModel */
		this.selectedOption = selected;
		console.log(this.selectedOption);
		//title of level which will be affected
		this.modalTitle = title;
		//console.log(title);
		//console.log(callType);
		//console.log(callData);
		console.log(this.showList);
		this.portfolioService.getPortfolioPerItem(callType, callData).subscribe(returnedData => {
			//console.log('Dashboard getChart data');
			//console.log(returnedData);
			this.jsonSubList = returnedData['data'];
			console.log('this.jsonList');
			console.log(this.jsonSubList);
			switch(title){
				case 'Programs':
					//this.jsonSubList = this.jsonSubList.programs;
					this.JSONProgramData = this.jsonSubList;
				break;
				case 'Calls':
					//this.jsonSubList = this.jsonSubList.calls;
					this.JSONCallData = this.jsonSubList;
				break;
				case 'Projects':
					//this.jsonSubList = this.jsonSubList.projects;
					this.JSONProjectData = this.jsonSubList;

					console.log();
				break;
			}
			this.selectedOption = selected;
			this.showList = true;
        });


		this.modalService.open(content).result.then((result) => {
			this.searchText = '';
			console.log('closed');
			this.showList = false;
			}, (reason) => {
				console.log('dismissed');
				this.showList = false;
        });

	}

	// Open Modal to verify we want to remove a portfolio item
	removePortfolioModal(callType, itemSlug, itemName) {
		console.log('removePortfolioModal');
		this.showList = true;
		this.itemToBeRemovedSlug = itemSlug;
		this.itemToBeRemovedName = itemName;
		this.itemToBeRemovedLevel = callType;
		console.log(this.itemToBeRemovedSlug);
		console.log(itemName);
		this.modalService.open(this.remove_item).result.then((result) => {
			this.searchText = '';
			//console.log('closed');
			this.showList = false;
			this.itemToBeRemovedSlug = '';
			this.itemToBeRemovedName = '';
			this.itemToBeRemovedLevel = '';
			}, (reason) => {
				//console.log('dismissed');
				this.showList = false;
				this.itemToBeRemovedSlug = '';
				this.itemToBeRemovedName = '';
				this.itemToBeRemovedLevel = '';
        });
	}

	removePortfolioItem() {
		console.log('removePortfolioItem');
		console.log(this.itemToBeRemovedLevel, this.itemToBeRemovedSlug);
		const term = this.itemToBeRemovedLevel;
		switch(term){
			case 'Organizations':
				this.newOrganizationOptions = [];
				this.saveOrganizationOptions = [];
				this.updateOrganizationList = true;

				//Remove all programs belonging to organization
				this.portfolioService.removePortfolioSubItem('program', this.itemToBeRemovedSlug).subscribe(returnedData => {
					console.log(returnedData);
					this.jsonSubList = returnedData['data'];
					console.log('this.jsonList');
					console.log(this.jsonSubList);
					console.log(this.newProgramOptions);

					let newOptions = (this.newProgramOptions).filter( item => {
						//console.log(item);
						let remove:Boolean = false;
						this.jsonSubList.map( option => {
							if (item.slug == option.slug) {
								remove = true;
							}
							return '';
						});
						console.log(remove);
						return remove != true;
					});
					this.cd.markForCheck();
					this.newProgramOptions = newOptions;
					this.saveProgramOptions = newOptions;
					console.log(newOptions);
					console.log(this.newProgramOptions);

					//Remove all calls belonging to all to-be-removed programs
					this.jsonSubList.map( singleProgram => {
						this.portfolioService.removePortfolioSubItem('call', singleProgram.slug).subscribe(returnedCalls => {
							const calls = returnedCalls['data'];
							let newCallOptions = (this.newCallOptions).filter( item => {
								//console.log(item);
								let remove:Boolean = false;
								calls.map( option => {
									if (item.slug == option.slug) {
										remove = true;
									}
									return '';
								});
								console.log(remove);
								return remove != true;
							});
							this.cd.markForCheck();
							this.newCallOptions = newCallOptions;
							this.saveCallOptions = newCallOptions;

							//Remove all projects belonging to all to-be-removed calls
							calls.map( singleCall => {
								this.portfolioService.removePortfolioSubItem('project', singleCall.slug).subscribe(returnedProjects => {
									const projects = returnedProjects['data'];
									let newProjectOptions = (this.newProjectOptions).filter( item => {
										//console.log(item);
										let remove:Boolean = false;
										projects.map( option => {
											if (item.slug == option.slug) {
												remove = true;
											}
											return '';
										});
										console.log(remove);
										return remove != true;
									});
									this.cd.markForCheck();
									this.newProjectOptions = newProjectOptions;
									this.saveProjectOptions = newProjectOptions;
								});
							});


						});
					});

				});


				//Remove item from Portfolio list
				this.newOrganizationOptions = (this.newOrganizationOptions).filter( item => {
					return item.slug != this.itemToBeRemovedSlug;
				});
				this.saveOrganizationOptions = (this.saveOrganizationOptions).filter( item => {
					return item.slug != this.itemToBeRemovedSlug;
				});
				console.log(this.newOrganizationOptions);
				console.log(this.newOrganizationOptions);
			break;
			case 'Programs':
				this.updateProgramList = true;
				console.log(this.JSONProgramData);
				//Remove all calls belonging to program
				this.portfolioService.removePortfolioSubItem('call', this.itemToBeRemovedSlug).subscribe(returnedData => {
					console.log(returnedData);
					this.jsonSubList = returnedData['data'];
					console.log('this.jsonList');
					console.log(this.jsonSubList);
					console.log(this.newCallOptions);

					let newOptions = (this.newCallOptions).filter( item => {
						//console.log(item);
						let remove:Boolean = false;
						this.jsonSubList.map( option => {
							if (item.slug == option.slug) {
								remove = true;
							}
							return '';
						});
						console.log(remove);
						return remove != true;
					});
					this.cd.markForCheck();
					this.newCallOptions = newOptions;
					this.saveCallOptions = newOptions;
					console.log(newOptions);
					console.log(this.newCallOptions);

					//Remove all projects belonging to all to-be-removed calls
					this.jsonSubList.map( singleCall => {
						this.portfolioService.removePortfolioSubItem('project', singleCall.slug).subscribe(returnedProjects => {
							const projects = returnedProjects['data'];
							let newProjectOptions = (this.newProjectOptions).filter( item => {
								//console.log(item);
								let remove:Boolean = false;
								projects.map( option => {
									if (item.slug == option.slug) {
										remove = true;
									}
									return '';
								});
								console.log(remove);
								return remove != true;
							});
							this.cd.markForCheck();
							this.newProjectOptions = newProjectOptions;
							this.saveProjectOptions = newProjectOptions;
						});
					});

				});


				//Remove item from Portfolio list
				this.newProgramOptions = (this.newProgramOptions).filter( item => {
					return item.slug != this.itemToBeRemovedSlug;
				});
				this.saveProgramOptions = (this.saveProgramOptions).filter( item => {
					return item.slug != this.itemToBeRemovedSlug;
				});
				console.log(this.newProgramOptions);
			break;
			case 'Calls':
				//this.newCallOptions = [];
				//this.saveCallOptions = [];
				this.updateCallList = true;
				//Remove sub items from Portfolio list
				this.portfolioService.removePortfolioSubItem('project', this.itemToBeRemovedSlug).subscribe(returnedData => {
					console.log(returnedData);
					this.jsonSubList = returnedData['data'];
					console.log('this.jsonList');
					console.log(this.jsonSubList);
					console.log(this.newProjectOptions);
					/*let newOptions = (this.newProjectOptions).map( item => {
						console.log(item);
						let remove:Boolean = false;
						this.jsonSubList.map( option => {
							console.log('option');
							console.log(option);
							if (item.slug == option.slug) {
								remove = true;
							}
							return '';
						});
						if (remove != true) {
							return item;
						}
					});*/
					let newOptions = (this.newProjectOptions).filter( item => {
						//console.log(item);
						let remove:Boolean = false;
						this.jsonSubList.map( option => {
							if (item.slug == option.slug) {
								remove = true;
							}
							return '';
						});
						console.log(remove);
						return remove != true;
						/*if (remove == true) {
							return false;
						}
						console.log('don t remove');
						return true;*/
						//return remove != true;
					});
					this.cd.markForCheck();
					this.newProjectOptions = newOptions;
					this.saveProjectOptions = newOptions;
					console.log(newOptions);
					console.log(this.newProjectOptions);
					/*this.saveProjectOptions = (this.saveProjectOptions).filter( item => {
						return item.slug != this.itemToBeRemovedSlug;
					});*/
					/*switch(title){
						case 'Programs':
							//this.jsonSubList = this.jsonSubList.programs;
							this.JSONProgramData = this.jsonSubList;
						break;
						case 'Calls':
							//this.jsonSubList = this.jsonSubList.calls;
							this.JSONCallData = this.jsonSubList;
						break;
						case 'Projects':
							//this.jsonSubList = this.jsonSubList.projects;
							this.JSONProjectData = this.jsonSubList;

							console.log();
						break;
					}
					*/
				});


				//Remove item from Portfolio list
				this.newCallOptions = (this.newCallOptions).filter( item => {
					return item.slug != this.itemToBeRemovedSlug;
				});
				this.saveCallOptions = (this.saveCallOptions).filter( item => {
					return item.slug != this.itemToBeRemovedSlug;
				});

				//console.log(this.newCallOptions);
			break;
			case 'Projects':
				this.updateProjectList = true;
				this.newProjectOptions = (this.newProjectOptions).filter( item => {
					return item.slug != this.itemToBeRemovedSlug;
				});
				this.saveProjectOptions = (this.saveProjectOptions).filter( item => {
					return item.slug != this.itemToBeRemovedSlug;
				});
				//console.log(this.newProjectOptions);
			break;
		}
	}

	onNgModelChange(list){
		//console.log('onNgModelChange');
	}

	/* Save portfolio to DB */
	savePortfolio() {
		console.log('savePortfolio');
		var portfolioObject = {
		"portfolio":
			{
				"organizations": this.saveOrganizationOptions,
				"programs": this.saveProgramOptions,
				"calls": this.saveCallOptions,
				"projects": this.saveProjectOptions
			}
		}
		//console.log('portfolioObject to be saved');
		//this.currentPortUser.portfolio = portfolioObject.portfolio;
		this.portfolioService.savePortfolio(portfolioObject).subscribe(returnedData => {
			//console.log('Portfolio data');
			//console.log(returnedData);
			var returnedData = returnedData;
			//console.log('this.jsonList');
			//console.log(returnedData);
			if(returnedData.success === true) {
				//console.log(this.currentPortUser);
				this.currentPortUser = {
					...this.currentPortUser,
					portfolio: portfolioObject.portfolio
				};
				//console.log(this.currentPortUser);
				this.store.dispatch(new CurrentUserUpdated({
					user: this.currentPortUser
				}));
				this.portfolioUpdated = true;
				this.cd.markForCheck();
				setTimeout(()=>{
					this.portfolioUpdated = false;
					this.cd.markForCheck();
				}, 1500);

			} else {
				this.portfolioNotUpdated = true;
				this.cd.markForCheck();
				setTimeout(()=>{
					this.portfolioNotUpdated = false;
					this.cd.markForCheck();
				}, 1500);
			}

        });

	}

	/* Update user portfolio with new portfolio items from Modal */
	updateList(term) {
		console.log('updateList');
		console.log(term);
		switch(term){
			case 'Organizations':
				this.newOrganizationOptions = [];
				this.saveOrganizationOptions = [];

				this.updateOrganizationList = true;
				//var options = this.JSONOrganizationData[0]['organizations'];
				//This is the data we got from our API call when we opened the modal
				var options:any = this.JSONOrganizationData;
				console.log(options);
				//We loop through the selected list titles.
				(this.organizationOptions).map( data1 => {
					options.map( option => {
						if (option.title == data1) {
							this.newOrganizationOptions.push({title: data1, slug: option.slug});
							this.saveOrganizationOptions.push({title: data1, slug: option.slug});
						}
					});
					/*if (options.hasOwnProperty(data1)) {
						this.newOrganizationOptions.push({title: data1, slug: options[data1]});
						this.saveOrganizationOptions.push({title: data1, slug: options[data1]});
					}*/
				});
				console.log(this.newOrganizationOptions);
			break;
			case 'Programs':
				this.newProgramOptions = [];
				this.saveProgramOptions = [];
				this.updateProgramList = true;
				console.log(this.JSONProgramData);
				//var options = this.JSONProgramData[0]['programs'];
				var options:any = this.JSONProgramData;
				console.log(options);
				console.log(this.programOptions);
				(this.programOptions).map( data1 => {
					options.map( option => {
						if (option.title == data1) {
							this.newProgramOptions.push({title: data1, slug: option.slug});
							this.saveProgramOptions.push({title: data1, slug: option.slug});
						}
					});
					/*if (options.hasOwnProperty(data1)) {
						this.newProgramOptions.push({title: data1, slug: options[data1]});
						this.saveProgramOptions.push({title: data1, slug: options[data1]});
					}*/
				});
				console.log(this.newProgramOptions);
			break;
			case 'Calls':
				this.newCallOptions = [];
				this.saveCallOptions = [];
				this.updateCallList = true;
				//var options = this.JSONCallData[0]['calls'];
				var options:any = this.JSONCallData;
				console.log(options);
				(this.callOptions).map( data1 => {
					options.map( option => {
						if (option.title == data1) {
							this.newCallOptions.push({title: data1, slug: option.slug});
							this.saveCallOptions.push({title: data1, slug: option.slug});
						}
					});
					/*if (options.hasOwnProperty(data1)) {
						this.newCallOptions.push({title: data1, slug: options[data1]});
						this.saveCallOptions.push({title: data1, slug: options[data1]});
					}*/
				});
				console.log(this.newCallOptions);
			break;
			case 'Projects':
				this.newProjectOptions = [];
				this.saveProjectOptions = [];
				this.updateProjectList = true;
				//var options = this.JSONProjectData[0]['projects'];
				var options:any = this.JSONProjectData;
				//console.log(this.JSONProjectData);
				//console.log(this.projectOptions);
				(this.projectOptions).map( data1 => {
					options.map( option => {
						if (option.title == data1) {
							this.newProjectOptions.push({title: data1, slug: option.slug});
							this.saveProjectOptions.push({title: data1, slug: option.slug});
						}
					});
					/*if (options.hasOwnProperty(data1)) {
						this.newProjectOptions.push({title: data1, slug: options[data1]});
						this.saveProjectOptions.push({title: data1, slug: options[data1]});
					}
					*/
				});
				//console.log(this.newProjectOptions);
				//this.projectData =
			break;
		}
	}


	/* Update user portfolio with new portfolio items from Modal per parent item. 'term' is equal to the level which will be affected. */
	updateSubList(term) {
		console.log('updateSubList');
		console.log(term);
		switch(term){
			/*case 'Organizations':
				this.newOrganizationOptions = [];
				this.saveOrganizationOptions = [];

				this.updateOrganizationList = true;
				//var options = this.JSONOrganizationData[0]['organizations'];
				//This is the entire data we got from our API call when we opened the modal
				var options:any = this.JSONOrganizationData;
				console.log(options);
				(this.organizationOptions).map( data1 => {
					options.map( option => {
						if (option.title == data1) {
							this.newOrganizationOptions.push({title: data1, slug: option.slug});
							this.saveOrganizationOptions.push({title: data1, slug: option.slug});
						}
					});
				});
				console.log(this.newOrganizationOptions);
			break;*/
			case 'Programs':
				//this.newProgramOptions = [];
				//this.saveProgramOptions = [];
				var tempSingleProgramOptions = [];
				this.updateProgramList = true;
				console.log(this.JSONProgramData);
				//var options = this.JSONProgramData[0]['programs'];
				var options:any = this.JSONProgramData;
				console.log(options);
				(this.programOptions).map( data1 => {
					options.map( option => {
						if (option.title == data1) {
							tempSingleProgramOptions.push({title: data1, slug: option.slug});
						}
					});
				});
				var titleArray = new Set(this.newProgramOptions.map(d => d.title));
				console.log(titleArray);
				var merged = [...this.newProgramOptions, ...tempSingleProgramOptions.filter(d => !titleArray.has(d.title))];
				this.newProgramOptions = merged;
				this.saveProgramOptions = merged;
				//update programOptions for ngModel
				var newtitleArray = (this.newProgramOptions.map(d => d.title));
				this.programOptions = newtitleArray;
				console.log(this.programOptions);
				console.log(merged);
			break;
			case 'Calls':
				//this.newCallOptions = [];
				//this.saveCallOptions = [];
				var tempSingleCallOptions = [];
				this.updateCallList = true;
				//var options = this.JSONCallData[0]['calls'];
				var options:any = this.JSONCallData;
				console.log(options);
				console.log(this.singleCallOptions);
				//We loop through the selected list titles.
				(this.singleCallOptions).map( data1 => {
					options.map( option => {
						if (option.title == data1) {
							tempSingleCallOptions.push({title: data1, slug: option.slug});
						}
					});
				});
				var titleArray = new Set(this.newCallOptions.map(d => d.title));
				console.log(titleArray);
				var merged = [...this.newCallOptions, ...tempSingleCallOptions.filter(d => !titleArray.has(d.title))];
				this.newCallOptions = merged;
				this.saveCallOptions = merged;
				var newtitleArray = (this.newCallOptions.map(d => d.title));
				this.callOptions = newtitleArray;
				console.log(this.callOptions);
				console.log(merged);
			break;
			case 'Projects':
				//this.newProjectOptions = [];
				//this.saveProjectOptions = [];
				var tempSingleProjectOptions = [];
				this.updateProjectList = true;
				//var options = this.JSONProjectData[0]['projects'];
				var options:any = this.JSONProjectData;
				//console.log(this.JSONProjectData);
				//console.log(this.projectOptions);
				(this.projectOptions).map( data1 => {
					options.map( option => {
						if (option.title == data1) {
							tempSingleProjectOptions.push({title: data1, slug: option.slug});
						}
					});
				});
				var titleArray = new Set(this.newProjectOptions.map(d => d.title));
				console.log(titleArray);
				var merged = [...this.newProjectOptions, ...tempSingleProjectOptions.filter(d => !titleArray.has(d.title))];
				this.newProjectOptions = merged;
				this.saveProjectOptions = merged;
				console.log(merged);
			break;
		}
	}

	ngOnInit() {
		//console.log('userInitialPortfolio -  userData1-');
		this.loadUserPortfolio();

	}

	loadUserPortfolio() {
		//this.user$ = this.store.pipe(select(currentUser));
		//console.log(this.user$);
		this.store.pipe(select(currentUser)).subscribe(userData => {
			//console.log('user portfolio');
			if(userData) {
				this.tempUserData = userData.firstname;
				if ( this.usersPermittedIds.indexOf(userData.id) < 0 ) {
					this.permittedAllUser = false;
				}
				//console.log(userData);
				this.currentPortUser = userData;
				if(userData.portfolio) {
					this.organizationOptions = (userData.portfolio.organizations).map( data => {
						return data.title;
					});
					this.newOrganizationOptions = userData.portfolio.organizations;
					this.saveOrganizationOptions = userData.portfolio.organizations;
					/*this.saveOrganizationOptions.push({title: data1, slug: options[data1]});
					(this.newOrganizationOptions).map( data => {
						this.saveOrganizationOptions.push({title: data1, slug: options[data1]});
					});*/
					this.programOptions = (userData.portfolio.programs).map( data => {
						return data.title;
					});
					this.newProgramOptions = userData.portfolio.programs;
					this.saveProgramOptions = userData.portfolio.programs;
					this.callOptions = (userData.portfolio.calls).map( data => {
						return data.title;
					});
					this.singleCallOptions = this.callOptions;
					this.newCallOptions = userData.portfolio.calls;
					this.saveCallOptions = userData.portfolio.calls;
					this.projectOptions = (userData.portfolio.projects).map( data => {
						return data.title;
					});
					this.newProjectOptions = userData.portfolio.projects;
					this.saveProjectOptions = userData.portfolio.projects;
					//console.log(userData);
					//console.log(this.newCallOptions);
					//console.log(this.saveProjectOptions);
					//console.log(userData.portfolio);
					//console.log(userData.portfolio.calls);
					//return userData.portfolio;
				}
			} else {
				//return;
			}

		});


	}

}
