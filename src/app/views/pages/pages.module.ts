// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
import { MailModule } from './apps/mail/mail.module';
//import { ECommerceModule } from './apps/e-commerce/e-commerce.module';
import { UserManagementModule } from './user-management/user-management.module';
import { MyPageComponent } from './my-page/my-page.component';
import { LandingComponent } from './landing/landing.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { AnalyzeDataComponent } from './analyze-data/analyze-data.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { BrowseTopicsComponent } from './browse-topics/browse-topics.component';
import { PrintReportComponent } from './print-report/print-report.component';


//Bootstrap
import { NgbAlertConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';
//Material
import { MatAutocompleteModule,MatFormFieldModule,MatInputModule,MatExpansionModule,MatButtonModule,MatSelectModule,MatListModule} from '@angular/material';
// Translate
import { TranslateModule } from '@ngx-translate/core';

//import { HintModule } from 'angular-custom-tour'
import {GuidedTourModule, GuidedTourService} from 'ngx-guided-tour';

@NgModule({
	declarations: [MyPageComponent,LandingComponent,PortfolioComponent,AnalyzeDataComponent,UserDashboardComponent,InstructionsComponent,SearchResultsComponent,BrowseTopicsComponent,PrintReportComponent],
	exports: [
		MatAutocompleteModule,
		MatFormFieldModule,
		MatInputModule,
		MatExpansionModule,
		MatButtonModule,
		MatSelectModule,
		MatListModule
	],
	imports: [
		CommonModule,
		RouterModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		CoreModule,
		PartialsModule,
		MailModule,
		//ECommerceModule,
		UserManagementModule,
		NgbModule,
		MatAutocompleteModule,
		MatFormFieldModule,
		MatInputModule,
		MatExpansionModule,
		MatButtonModule,
		MatSelectModule,
		MatListModule,
		//HintModule,
		GuidedTourModule,
		TranslateModule.forChild(),
	],
	providers: [
		NgbAlertConfig,
		GuidedTourService,
	],
})
export class PagesModule {
}
