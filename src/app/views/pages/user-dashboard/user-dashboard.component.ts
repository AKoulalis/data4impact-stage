import { Component, OnInit, OnDestroy } from '@angular/core';
import { currentUser } from '../../../core/auth';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
// State
import { AppState } from '../../../core/reducers';

@Component({
  selector: 'kt-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {
	user$: Observable<any>;
	favourites: Array<object> = [ 
		{
			'1oo': {
				'Field': 'Health',
				'Program': 'FP7',
				'Call': 'ImpactfulData2020',
			},
			'2oo': {
				'Country': ['France', 'Greece', 'Italy'], 
				'Time': [2018, 2019]
			}
		},
		{
			'1': {
				'Field': 'Health',
				'Program': 'FP7',
			},
		},
		{
			'1': {
				'Organization': 'ATHENA RC', 
				'Topic': 'Cardiovascular diseases',
			},
			'2': {
				'Time': [2008, 2015]
			}
		},
	];
	constructor(private store: Store<AppState>) {

	}

	ngOnInit() {
	  //
	  	this.user$ = this.store.pipe(select(currentUser));
	}

}
