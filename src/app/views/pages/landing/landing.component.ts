import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { LayoutConfigService, SearchKeywordService } from '../../../core/_base/layout';
import {Observable} from 'rxjs';
import { catchError, tap, map, filter, debounceTime, distinctUntilChanged, switchMap, startWith } from 'rxjs/operators';
import {FormControl} from '@angular/forms';
// NGRX
import { select, Store } from '@ngrx/store';
//import { NgbModal, NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {GuidedTourModule, GuidedTourService} from 'ngx-guided-tour';
import { GuidedTour, Orientation } from 'ngx-guided-tour';
//import * as jsPDF from 'jspdf';


export interface Categories {
  major: string;
  minor: string[];
}

@Component({
  selector: 'kt-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})



export class LandingComponent implements OnInit {
	keywordSearch: any = '';
	title: string = 'Data4Impact' ;
	image: string = './assets/media/bg/bg-2.jpg' ;
	logo: string = './assets/media/logos/D4I logo-dark.png' ;
	public model: any;
	closeResult: string;
	fieldChosen:boolean = false;
	user$: Observable<any>;

	constructor(private layoutConfigService: LayoutConfigService, private searchKeywordService : SearchKeywordService) {
	  // set temporary values to the layout config on this page
	  //this.layoutConfigService.setConfig({self: {layout: 'blank'}});
	  //console.log(this.layoutConfigService);
	}
		
	
	startTour() {
		//this.hintService.initialize();
	} 
  
	ngOnInit() {  
	}
	
	clickSearch() {
		this.searchKeywordService.changeKeyword(this.keywordSearch);
	}
	
	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		// reset config from any temporary values
		this.layoutConfigService.reloadConfigs();
	}
	
}