// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// RxJS
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Auth
import { AuthNoticeService, AuthService } from '../../../../core/auth';
//Route
import { ActivatedRoute } from '@angular/router';

export interface Categories {
  major: string;
  minor: string[];
}

@Component({
  selector: 'kt-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})



export class ResetPasswordComponent implements OnInit {
	resetPasswordForm: FormGroup;
	loading = false;
	resetToken: string = '';
	errors: any = [];
	passwordsMatch = true;
	private unsubscribe: Subject<any>;
	private sub: any;
	that = this;
	constructor(
		private authService: AuthService,
		public authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private router: Router,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute
	) {
		this.unsubscribe = new Subject();
		//this.customValidator = this.customValidator.bind(this);
		//this.matchValidator = this.matchValidator.bind(this);
	}
		
  
	ngOnInit() {
		this.sub = this.route.params.subscribe(params => {
			this.resetToken = params['id'];
		});
		this.initResetPasswordForm();
	  
	}
	
	/**
	 * Form initalization
	 * Default params, validators
	 */
	initResetPasswordForm() {
		
		this.resetPasswordForm = this.fb.group({
			new_password: ['', Validators.compose([
				Validators.required,
				Validators.minLength(5),
				Validators.maxLength(30),
				//this.customValidator
			])
			],
			ver_password: ['', Validators.compose([
				Validators.required,
				Validators.minLength(5),
				Validators.maxLength(30)
			])
			]
		}, {validator: this.matchValidator.bind(this)});
	}

	matchValidator(group) {
		console.log('matchValidator');
		var valid = false;
		this.passwordsMatch = true;
		let password = group.controls['new_password'];
		let confirmPassword = group.controls['ver_password'];
		if (password.value === confirmPassword.value) {
			valid = true;
			console.log('match');
		}
		if (valid) {
			return null;
		}
		this.passwordsMatch = false;		
		return {
			mismatch: true
		};
	}
	
	/*private customValidator(control) {
        // check if control is equal to the password1 control
		return {isEqual: control.value === this.resetPasswordForm.controls['ver_password'].value};
        //return {isEqual: control.value === this.resetPasswordForm.controls['ver_password'].value};
    }*/
	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		// reset config from any temporary values
		
	}
	
	/**
	 * Form Submit
	 */
	submit() {
		const controls = this.resetPasswordForm.controls;
		/** check form */
		if (this.resetPasswordForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;

		const new_password = controls['new_password'].value;
		const ver_password = controls['ver_password'].value;
		const body = {new_password: new_password, ver_password: ver_password};
		console.log(this.resetToken);
		console.log(body);
		this.authService.resetPassword(body, this.resetToken).pipe(
			tap(response => {
				if (response && response.success) {
					console.log(response);
					this.authNoticeService.setNotice('Your password has been successfuly reset!', 'success');
					this.router.navigateByUrl('/auth/login');
				} else {
					console.log(response);
					this.authNoticeService.setNotice('The link you used is either wrong or expired', 'danger');
				}
			}),
			takeUntil(this.unsubscribe),
			finalize(() => {
				this.loading = false;
				this.cdr.markForCheck();
			})
		).subscribe();
	}
	
	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.resetPasswordForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
	
}