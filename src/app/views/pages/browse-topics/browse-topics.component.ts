import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FilterResultsPipe, BrowseTopicsService } from '../../../core/_base/layout';

@Component({
  selector: 'kt-browse-topics',
  templateUrl: './browse-topics.component.html',
  styleUrls: ['./browse-topics.component.scss']
})
export class BrowseTopicsComponent implements OnInit {
	returnedTopics: boolean = false;
	subtopics: boolean = false;
	majortopics: boolean = false;
	objectKeys = Object.keys;
	currentTopicIndex: number;
	searchText: any ='';
	catList: object  = {};

	constructor(private browseTopicsService: BrowseTopicsService, private cd: ChangeDetectorRef ) {
		
	}

	ngOnInit() {
	  //	
		this.browseTopicsService.getTopics().subscribe(returnedData => {			
			var returnedData = returnedData;
			console.log('this.jsonList');
			console.log(returnedData);
			if(returnedData.success === true) {
				this.catList = returnedData.data.results[0];
				this.returnedTopics = true;
				this.cd.markForCheck();
			} else {				
				console.log('Error while receiving data');
			}

		});	  
	}
	
	showmajorTopics() {
		this.majortopics = true;
		this.searchText = '';	
	}
	
	showsubTopics(topicEvent) {
		console.log(topicEvent);
		console.log(topicEvent.currentTarget);
		this.currentTopicIndex = topicEvent.currentTarget.attributes['data-index']['value'];
		this.subtopics = true;
		this.searchText = '';
		let windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (windowWidth < 769) {
			var target: any = document.getElementById('scroll_to');
			var scrollContainer: any = target;
			do { //find scroll container
				scrollContainer = scrollContainer.parentNode;
				if (!scrollContainer) return;
				scrollContainer.scrollTop += 1;
			} while (scrollContainer.scrollTop == 0);

			var targetY: any = 0;
			do { //find the top of target relatively to the container
				if (target == scrollContainer) break;
				targetY += target.offsetTop - 20;
			} while (target = target.offsetParent);

			var scroll = function(c, a, b, i) {
				i++; if (i > 30) return;
				c.scrollTop = a + (b - a) / 30 * i;
				setTimeout(function(){ scroll(c, a, b, i); }, 20);
			}
			// start scrolling
			scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
		}
	}

}
