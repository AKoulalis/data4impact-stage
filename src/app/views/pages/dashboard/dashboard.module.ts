// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { DashboardComponent } from './dashboard.component';
//Bootstrap
import { NgbAlertConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';


//Extra Stuff
import { FilterSidebarComponent } from '../../theme/filter-sidebar/filter-sidebar.component';
// Perfect Scrollbar
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import {GuidedTourModule} from 'ngx-guided-tour';

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		CoreModule,
		NgbModule,
		
		RouterModule.forChild([
			{
				path: '',
				component: DashboardComponent
			},
		]),
		PerfectScrollbarModule,
		GuidedTourModule,
	],
	providers: [
		NgbAlertConfig,
	],
	declarations: [
		DashboardComponent,	
		FilterSidebarComponent,		
	]
})
export class DashboardModule {
}
