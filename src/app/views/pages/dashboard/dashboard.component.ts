// Angular
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SearchToolbarComponent } from '../../partials/layout/search-toolbar/search-toolbar.component';
import {Observable} from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../core/reducers';
import { currentUser, User } from '../../../core/auth';
//import { Directive, ElementRef, Input, HostBinding, OnChanges, OnDestroy, ViewChild } from '@angular/core';
//Bootstrap
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
// Lodash
import { shuffle } from 'lodash';
// Services
// Widgets model
import { LayoutConfigService, LoadJsonService, SubheaderService, SearchSidebarService } from '../../../core/_base/layout';
import { FilterSidebarComponent } from '../../theme/filter-sidebar/filter-sidebar.component';
import { ActivatedRoute } from '@angular/router';
// GuidedTour
import {GuidedTourModule, GuidedTourService} from 'ngx-guided-tour';
import { GuidedTour, Orientation } from 'ngx-guided-tour';


@Component({
	selector: 'kt-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['dashboard.component.scss'],
})


export class DashboardComponent implements OnInit {	
	chartData: any;
	chartId: any;
	restrictedCalls = ['Organisation', 'Programme', 'Call', 'Project'];
	restricted: boolean = true;
	dataLoaded: boolean = false;
	user$: Observable<any>;
	filters: any = [];
	reportTitle: string;
	private routeSubscription: any;

	constructor( private loadJsonService : LoadJsonService, private cdRef: ChangeDetectorRef, private route: ActivatedRoute, private subheaderService: SubheaderService, private searchSidebarService: SearchSidebarService, private store: Store<AppState>) {

	}


	ngOnInit(): void {
		this.user$ = this.store.pipe(select(currentUser));
		this.routeSubscription = this.route.params.subscribe(params => {
			this.chartId = params['id'];
			this.loadJsonService.getChart(this.chartId).subscribe(data => {			
				//console.log('Dashboard getChart data');
				//console.log(data);
				const pageTitle = data['data']['results']['title'] + ' (' + data['data']['results']['type'] + ')';
				this.subheaderService.setDescription('Report', pageTitle);
				if (this.chartData !== data) {
					this.cdRef.markForCheck();
				}
				this.chartData = data['data']['results']['charts'];
				//var filters = {'Field': 'Health', 'Time Range': '2005 - 2010'};
				var chartFilters = data['data']['results']['filters'];
				//var chartFilters = {'Field': 'Health', 'Time Range': '2005 - 2010'};
				var filterArray = [];
				Object.keys(chartFilters).map((key, index) => {
					filterArray.push(key + ': ' + chartFilters[key]);
				})
				var chartType = data['data']['results']['type'];
				var filterString = filterArray.join(', ');
				this.filters = [chartType, filterString];
				this.reportTitle = data['data']['results']['title'];
				// check if calls are restricted for non-loged-in users
				if (this.restrictedCalls.includes(chartType)) {
					this.restricted = true;
				} else {
					this.restricted = false;
				}
				this.dataLoaded = true;
			});

		});
	}
	
	ngOnDestroy() {
		this.routeSubscription.unsubscribe();
	}
	
	toggleSearch() {
		console.log('click toggled');
		this.searchSidebarService.toggleSearch();
		//let searchToolbarComponent = new SearchToolbarComponent();
		//searchToolbarComponent.toggleSidebar();
	}
	
}
