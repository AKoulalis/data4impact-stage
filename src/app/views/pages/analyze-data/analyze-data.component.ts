import { ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { SendMailService } from '../../../core/_base/layout';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Translate
import { TranslateService } from '@ngx-translate/core';
// RxJS
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
// Auth
import { AuthNoticeService } from '../../../core/auth';

@Component({
  selector: 'kt-analyze-data',
  templateUrl: './analyze-data.component.html',
  styleUrls: ['./analyze-data.component.scss']
})
export class AnalyzeDataComponent implements OnInit {
	model: any = {
		fname: 'John',
		lname: 'Wick',
		phone: '+61412345678',
		email: 'john.wick@reeves.com',
		company: 'Address Line 1',
		country: 'AU',
		title: ''
	};
	analyzeDataForm: FormGroup;
	loading = false;
	errors: any = [];
	private unsubscribe: Subject<any>;
	
	constructor(private fb: FormBuilder,private translate: TranslateService,private sendMailService: SendMailService, private cdr: ChangeDetectorRef, public authNoticeService: AuthNoticeService,) {
		this.unsubscribe = new Subject();
	}

	ngOnInit() {
	  this.initAnalyzeDataForm();
	  
	}
	
	/**
	 * Form initalization
	 * Default params, validators
	 */
	initAnalyzeDataForm() {
		this.analyzeDataForm = this.fb.group({
			email: ['', Validators.compose([
				Validators.required,
				//Validators.email,
				Validators.minLength(3),
				Validators.maxLength(320)
			])
			],
			phone:'',
			fname:['', Validators.compose([
				Validators.required,
				Validators.minLength(3),
			])
			],
			lname:[''/*, Validators.compose([
				Validators.required,
				Validators.minLength(3),
			])*/
			], 
			company:[''/*, Validators.compose([
				Validators.required,
				Validators.minLength(3),
			])*/
			], 
			title:''
		});
	}
	
	onSubmit() {
		console.log('submitted');
		const controls = this.analyzeDataForm.controls;
		/** check form */
		if (this.analyzeDataForm.invalid) {
			console.log('invalid');
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		this.loading = true;
		
		const email = controls['email'].value;
		const fname = controls['fname'].value;
		const lname = controls['lname'].value;
		const phone = controls['phone'].value;
		const company = controls['company'].value;
		const title = controls['title'].value;
		console.log(controls);
		console.log(email);
		let emailText = 'A user has contacted you via the platform data4Impact, from the page "Analyse my Data". <br>';
		emailText += 'User info: <br>';
		emailText += 'Name:' + fname + " " + lname + "<br>";
		emailText += 'E-mail:' + email + "<br>";
		emailText += 'Phone Number:' + phone + "<br>";
		emailText += 'Company:' + company + "<br>";
		emailText += 'Title:' + title + "<br>";
		
		this.sendMailService.sendMail({email:email, text:emailText}).pipe(
			tap(response => {
				if (response && response.success) {
					console.log(response);
					this.authNoticeService.setNotice('Your request has been successfully sent.', 'success');
				} else {
					console.log(response);
					this.authNoticeService.setNotice(this.translate.instant('Temporarily out of order. Please try again later', {name: this.translate.instant('AUTH.INPUT.EMAIL')}), 'danger');
				}
			}),
			takeUntil(this.unsubscribe),
			finalize(() => {
				this.loading = false;
				this.cdr.markForCheck();
			})
		).subscribe();
		
	}
	
	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.analyzeDataForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
	
	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

}
