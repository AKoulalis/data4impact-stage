(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-dashboard-dashboard-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/dashboard/dashboard.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/dashboard/dashboard.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-section\">\r\n\t<div class=\"kt-section__content\">\r\n\t\t<div class=\"mobile-search\">\r\n\t\t\t<button class=\"btn mb-2\" (click)=\"toggleSearch()\"><i class=\"flaticon2-search-1\"></i>Search</button> \r\n\t\t</div>\r\n\t\t<div *ngIf=\"dataLoaded\">\r\n\t\t\t<div *ngIf=\"!(user$ | async) as _user; else chartContainer\">\r\n\t\t\t\t<div *ngIf=\"restricted; else chartContainer\">\r\n\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t<kt-portlet [class]=\"'kt-portlet--height-fluid-half kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"restricted-text\">\r\n\t\t\t\t\t\t\t\t\t\t<h5>Please <a [routerLink]=\"['/auth/login']\">log in</a> or <a [routerLink]=\"['/auth/register']\">register</a> to view the corresponding analysis.</h5>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<ng-template #chartContainer>\r\n\t\t\t<ngb-tabset type=\"pills\" >\r\n\t\t\t\t<ngb-tab title=\"Input\">\r\n\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t<!--<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-table *ngIf=\"chartData['table1']\" [chartTitle]=\"'l1_d4_innovations'\" [chartData]=\"chartData['table1']\" style=\"width: 100%\"></kt-table>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>-->\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart1 *ngIf=\"chartData['chart1']\" [chartTitle]=\"this.reportTitle\" [chartData]=\"chartData['chart1']\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart1>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart2 *ngIf=\"chartData['chart2']\" [chartTitle]=\"this.reportTitle\"  [chartData]=\"chartData['chart2']\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart2>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<!--<kt-echart [chartTitle]=\"'l1_d1_year_activitytype_funding'\"></kt-echart>-->\r\n\t\t\t\t\t\t\t\t<kt-echart3 *ngIf=\"chartData['chart3']\" [chartTitle]=\"this.reportTitle\" [chartData]=\"chartData['chart3']\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart3>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart4 *ngIf=\"chartData['chart4']\" [chartTitle]=\"this.reportTitle\"  [chartData]=\"chartData['chart4']\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart4>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t</ngb-tab>\r\n\t\t\t\t<ngb-tab title=\"Throughput/Output\">\r\n\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t<!--<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-table *ngIf=\"chartData['table2']\" [chartTitle]=\"'l1_d4_innovations'\" [chartData]=\"chartData['table2']\" style=\"width: 100%\"></kt-table>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>-->\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart5 *ngIf=\"chartData['chart5']\" [chartTitle]=\"this.reportTitle\" [chartData]=\"chartData['chart5']\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart5>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart6  *ngIf=\"chartData['chart6']\" [chartTitle]=\"this.reportTitle\" [chartData]=\"chartData['chart6']\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart6>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart7 *ngIf=\"chartData['chart7']\" [chartData]=\"chartData['chart7']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart7>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart8 *ngIf=\"chartData['chart8']\" [chartData]=\"chartData['chart8']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart8>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t</ngb-tab>\r\n\t\t\t\t<ngb-tab title=\"Academic Impact\">\r\n\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t<!--<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-table *ngIf=\"chartData['table3']\" [chartTitle]=\"'l1_d4_innovations'\" [chartData]=\"chartData['table3']\" style=\"width: 100%\"></kt-table>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>-->\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart9 *ngIf=\"chartData['chart9']\" [chartData]=\"chartData['chart9']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart9>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart10 *ngIf=\"chartData['chart10']\" [chartData]=\"chartData['chart10']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart10>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart11 *ngIf=\"chartData['chart11']\" [chartData]=\"chartData['chart11']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart11>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart12  *ngIf=\"chartData['chart12']\" [chartData]=\"chartData['chart12']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart12>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t</ngb-tab>\r\n\t\t\t\t<ngb-tab title=\"Economic Impact\">\r\n\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t<!--<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-table *ngIf=\"chartData['table4']\" [chartTitle]=\"'l1_d4_innovations'\" [chartData]=\"chartData['table4']\" style=\"width: 100%\"></kt-table>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>-->\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart13 *ngIf=\"chartData['chart13']\" [chartData]=\"chartData['chart13']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart13>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart14 *ngIf=\"chartData['chart14']\" [chartData]=\"chartData['chart14']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart14>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart15 *ngIf=\"chartData['chart15']\" [chartData]=\"chartData['chart15']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart15>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t</ngb-tab>\r\n\t\t\t\t<ngb-tab title=\"Societal Impact\">\r\n\t\t\t\t\t<ng-template ngbTabContent>\r\n\t\t\t\t\t\t<!--<kt-portlet *ngIf=\"chartData\" [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-table *ngIf=\"chartData['table5']\" [chartTitle]=\"'l1_d4_innovations'\" [chartData]=\"chartData['table5']\" style=\"width: 100%\"></kt-table>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>-->\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart16 *ngIf=\"chartData['chart16']\" [chartData]=\"chartData['chart16']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart16>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<!--<kt-portlet [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart17 [chartTitle]=\"'l1_d5_medicinetype_medicinesubtype_medicinenumber'\" style=\"width: 100%\"></kt-echart17>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>-->\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart18 *ngIf=\"chartData['chart17']\" [chartData]=\"chartData['chart17']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart18>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t\t<div class=\"kt-space-20\"></div>\r\n\t\t\t\t\t\t<kt-portlet *ngIf=\"chartData\"  [class]=\"'kt-portlet--border-bottom-brand'\">\r\n\t\t\t\t\t\t\t<kt-portlet-body [class]=\"'kt-portlet__body--fluid'\">\r\n\t\t\t\t\t\t\t\t<kt-echart19 *ngIf=\"chartData['chart18']\" [chartData]=\"chartData['chart18']\" [chartTitle]=\"this.reportTitle\" [filters]=\"filters\" style=\"width: 100%\"></kt-echart19>\r\n\t\t\t\t\t\t\t</kt-portlet-body>\r\n\t\t\t\t\t\t</kt-portlet>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t</ngb-tab>\r\n\t\t\t</ngb-tabset>\r\n\t\t</ng-template>\r\n\t\t\r\n\r\n\t\t\r\n\t</div>\r\n\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/theme/filter-sidebar/filter-sidebar.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/theme/filter-sidebar/filter-sidebar.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- BEGIN: Left Aside -->\r\n<div ktOffcanvas [options]=\"menuCanvasOptions\" class=\"kt-filter-sidebar kt-aside--absolute kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop\" id=\"kt_aside\">\r\n\r\n\t<!-- BEGIN: Aside Menu -->\r\n\t<div class=\"kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid\" id=\"kt_aside_menu_wrapper\">\r\n\t\t<div #asideMenu ktMenu [options]=\"menuOptions\" [perfectScrollbar]=\"{wheelPropagation: false}\" [ngStyle]=\"{'max-height': '100vh', 'position': 'relative'}\" id=\"kt_aside_menu\" class=\"kt-aside-menu\"\r\n\t\t\t(mouseenter)=\"mouseEnter($event)\" (mouseleave)=\"mouseLeave($event)\"\r\n\t\t\t[ngClass]=\"htmlClassService.getClasses('aside_menu', true)\">\r\n\t\t\t<ul class=\"kt-menu__nav\" [ngClass]=\"htmlClassService.getClasses('aside_menu_nav', true)\">\r\n\t\t\t\t<li>lalalalaalalaaaaaaaaaa</li>\r\n\t\t\t\t<li>lalalalaalalaaaaaaaaaa</li>\r\n\t\t\t\t<li>lalalalaalalaaaaaaaaaa</li>\r\n\t\t\t</ul>\r\n\t\t</div>\r\n\t</div>\r\n\t<!-- END: Aside Menu -->\r\n</div>\r\n<!-- END: Left Aside -->"

/***/ }),

/***/ "./src/app/views/pages/dashboard/dashboard.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/views/pages/dashboard/dashboard.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ::ng-deep ngb-tabset > .nav-tabs {\n  display: none;\n}\n:host ::ng-deep .restricted-text a {\n  color: #0abb87;\n  transition: opacity 0.3s;\n}\n:host ::ng-deep .restricted-text a:hover {\n  opacity: 0.7;\n}\n@media (min-width: 1025px) {\n  .mobile-search {\n    display: none;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvcGFnZXMvZGFzaGJvYXJkL0M6XFxVc2Vyc1xcYWtvdWxhbGlzXFxXZWJzdG9ybVByb2plY3RzXFxkYXRhNGltcGFjdC1zdGFnZS9zcmNcXGFwcFxcdmlld3NcXHBhZ2VzXFxkYXNoYm9hcmRcXGRhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvcGFnZXMvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFRTtFQUNDLGFBQUE7QUNESDtBRElHO0VBQ0MsY0FBQTtFQUNBLHdCQUFBO0FDRko7QURHSTtFQUNDLFlBQUE7QUNETDtBRFFBO0VBQ0M7SUFDQyxhQUFBO0VDTEE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcblx0OjpuZy1kZWVwIHtcclxuXHRcdG5nYi10YWJzZXQgPiAubmF2LXRhYnMge1xyXG5cdFx0XHRkaXNwbGF5OiBub25lO1xyXG5cdFx0fVxyXG5cdFx0LnJlc3RyaWN0ZWQtdGV4dCB7XHJcblx0XHRcdGEge1xyXG5cdFx0XHRcdGNvbG9yOiAjMGFiYjg3O1xyXG5cdFx0XHRcdHRyYW5zaXRpb246IG9wYWNpdHkgMC4zcztcclxuXHRcdFx0XHQmOmhvdmVyIHtcclxuXHRcdFx0XHRcdG9wYWNpdHk6IDAuNztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiAxMDI1cHgpIHtcclxuXHQubW9iaWxlLXNlYXJjaCB7XHJcblx0XHRkaXNwbGF5OiBub25lO1xyXG5cdH1cclxufSIsIjpob3N0IDo6bmctZGVlcCBuZ2ItdGFic2V0ID4gLm5hdi10YWJzIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbjpob3N0IDo6bmctZGVlcCAucmVzdHJpY3RlZC10ZXh0IGEge1xuICBjb2xvcjogIzBhYmI4NztcbiAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjNzO1xufVxuOmhvc3QgOjpuZy1kZWVwIC5yZXN0cmljdGVkLXRleHQgYTpob3ZlciB7XG4gIG9wYWNpdHk6IDAuNztcbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkge1xuICAubW9iaWxlLXNlYXJjaCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/pages/dashboard/dashboard.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/views/pages/dashboard/dashboard.component.ts ***!
  \**************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm5/store.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");

// Angular

// NGRX


// Services
// Widgets model


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(loadJsonService, cdRef, route, subheaderService, searchSidebarService, store) {
        this.loadJsonService = loadJsonService;
        this.cdRef = cdRef;
        this.route = route;
        this.subheaderService = subheaderService;
        this.searchSidebarService = searchSidebarService;
        this.store = store;
        this.restrictedCalls = ['Organisation', 'Programme', 'Call', 'Project'];
        this.restricted = true;
        this.dataLoaded = false;
        this.filters = [];
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user$ = this.store.pipe(Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_2__["select"])(_core_auth__WEBPACK_IMPORTED_MODULE_3__["currentUser"]));
        this.routeSubscription = this.route.params.subscribe(function (params) {
            _this.chartId = params['id'];
            _this.loadJsonService.getChart(_this.chartId).subscribe(function (data) {
                //console.log('Dashboard getChart data');
                //console.log(data);
                var pageTitle = data['data']['results']['title'] + ' (' + data['data']['results']['type'] + ')';
                _this.subheaderService.setDescription('Report', pageTitle);
                if (_this.chartData !== data) {
                    _this.cdRef.markForCheck();
                }
                _this.chartData = data['data']['results']['charts'];
                //var filters = {'Field': 'Health', 'Time Range': '2005 - 2010'};
                var chartFilters = data['data']['results']['filters'];
                //var chartFilters = {'Field': 'Health', 'Time Range': '2005 - 2010'};
                var filterArray = [];
                Object.keys(chartFilters).map(function (key, index) {
                    filterArray.push(key + ': ' + chartFilters[key]);
                });
                var chartType = data['data']['results']['type'];
                var filterString = filterArray.join(', ');
                _this.filters = [chartType, filterString];
                _this.reportTitle = data['data']['results']['title'];
                // check if calls are restricted for non-loged-in users
                if (_this.restrictedCalls.includes(chartType)) {
                    _this.restricted = true;
                }
                else {
                    _this.restricted = false;
                }
                _this.dataLoaded = true;
            });
        });
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        this.routeSubscription.unsubscribe();
    };
    DashboardComponent.prototype.toggleSearch = function () {
        console.log('click toggled');
        this.searchSidebarService.toggleSearch();
        //let searchToolbarComponent = new SearchToolbarComponent();
        //searchToolbarComponent.toggleSidebar();
    };
    DashboardComponent.ctorParameters = function () { return [
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["LoadJsonService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SearchSidebarService"] },
        { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
    ]; };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-dashboard',
            template: __webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/views/pages/dashboard/dashboard.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_base_layout__WEBPACK_IMPORTED_MODULE_4__["LoadJsonService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SubheaderService"], _core_base_layout__WEBPACK_IMPORTED_MODULE_4__["SearchSidebarService"], _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/dashboard/dashboard.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/views/pages/dashboard/dashboard.module.ts ***!
  \***********************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _partials_partials_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../partials/partials.module */ "./src/app/views/partials/partials.module.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/views/pages/dashboard/dashboard.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _theme_filter_sidebar_filter_sidebar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../theme/filter-sidebar/filter-sidebar.component */ "./src/app/views/theme/filter-sidebar/filter-sidebar.component.ts");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var ngx_guided_tour__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-guided-tour */ "./node_modules/ngx-guided-tour/fesm5/ngx-guided-tour.js");

// Angular



// Core Module



//Bootstrap

//Extra Stuff

// Perfect Scrollbar


var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _partials_partials_module__WEBPACK_IMPORTED_MODULE_5__["PartialsModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _dashboard_component__WEBPACK_IMPORTED_MODULE_6__["DashboardComponent"]
                    },
                ]),
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__["PerfectScrollbarModule"],
                ngx_guided_tour__WEBPACK_IMPORTED_MODULE_10__["GuidedTourModule"],
            ],
            providers: [
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbAlertConfig"],
            ],
            declarations: [
                _dashboard_component__WEBPACK_IMPORTED_MODULE_6__["DashboardComponent"],
                _theme_filter_sidebar_filter_sidebar_component__WEBPACK_IMPORTED_MODULE_8__["FilterSidebarComponent"],
            ]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ }),

/***/ "./src/app/views/theme/filter-sidebar/filter-sidebar.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/views/theme/filter-sidebar/filter-sidebar.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .kt-aside {\n  height: 100%;\n}\n:host .kt-aside .kt-aside-menu {\n  margin: 0;\n}\n:host .kt-menu__link-text {\n  white-space: nowrap;\n}\n.kt-filter-sidebar {\n  /*position: absolute;\n  left: -25px;\n  top: 0;\n  height: 100%;\n  z-index: 1;*/\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvdGhlbWUvZmlsdGVyLXNpZGViYXIvQzpcXFVzZXJzXFxha291bGFsaXNcXFdlYnN0b3JtUHJvamVjdHNcXGRhdGE0aW1wYWN0LXN0YWdlL3NyY1xcYXBwXFx2aWV3c1xcdGhlbWVcXGZpbHRlci1zaWRlYmFyXFxmaWx0ZXItc2lkZWJhci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvdGhlbWUvZmlsdGVyLXNpZGViYXIvZmlsdGVyLXNpZGViYXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0M7RUFDQyxZQUFBO0FDQUY7QURFRTtFQUNDLFNBQUE7QUNBSDtBREtDO0VBQ0MsbUJBQUE7QUNIRjtBRE9BO0VBQ0M7Ozs7Y0FBQTtBQ0FEIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvdGhlbWUvZmlsdGVyLXNpZGViYXIvZmlsdGVyLXNpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcblx0Lmt0LWFzaWRlIHtcclxuXHRcdGhlaWdodDogMTAwJTtcclxuXHJcblx0XHQua3QtYXNpZGUtbWVudSB7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC8vIGZpeGVkIHRleHQgbGluZSBicmVhayBpc3N1ZSBvbiBtaW5pbWl6ZWQgYXNpZGUgaG92ZXJcclxuXHQua3QtbWVudV9fbGluay10ZXh0IHtcclxuXHRcdHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcblx0fVxyXG59XHJcblxyXG4ua3QtZmlsdGVyLXNpZGViYXIge1xyXG5cdC8qcG9zaXRpb246IGFic29sdXRlO1xyXG5cdGxlZnQ6IC0yNXB4O1xyXG5cdHRvcDogMDtcclxuXHRoZWlnaHQ6IDEwMCU7XHJcblx0ei1pbmRleDogMTsqL1xyXG59IiwiOmhvc3QgLmt0LWFzaWRlIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuOmhvc3QgLmt0LWFzaWRlIC5rdC1hc2lkZS1tZW51IHtcbiAgbWFyZ2luOiAwO1xufVxuOmhvc3QgLmt0LW1lbnVfX2xpbmstdGV4dCB7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG5cbi5rdC1maWx0ZXItc2lkZWJhciB7XG4gIC8qcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAtMjVweDtcbiAgdG9wOiAwO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHotaW5kZXg6IDE7Ki9cbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/theme/filter-sidebar/filter-sidebar.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/views/theme/filter-sidebar/filter-sidebar.component.ts ***!
  \************************************************************************/
/*! exports provided: FilterSidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterSidebarComponent", function() { return FilterSidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! object-path */ "./node_modules/object-path/index.js");
/* harmony import */ var object_path__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(object_path__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _core_base_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../core/_base/layout */ "./src/app/core/_base/layout/index.ts");
/* harmony import */ var _html_class_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../html-class.service */ "./src/app/views/theme/html-class.service.ts");





// Layout


var FilterSidebarComponent = /** @class */ (function () {
    /**
     * Component Conctructor
     *
     * @param htmlClassService: HtmlClassService
     * @param menuAsideService
     * @param layoutConfigService: LayouConfigService
     * @param router: Router
     * @param render: Renderer2
     * @param cdr: ChangeDetectorRef
     */
    function FilterSidebarComponent(htmlClassService, menuAsideService, layoutConfigService, router, render, cdr) {
        this.htmlClassService = htmlClassService;
        this.menuAsideService = menuAsideService;
        this.layoutConfigService = layoutConfigService;
        this.router = router;
        this.render = render;
        this.cdr = cdr;
        this.currentRouteUrl = '';
        this.menuCanvasOptions = {
            baseClass: 'kt-aside',
            overlay: true,
            closeBy: 'kt_aside_close_btn',
            toggleBy: {
                target: 'kt_aside_mobile_toggler',
                state: 'kt-header-mobile__toolbar-toggler--active'
            }
        };
        this.menuOptions = {
            // vertical scroll
            scroll: null,
            // submenu setup
            submenu: {
                desktop: {
                    // by default the menu mode set to accordion in desktop mode
                    default: 'dropdown',
                },
                tablet: 'accordion',
                mobile: 'accordion' // menu set to accordion in mobile mode
            },
            // accordion setup
            accordion: {
                expandAll: false // allow having multiple expanded accordions in the menu
            }
        };
    }
    FilterSidebarComponent.prototype.ngAfterViewInit = function () {
    };
    FilterSidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentRouteUrl = this.router.url.split(/[?#]/)[0];
        console.log('aside!---');
        this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]; }))
            .subscribe(function (event) {
            _this.currentRouteUrl = _this.router.url.split(/[?#]/)[0];
            _this.cdr.markForCheck();
        });
        var config = this.layoutConfigService.getConfig();
        if (object_path__WEBPACK_IMPORTED_MODULE_4__["get"](config, 'aside.menu.dropdown') !== true && object_path__WEBPACK_IMPORTED_MODULE_4__["get"](config, 'aside.self.fixed')) {
            this.render.setAttribute(this.asideMenu.nativeElement, 'data-ktmenu-scroll', '1');
        }
        if (object_path__WEBPACK_IMPORTED_MODULE_4__["get"](config, 'aside.menu.dropdown')) {
            this.render.setAttribute(this.asideMenu.nativeElement, 'data-ktmenu-dropdown', '1');
            // tslint:disable-next-line:max-line-length
            this.render.setAttribute(this.asideMenu.nativeElement, 'data-ktmenu-dropdown-timeout', object_path__WEBPACK_IMPORTED_MODULE_4__["get"](config, 'aside.menu.submenu.dropdown.hover-timeout'));
        }
    };
    /**
     * Check Menu is active
     * @param item: any
     */
    FilterSidebarComponent.prototype.isMenuItemIsActive = function (item) {
        if (item.submenu) {
            return this.isMenuRootItemIsActive(item);
        }
        if (!item.page) {
            return false;
        }
        return this.currentRouteUrl.indexOf(item.page) !== -1;
    };
    /**
     * Check Menu Root Item is active
     * @param item: any
     */
    FilterSidebarComponent.prototype.isMenuRootItemIsActive = function (item) {
        var result = false;
        for (var _i = 0, _a = item.submenu; _i < _a.length; _i++) {
            var subItem = _a[_i];
            result = this.isMenuItemIsActive(subItem);
            if (result) {
                return true;
            }
        }
        return false;
    };
    /**
     * Use for fixed left aside menu, to show menu on mouseenter event.
     * @param e Event
     */
    FilterSidebarComponent.prototype.mouseEnter = function (e) {
        var _this = this;
        // check if the left aside menu is fixed
        if (document.body.classList.contains('kt-aside--fixed')) {
            if (this.outsideTm) {
                clearTimeout(this.outsideTm);
                this.outsideTm = null;
            }
            this.insideTm = setTimeout(function () {
                // if the left aside menu is minimized
                if (document.body.classList.contains('kt-aside--minimize') && KTUtil.isInResponsiveRange('desktop')) {
                    // show the left aside menu
                    _this.render.removeClass(document.body, 'kt-aside--minimize');
                    _this.render.addClass(document.body, 'kt-aside--minimize-hover');
                }
            }, 50);
        }
    };
    /**
     * Use for fixed left aside menu, to show menu on mouseenter event.
     * @param e Event
     */
    FilterSidebarComponent.prototype.mouseLeave = function (e) {
        var _this = this;
        if (document.body.classList.contains('kt-aside--fixed')) {
            if (this.insideTm) {
                clearTimeout(this.insideTm);
                this.insideTm = null;
            }
            this.outsideTm = setTimeout(function () {
                // if the left aside menu is expand
                if (document.body.classList.contains('kt-aside--minimize-hover') && KTUtil.isInResponsiveRange('desktop')) {
                    // hide back the left aside menu
                    _this.render.removeClass(document.body, 'kt-aside--minimize-hover');
                    _this.render.addClass(document.body, 'kt-aside--minimize');
                }
            }, 100);
        }
    };
    /**
     * Returns Submenu CSS Class Name
     * @param item: any
     */
    FilterSidebarComponent.prototype.getItemCssClasses = function (item) {
        var classes = 'kt-menu__item';
        if (object_path__WEBPACK_IMPORTED_MODULE_4__["get"](item, 'submenu')) {
            classes += ' kt-menu__item--submenu';
        }
        if (!item.submenu && this.isMenuItemIsActive(item)) {
            classes += ' kt-menu__item--active kt-menu__item--here';
        }
        if (item.submenu && this.isMenuItemIsActive(item)) {
            classes += ' kt-menu__item--open kt-menu__item--here';
        }
        // custom class for menu item
        var customClass = object_path__WEBPACK_IMPORTED_MODULE_4__["get"](item, 'custom-class');
        if (customClass) {
            classes += ' ' + customClass;
        }
        if (object_path__WEBPACK_IMPORTED_MODULE_4__["get"](item, 'icon-only')) {
            classes += ' kt-menu__item--icon-only';
        }
        return classes;
    };
    FilterSidebarComponent.prototype.getItemAttrSubmenuToggle = function (item) {
        var toggle = 'hover';
        if (object_path__WEBPACK_IMPORTED_MODULE_4__["get"](item, 'toggle') === 'click') {
            toggle = 'click';
        }
        else if (object_path__WEBPACK_IMPORTED_MODULE_4__["get"](item, 'submenu.type') === 'tabs') {
            toggle = 'tabs';
        }
        else {
            // submenu toggle default to 'hover'
        }
        return toggle;
    };
    FilterSidebarComponent.ctorParameters = function () { return [
        { type: _html_class_service__WEBPACK_IMPORTED_MODULE_6__["HtmlClassService"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_5__["MenuAsideService"] },
        { type: _core_base_layout__WEBPACK_IMPORTED_MODULE_5__["LayoutConfigService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('asideMenu', { static: true }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], FilterSidebarComponent.prototype, "asideMenu", void 0);
    FilterSidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-filter-sidebar',
            template: __webpack_require__(/*! raw-loader!./filter-sidebar.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/theme/filter-sidebar/filter-sidebar.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./filter-sidebar.component.scss */ "./src/app/views/theme/filter-sidebar/filter-sidebar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_html_class_service__WEBPACK_IMPORTED_MODULE_6__["HtmlClassService"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_5__["MenuAsideService"],
            _core_base_layout__WEBPACK_IMPORTED_MODULE_5__["LayoutConfigService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], FilterSidebarComponent);
    return FilterSidebarComponent;
}());



/***/ })

}]);
//# sourceMappingURL=app-views-pages-dashboard-dashboard-module.js.map